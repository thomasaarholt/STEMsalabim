What's new
==========

STEMsalabim 4.0.1, 4.0.2
------------------------

March 23rd, 2018
March 21st, 2018

- Bug fixes
- Changed chunking of the ADF variable


STEMsalabim 4.0
---------------

March 9th, 2018

**IMPORTANT**

I'm releasing this version as 4.0.0, but neither the input nor output files changed. The parameter `precision` has
become deprecated and there is a parameter `tmp-dir`. Please see the documentation.

- Removed option for double precision. When requested, this may be re-introduced, but it slowed down compilation times
  and made the code significantly more complicated. The multislice algorithm with all its approximations, including the
  scattering factor parametrization, is not precise enough to make the difference between single and double precision
  significant.
- Improved the Wave class, so that some important parts can now be vectorized by the compiler.
- Introduced some more caches, so that performance could greatly be improved. STEMsalabim should now be about twice as
  fast as before.
- Results of the MPI processors are now written to temporary files and merged after each configuration is finished.
  This removes many MPI calls which tended to slow down the simulation. See the `--tmp-dir` parameter.
- Moved the `Element`, `Atom`, and `Scattering` classes to their own (isolated) library `libatomic`. This is easier to
  maintain.
- Simplified MPI communication by getting rid of serialization of C++ objects into char arrays. This is too error-prone
  anyway.
- Added compatibility with the Intel parallel studio (Compilers, MKL for FFTs, Intel MPI).
  Tested with Intel 17 only.
- Some minor fixes and improvements.


STEMsalabim 3.1.0, 3.1.1, 3.1.2, 3.1.3, 3.1.4
---------------------------------------------

February 23nd, 2018

- Added GPL-3 License
- Moved all the code to Gitlab
- Moved documentation to readthedocs.org
- Added Gitlab CI

STEMsalabim 3.0.1 and 3.0.2
---------------------------

February 22nd, 2018

- Fixed a few bugs
- Improved the CMake files for better build process

STEMsalabim 3.0.0
-----------------

January 3rd, 2018

- Reworked input/output file format.
- Reworked CBED storing. Blank areas due to bandwidth limiting are now removed.
- Changes to the configuration, mainly to defocus series.
- Compression can be switched on and off via config file now.
- Prepared the project for adding a Python API in the future.
- Added tapering to smoothen the atomic potential at the edges as explained in
  `I. Lobato, et al, Ultramicroscopy 168, 17 (2016) <https://www.sciencedirect.com/science/article/pii/S030439911630081X>`_.
- Added analysis scripts for Python and MATLAB to the Si 001 example.

STEMsalabim 2.0.0
-----------------

August 1st, 2017

- Changed Documentation generator to Sphinx
- Introduced a lot of memory management to prevent memory fragmentation bugs
- split STEMsalabim into a core library and binaries to ease creation of tools
- Added diagnostics output with --print-diagnostics
- Code cleanup and commenting


STEMsalabim 2.0.0-beta2
-----------------------

April 20th, 2017

-  Added possibility to also save CBEDs, i.e., the kx/ky resolved
   intensities in reciprocal space.
-  Improved documentation.
-  Switched to NetCDF C API. Dependency on NetCDF C++ is dropped.
-  Switched to distributed (parallel) writing of the NC files, which is
   required for the CBED feature. This requires NetCDF C and HDF5 to be
   compiled with MPI support.

STEMsalabim 2.0.0-beta
----------------------

March 27th, 2017

-  Lots of code refactoring and cleanup
-  Added Doxygen doc strings
-  Added Markdown documentation and ``make doc`` target to build this
   website.
-  Refined the output file structure
-  Added HTTP reporting feature
-  Added ``fixed_slicing`` option to fix each atom's slice througout the
   simulation
-  Got rid of the boost libraries to ease compilation and installation

STEMsalabim 1.0
---------------

November 18th, 2016

-  Initial release.
