Citing STEMsalabim
==================

A technical paper introducing STEMsalabim is published in Ultramicroscopy journal [1].

If you use our program or its results, please cite us. You may use the following bibTeX entry:

::

    @article{Oelerich2017,
        title = "STEMsalabim: A high-performance computing cluster friendly code for scanning transmission electron microscopy image simulations of thin specimens",
        journal = "Ultramicroscopy",
        volume = "177",
        number = "",
        pages = "91 - 96",
        year = "2017",
        note = "",
        issn = "0304-3991",
        doi = "http://dx.doi.org/10.1016/j.ultramic.2017.03.010",
        url = "http://www.sciencedirect.com/science/article/pii/S030439911630300X",
        author = "Jan Oliver Oelerich and Lennart Duschek and Jürgen Belz and Andreas Beyer and Sergei D. Baranovskii and Kerstin Volz",
        keywords = "Multislice simulations",
        keywords = "Electron scattering factors",
        keywords = "MPI",
        keywords = "Phonons"
    }
    
[1]: http://dx.doi.org/10.1016/j.ultramic.2017.03.010