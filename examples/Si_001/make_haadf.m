% the filename with the results
filename = 'Si_001.nc';

% get some parameters from the file
adf_var_info = h5info(filename, '/adf/adf_intensities');

num_px_x = adf_var_info.Dataspace.Size(5);
num_px_y = adf_var_info.Dataspace.Size(4);
num_slices = adf_var_info.Dataspace.Size(2);

start_angle = 73;
end_angle = 173;

% as usual, the dimensions are [defocus, x, y, FP conf, slice, angle]
% reversed, because MATLAB. 
% Because the parameters adf.average_configurations and adf.average_defoci
% are set to true, there is only a single configuration and defocs in the
% results array, as the results have already been averaged by STEMsalabim.
start = [start_angle,             num_slices, 1, 1,     1,           1];
count = [end_angle - start_angle, 1,          1, num_px_y, num_px_x, 1];

% read in the desired intensities
img = h5read(filename, '/adf/adf_intensities', start, count);

% sum angles and eliminate dimensions with length 1
img = sum(img, 1);
img = squeeze(img);

imagesc(img);