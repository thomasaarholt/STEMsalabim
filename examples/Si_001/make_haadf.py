#!/bin/env python

# generates an HAADF image from Si_001.nc using netCDF4, numpy and 
# the Pillow image library

import netCDF4
from PIL import Image
import numpy as np

with netCDF4.Dataset('Si_001.nc', 'r') as inf:
    adf_group = inf.groups['adf']
    adf_var = adf_group.variables['adf_intensities']
    
    s = adf_var.shape
    num_defoci, px_x, px_y, num_confs, num_slices, num_angles = adf_var.shape
    
    # get data, sum over angles, and squeeze the dimension
    data = adf_var[:, :, :, :, num_slices-1, 73:173]
    data = np.sum(data, axis=4)
    data = np.squeeze(data)

    # rescale data, generate an image and save.
    data = (data - np.min(data))
    data /= np.max(data)
    im = Image.fromarray(np.uint8(data * 255))
    im = im.resize((500, 500), resample=Image.BILINEAR)
    im.save('haadf.png')
