application: {
    random_seed = 0;             # the random seed. 0 -> generate random random seed
    precision = "single";        # single or double precision
    skip_simulation = false;     # skip the actual multi-slice simulation (for debugging)
}

simulation: {
    title = "";                  # title of the simulation
    bandwidth_limiting = true;   # bandwidth limit the wave functions?
    normalize_always = false;    # normalize wave functions after each slice?
    output_file = "";            # output file name
    output_compress = false;     # compress output. Reduces the output size, but increases IO times.
}

probe: {
    c5 = 5e6;                    # Fifth order spherical aberrations coefficient. in nm
    cs = 2e3;                    # Third order spherical aberrations coefficient. in nm
    defocus = -2.0;              # defocus in nm.
    fwhm_defoci = 6.0;           # FWHM of the defocus distribution when simulating a defocus series for Cc.
    num_defoci = 1;              # number of the defoci when simulating a defocus series for Cc. Should be odd.
    astigmatism_ca = 0;          # Two-fold astigmatism. in nm
    astigmatism_angle = 0;       # Two-fold astigmatism angle. in mrad
    min_apert = 0.0;             # Minimum numerical aperture of the objective. in mrad
    max_apert = 24.0;            # Maximum numerical aperture of the objective. in mrad
    beam_energy = 200.0;         # Electron beam energy. in kV
    scan_density = 40;           # The sampling density for the electron probe scanning. in 1/nm
}

specimen: {
    max_potential_radius = 0.3;  # potential cut-off radius. in nm
    crystal_file = "";           # xyz file with columns [Element, x, y, z, MSD]
    field_file = "";             # A file with field strengths that are added to the atomic potentials.
}

grating: {
    density = 360.0;             # The density for the real space and fourier space grids. in 1/nm
    slice_thickness = 0.2715;    # Multi-slice slice thickness. in nm
}

frozen_phonon: {
    number_configurations = 1;   # Number of frozen phonon configurations to calculate
    fixed_slicing = true;        # When this is true, the z coordinate is not varied during phonon vibrations.
}

adf: {
    enabled = true;              # whether ADF intensities are calculated and stored
    x = (0.0, 1.0);              # [min, max] where min and max are in relative units
    y = (0.0, 1.0);              # [min, max] where min and max are in relative units
    detector_min_angle = 1.0;    # inner detector angle in mrad
    detector_max_angle = 300.0;  # outer detector angle in mrad
    detector_num_angles = 300;   # number of bins of the ADF detector.
    detector_interval_exponent = 1.0;     # possibility to set non-linear detector bins.
    save_slices_every = 1;       # save only every n slices. 0 -> only the sample bottom is saved.
    average_configurations = true; # average the frozen phonon configurations in the output file
    average_defoci = true;       # average the defoci in the output file
}

cbed: {
    enabled = false;             # whether CBED intensities are calculated and stored
    x = (0.0, 1.0);              # [min, max] where min and max are in relative units
    y = (0.0, 1.0);              # [min, max] where min and max are in relative units
    save_slices_every = 0;       # save only every n slices. 0 -> only the sample bottom is saved.
    size = [0, 0];               # When provided, this parameter determines the size of CBEDs saved
                                 # to the output file. The CBEDs are resized using bilinear interpolation.
    average_configurations = true; # average the frozen phonon configurations in the output file
    average_defoci = true;       # average the defoci in the output file
}

http_reporting: {                # send POST status requests of the simulation to some HTTP endpoint.
    enabled = false;             # whether HTTP reporting is enabled.
    url = "";                    # The URL to POST to.
    auth_user = "";              # username for HTTP basic auth
    auth_pass = "";              # password for HTTP basic auth
    parameters: {                # All parameters in this http_reporting.parameters are sent as additional payload.

    }
}