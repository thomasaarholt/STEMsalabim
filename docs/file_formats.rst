File formats
============

A *STEMsalabim* simulation is set-up via **input files** and its results are stored in an **output file**. The file for
configuring a simulation is described in detail at :ref:`parameter-file`. Here, we describe the format of the **crystal
file**, i.e., the atomic information about the specimen, and the **output file**, in which the results are stored.

.. _crystal-file:

Crystal file format
-------------------

The crystal file is expected to be in `XYZ format <https://en.wikipedia.org/wiki/XYZ_file_format>`_.

1. The **first line** contains the number of atoms.

2. The **second line** contains the cell dimension in x,y,z direction as floating point numbers in units of ``nm``,
   separated by a space. Optionally, it can contain the x, y, z dimensions in the .exyz format: ::

      Lattice="lx 0.0 0.0 0.0 ly 0.0 0.0 0.0 lz"

3. The atomic information is given from the **third line onwards**, with each line corresponding to a single atom. Each
   line must have *exactly 5 columns*:

    - The atomic species as elemental abbreviation (e.g., ``Ga`` for gallium)
    - the x,y,z coordinates as floating point numbers in units of ``nm``
    - the mean square displacement for the frozen lattice dislocations in units of :math:`nm^2`.
    - (**optional**) The id of the slice this atom belongs to. This can be used to do custom slicing.

Below is a very brief, artificial example (without custom slicing): ::

    5
    1.0 2.0 10.0
    Ga  0.0  0.0   0.0   1e-5
    P   0.2  0.1   0.0   2e-5
    Ga  0.0  0.0   1.0   1e-5
    P   1.2  0.1   0.0   2e-5
    O   1.0  2.0  10.0   0.0


.. note:: Atomic coordinates outside of the cell are periodically wrapped in ``x`` and ``y`` and clipped to the
   simulation box in ``z`` direction!

.. _output-file:

Output file format
------------------

All results of a *STEMsalabim* simulation are written to a binary `NetCDF <https://en.wikipedia.org/wiki/NetCDF>`_ file.
The NetCDF format is based on the `Hierarchical Data Format <https://en.wikipedia.org/wiki/Hierarchical_Data_Format>`_
and there are libraries to read the data for many programming languages.

The structure of NetCDF files is hierarchical and organized in groups. The following groups are written by
*STEMsalabim*:

runtime
~~~~~~~

This group contains information about the program and the simulation, such as version, UUID and so on.

AMBER
~~~~~

This group contains the atomic coordinates, species, displacements, radii, etc. for the complete crystal for each single
calculated frozen lattice configuration, as well as for each calculated defocus value. The AMBER group content is
compatible with the `AMBER specifications <http://ambermd.org/netcdf/nctraj.xhtml>`_. A *STEMsalabim* NetCDF file can
be opened seamlessly with the `Ovito <http://www.ovito.org/>`_ crystal viewer.

params
~~~~~~

All simulation parameters are collected in the ``params`` group as attributes.

adf
~~~

This group contains the simulated ADF intensities, the coordinates of the electron probe beam during scanning, the
detector angle grid that is used, and coordinates of the slices as used in the multi-slice algorithm.

cbed
~~~~

This group contains the simulated CBED intensities, the coordinates of the electron probe beam during scanning, k-space
grid, and coordinates of the slices as used in the multi-slice algorithm.

Reading NC Files
----------------

For a detailed view of the structure, we suggest using the `ncdump
<http://www.unidata.ucar.edu/software/netcdf//old_docs/docs_4_1/netcdf/ncdump.html>`_ utility: ``ncdump -h
some_results_file.nc``. As the underlying file format of NetCDF is HDF5, you may use any other HDF5 viewer to have a
look at the results.

There are NetCDF bindings for most popular programming languages.

1. In MATLAB, we recommend using ``h5read()`` and the `other HDF5 functions
   <https://de.mathworks.com/help/matlab/high-level-functions.html>`_.
2. For Python, use the
   `netCDF4 <http://unidata.github.io/netcdf4-python/>`_ module.
