/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#ifndef STEMSALABIM_ATOM_HPP
#define STEMSALABIM_ATOM_HPP


#include <utility>
#include "Element.hpp"

namespace stemsalabim { namespace atomic {

    /*!
     * Class to represent an atom at a fixed position in space. Holds information about the
     * chemical element, position, a unique ID and the thermal mean square displacement of
     * the frozen phonon vibrations.
     */
    class Atom {

    public:
        /*!
         * Empty constructor
         */
        Atom() = default;

        /*!
         * Constructor with initial values
         * @param x x coordinate of the atom
         * @param y y coordinate of the atom
         * @param z z coordinate of the atom
         * @param msd mean square displacement in the Einstein solid of the atom
         * @param el the Element type
         * @param id some identifier
         */
        Atom(double x, double y, double z, double msd, std::shared_ptr<Element> el, size_t id)
                : _x(x)
                , _y(y)
                , _z(z)
                , _element(std::move(el))
                , _msd(msd)
                , _id(id) {

        };

        /*!
         * Constructor with initial values
         * @param x x coordinate of the atom
         * @param y y coordinate of the atom
         * @param z z coordinate of the atom
         * @param msd mean square displacement in the Einstein solid of the atom
         * @param el the Element type
         * @param id some identifier
         */
        Atom(double x, double y, double z, double msd, std::shared_ptr<Element> el, size_t id, unsigned int slice)
                : _x(x)
                , _y(y)
                , _z(z)
                , _element(std::move(el))
                , _msd(msd)
                , _id(id)
                , _slice(slice)
                , _has_slice(true) {

        };

        /*!
         * Get the x coordinate
         * @return x coordinate of the atom
         */
        double getX() const {
            return _x;
        }

        /*!
         * Get the y coordinate
         * @return y coordinate of the atom
         */
        double getY() const {
            return _y;
        }

        /*!
         * Get the z coordinate
         * @return z coordinate of the atom
         */
        double getZ() const {
            return _z;
        }

        /*!
         * Get the mean square displacement
         * @return mean square displacement of the atom
         */
        double getMSD() const {
            return _msd;
        }

        /*!
         * Get the ID of the atom
         * @return id
         */
        size_t getId() const {
            return _id;
        }

        /*!
         * Get the custom slice of the atom
         * @return id
         */
        unsigned int getSlice() const {
            return _slice;
        }

        /*!
         * true if has custom slice
         * @return true if custom slice
         */
        bool hasSlice() const {
            return _has_slice;
        }

        /*!
         * Get the element of the atom
         * @return element
         */
        std::shared_ptr<Element> getElement() const {
            return _element;
        }

    private:
        double _x;
        double _y;
        double _z;
        std::shared_ptr<Element> _element;
        double _msd;
        size_t _id;
        unsigned int _slice{0};
        bool _has_slice{false};
    };
}}

#endif //STEMSALABIM_ATOM_HPP
