/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#ifndef ELEMENT_HPP_
#define ELEMENT_HPP_


#include <string>
#include <iostream>
#include <sstream>
#include <map>

#ifdef __GNUG__
#if __GNUG__ > 6
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wimplicit-fallthrough"
#endif
#endif
#include "../3rdparty/jsoncpp/json/json.h"
#ifdef __GNUG__
#if __GNUG__ > 6
#pragma GCC diagnostic pop
#endif
#endif

#include "elements_json.hpp"
#include "../utilities/output.hpp"

namespace stemsalabim { namespace atomic {


    /*!
     * Class representing a chemical element along with its scattering parameters.
     */
    class Element {

    public:
        /*!
         * Constructor
         * @param name element full name
         * @param symbol element symbol / abbreviation
         * @param atomic_number The atomic number Z
         */
        Element(std::string name, std::string symbol, unsigned atomic_number)
                : _name(name)
                , _symbol(symbol)
                , _atomic_number(atomic_number)
                , _scattering_params_dt(12) {
        };

        /*!
         * Default constructor needed for std::map
         */
        Element() = default;

        /*!
         * Get the element's symbolic description.
         * @return symbol
         */
        std::string symbol() const {
            return _symbol;
        }

        /*!
         * Get the full name of the element.
         * @return name
         */
        std::string name() const {
            return _name;
        }

        /*!
         * Get the atomic number of the element.
         * @return atomic number
         */
        unsigned int atomicNumber() const {
            return _atomic_number;
        }

        /*!
         * Get the atomic weight of the element.
         * @return atomic weight
         */
        float atomicWeight() const {
            return _atomic_weight;
        }

        /*!
         * Set atomic weight of the element.
         * @param atomic_weight atomic weight
         */
        void setAtomicWeight(float atomic_weight) {
            _atomic_weight = atomic_weight;
        }

        /*!
         * Get atomic radius of the element.
         * @return atomic radius
         */
        float atomicRadius() const {
            return _atomic_radius;
        }

        /*!
         * Set atomic radius of the element.
         * @param atomic_radius atomic radius
         */
        void setAtomicRadius(const float atomic_radius) {
            _atomic_radius = atomic_radius;
        }

        /*!
         * Set the Doyle Turner scattering parameters of the element.
         * They are encoded as a sequence in a vector of doubles, where
         * the elements are:
         * a1, b1, a2, b2, a3, b3, c1, d1, c2, d2, c3, d3
         * @param s scattering parameter vector
         */
        void setScatteringParamsDT(const std::vector<double> &s) {
            _scattering_params_dt = s;
        }

        /*!
         * Get the Doyle Turner scattering parameters of the element.
         * They are encoded as a sequence in a vector of doubles, where
         * the elements are:
         * a1, b1, a2, b2, a3, b3, c1, d1, c2, d2, c3, d3
         * @return scattering parameter vector
         */
        const std::vector<double> &scatteringParamsDT() const {
            return _scattering_params_dt;
        }

        /*!
         * Get the lattice constant of the element when in solid phase.
         * @return lattice constant
         */
        float latticeConstant() const {
            return _lattice_constant;
        }

        /*!
         * Set the lattice constant of the element when in solid phase.
         * @param lattice_constant lattice constant
         */
        void setLatticeConstant(const float lattice_constant) {
            _lattice_constant = lattice_constant;
        }

        /*!
         * Get a string representation of the lattice structure when crystallized.
         * @return lattice structure
         */
        const std::string &latticeStructure() const {
            return _lattice_structure;
        }

        /*!
         * Set a string representation of the lattice structure when crystallized.
         * @param lattice_structure lattice structure
         */
        void setLatticeStructure(std::string const &lattice_structure) {
            _lattice_structure = lattice_structure;
        }

    private:
        std::string _name;
        std::string _symbol;
        unsigned _atomic_number;
        float _atomic_weight;
        float _atomic_radius;
        float _lattice_constant;
        std::vector<double> _scattering_params_dt;
        std::string _lattice_structure;
    };

/*!
 * Singleton class for reading and storing elements via the elements_json.hpp file.
 */
    class ElementProvider {
    public:
        /*!
         * The only way to get an instance of ElementProvider is via this function.
         * @return Singleton instance of ElementProvider
         */
        static ElementProvider &getInstance() {
            static ElementProvider instance;
            return instance;
        }

        /*!
         * Return an Element via its symbol string
         * @param symbol the symbol
         * @return the Element with the symbol
         */
        std::shared_ptr<Element> elementBySymbol(std::string const &symbol) {
            return _elements[symbol];
        }

        /*!
         * Check if element is known by its symbol
         * @param symbol the symbol
         * @return true if element exists.
         */
        bool hasElement(std::string const &symbol) {
            return _elements.count(symbol) > 0;
        }

    private:
        /*!
         * Private constructor to be called only once.
         * Here, we read element data via elements_json.hpp
         */
        ElementProvider() {

            if(ElementProvider::_elements.empty()) {

                Json::Value root;
                Json::Reader reader;
                bool parsingSuccessful = reader.parse(elements_json, root, false);
                if(!parsingSuccessful) {
                    output::error("Failed to parse elements json");
                }

                for(Json::ValueIterator itr = root.begin(); itr != root.end(); itr++) {
                    auto &val = (*itr);
                    std::shared_ptr<Element> e(new Element(itr.key().asString(),
                                                           val.get("symbol", "").asString(),
                                                           val.get("atomic_number", 0).asUInt()));

                    e->setLatticeStructure(val.get("lattice_structure", "").asString());
                    e->setAtomicWeight(val.get("atomic_weight", 0.f).asFloat());

                    try {
                        // convert pm to angstrom
                        e->setAtomicRadius(val.get("atomic_radius pm", 0.f).asFloat() / 100);
                    } catch(const Json::LogicError &err) {
                        e->setAtomicRadius(0.0);
                    }

                    try {
                        e->setLatticeConstant(val.get("lattice_constant ang", 0.f).asFloat());
                    } catch(const Json::LogicError &err) {
                        e->setLatticeConstant(0.0);
                    }

                    std::vector<double> scattering_params_dt(12);

                    scattering_params_dt[0] = val["scattering_dt"].get("a1", 0).asDouble();
                    scattering_params_dt[1] = val["scattering_dt"].get("b1", 0).asDouble();
                    scattering_params_dt[2] = val["scattering_dt"].get("a2", 0).asDouble();
                    scattering_params_dt[3] = val["scattering_dt"].get("b2", 0).asDouble();
                    scattering_params_dt[4] = val["scattering_dt"].get("a3", 0).asDouble();
                    scattering_params_dt[5] = val["scattering_dt"].get("b3", 0).asDouble();
                    scattering_params_dt[6] = val["scattering_dt"].get("c1", 0).asDouble();
                    scattering_params_dt[7] = val["scattering_dt"].get("d1", 0).asDouble();
                    scattering_params_dt[8] = val["scattering_dt"].get("c2", 0).asDouble();
                    scattering_params_dt[9] = val["scattering_dt"].get("d2", 0).asDouble();
                    scattering_params_dt[10] = val["scattering_dt"].get("c3", 0).asDouble();
                    scattering_params_dt[11] = val["scattering_dt"].get("d3", 0).asDouble();

                    e->setScatteringParamsDT(scattering_params_dt);

                    _elements[val.get("symbol", "").asString()] = e;
                }
            }


        };

        std::map<std::string, std::shared_ptr<Element>> _elements;
    };

}}
#endif // ELEMENT_HPP_
