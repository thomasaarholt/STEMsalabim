/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#ifndef STEMSALABIM_PARAMS_HPP
#define STEMSALABIM_PARAMS_HPP

#include <string>
#include <tuple>
#include <libconfig.h++>
#include <random>
#include <cmath>
#include <algorithm>

namespace stemsalabim {

    /*!
     * A singleton class containing all simulation parameters, which are collected from command line
     * arguments and the input parameter file. In addition, it generates (if required) a random seed
     * as well as the simulation UUID.
     */
    class Params {
    public:
        /*!
         * Return the instance of the class, which is created if called for the first time.
         * @return Params instance.
         */
        static Params &getInstance() {
            static Params instance;
            return instance;
        }

        /*!
         * Initialize the parameters that are read from the command line, including most importantly
         * the location of the parameter file.
         * @param argc Number of arguments
         * @param argv char array of arguments
         */
        void initFromCLI(int argc, const char **argv);

        /*!
         * Return the generated universally unique identifier (UUID) of this simulation.
         * @return the UUID string
         */
        const std::string &uuid() const {
            return _uuid;
        }

        /*!
         * Return the command line arguments string, i.e., the command line call
         * minus the executable name.
         * @return Command line arguments string.
         */
        const std::string &cliArguments() const {
            return _command_line_arguments;
        }

        /*!
         * Return the file path to the parameters file.
         * @return parameters file path
         */
        const std::string &paramsFileName() const {
            return _param_file;
        };

        /*!
         * Read the simulation parameters from a string with the contents of the parameters file.
         * The file is not read in by the Param class itself to ensure that MPI processes other than the
         * master don't do IO.
         * @param prms string with the contents of the parameters file.
         */
        void readParamsFromString(const std::string &prms);

        /*!
         * Return the path to the crystal XYZ file.
         * @return path to crystal XYZ file.
         */
        const std::string &crystalFilename() const {
            return _crystal_file;
        }

        /*!
         * Return the path to the field XYZ file.
         * @return path to field XYZ file.
         */
        const std::string &fieldFilename() const {
            return _field_file;
        }

        /*!
         * Return path to the output file name.
         * @return path to output file.
         */
        const std::string &outputFilename() const {
            return _output_file;
        }

        /*!
         * Return path to the temporary directory. If empty string, use the
         * dir of the output file.
         * @return tmp dir path
         */
        const std::string &tmpDir() const {
            return _tmp_dir;
        }

        /*!
         * Returns if output should be compressed.
         * @return true if output should be compressed.
         */
        bool outputCompress() const {
            return _output_compress;
        }
        /*!
         * Return the title of the simulation.
         * @return title of the simulation
         */
        const std::string &title() const {
            return _title;
        }

        /*!
         * Return the Slice thickness of the multi-slice algorithm.
         * @return Slice thickness
         */
        double sliceThickness() const {
            return _slice_thickness;
        }

        /*!
         * Return the cutoff radius of the atomic potentials.
         * @return cutoff radius
         */
        double maxPotentialRadius() const {
            return _max_potential_radius;
        }

        /*!
         * Return the beam energy of the microscope.
         * @return beam enery
         */
        double beamEnergy() const {
            return _beam_energy;
        }

        /*!
         * Get the density of probe scan sampling in points/nm.
         * @return scan sampling density.
         */
        double probeDensity() const {
            return _probe_scan_density;
        }

        /*!
         * Return the max. numeric aperture of the electron probe.
         * @return max probe aperture
         */
        double probeMaxAperture() const {
            return _probe_max_aperture;
        }

        /*!
         * Return the min. numeric aperture of the electron probe.
         * @return min probe aperture
         */
        double probeMinAperture() const {
            return _probe_min_aperture;
        }

        /*!
         * Return the fifth order spherical aberration coefficient of the microscope.
         * @return 5th order spherical aberration coefficient.
         */
        double probeC5() const {
            return _probe_c5_coeff;
        }

        /*!
         * Return the third order spherical aberration coefficient of the microscope.
         * @return 3rd order spherical aberration coefficient.
         */
        double probePhericalAberrationCoeff() const {
            return _probe_spherical_aberration_coeff;
        }

        /*!
         * Get the FWHM part of the defocus information tuple.
         * @return defocus distribution FWHM.
         */
        double fwhmDefocus() const {
            return _probe_fwhm_defoci;
        }

        /*!
         * Get the center part of the defocus information tuple.
         * @return defocus distribution center.
         */
        double meanDefocus() const {
            return _probe_defocus;
        }

        /*!
         * Get the number part of the defocus information tuple.
         * @return defocus distribution number.
         */
        unsigned int numberOfDefoci() const {
            return _probe_num_defoci;
        }

        /*!
         * Return two-fold astigmatism coefficient.
         * @return two-fold astigmatism coefficient.
         */
        double probeAstigmatismCa() const {
            return _probe_astigmatism_ca;
        }

        /*!
         * Return two-fold astigmatism angle coefficient.
         * @return two-fold astigmatism angle coefficient.
         */
        double probeAstigmatismAngle() const {
            return _probe_astigmatism_angle;
        }

        /*!
         * Get the number of frozen phonon configurations.
         * @return number of FP configurations.
         */
        unsigned int numberOfConfigurations() const {
            return _number_configurations;
        }

        /*!
         * Get the (generated) random seed of this simulation.
         * @return random seed
         */
        unsigned int randomSeed() const {
            return _random_seed;
        }

        /*!
         * Get the density of real and phase space grating sampling in points/nm.
         * @return grating sampling density.
         */
        double samplingDensity() const {
            return _sample_density;
        }

        /*!
         * Get the ranom number generator.
         * @return  random number generator.
         */
        const std::mt19937 &getRandomGenerator() const {
            return _rgen;
        }

        /*!
         * Return whether slicing is fixed, i.e., the z coordinates of the Atom s are not
         * varied in FP vibrations.
         * @return true if slicing is fixed
         */
        bool isFixedSlicing() const {
            return _fixed_slicing;
        }

        /*!
         * Return whether simulation runs with double precision.
         * @return true if double precision
         */
        bool doublePrecision() const {
            return _double_precision;
        }

        /*!
         * Return whether wave functions are bandwidth limited.
         * @return true if bandwidth limiting is ON
         */
        bool bandwidthLimiting() const {
            return _bandwidth_limiting;
        }

        /*!
         * Return whether wave functions are renormalized after each slice.
         * @return true if wave functions are renormalized.
         */
        bool normalizeAlways() const {
            return _normalize_always;
        }

        /*!
         * Return whether diagnostic output should be printed.
         * @return true if diagnostics
         */
        bool printDiagnostics() const {
            return !_diagnostics_dir.empty();
        }

        /*!
         * Get directory of diagnostics output.
         * @return directory as string
         */
        std::string diagnosticsDir() const {
            return _diagnostics_dir;
        }

        /*!
         * Return whether HTTP reporting is enabled.
         * @return true if HTTP reporting is ON.
         */
        bool httpReporting() const {
            return _http_reporting;
        }

        /*!
         * Vector of key/value pairs that are sent as additional payload in HTTP reporting requests.
         * @return vector of additional key/value tuples in HTTP reporting requests.
         */
        const std::vector<std::tuple<std::string, std::string>> &httpParameters() const {
            return _http_parameters;
        }

        /*!
         * Return the HTTP reporting endpoint (URL).
         * @return HTTP reporting URL
         */
        const std::string &httpUrl() const {
            return _http_url;
        }

        /*!
         * Return the username for HTTP reporting basic auth.
         * @return username of HTTP basic auth
         */
        const std::string &httpAuthUser() const {
            return _http_auth_user;
        }

        /*!
         * Return the password for HTTP reporting basic auth.
         * @return password of HTTP basic auth
         */
        const std::string &httpAuthPass() const {
            return _http_auth_pass;
        }

        /*!
         * Return whether the multi-slice simulation is skipped, i.e., a dry-run is performed
         * writing only crystal coordinates to the output.
         * @return true if skip simulation
         */
        bool skipSimulation() const {
            return _skip_simulation;
        }

        /*!
         * Return the package of each thread of slave MPI processes,
         * i.e., how many pixels this thread calculates before reporting the results back to the master.
         * @return MPI work package size
         */
        unsigned int workPackageSize() const {
            return _work_package_size;
        }

        /*!
         * Return the wavelength of the electron beam in angstroms.
         * @return electron wavelength in angstrom.
         */
        double wavelength() const {
            return PLANCK_CONSTANT_SPEED_LIGHT / sqrt(_beam_energy * (2 * ELECTRON_REST_MASS + _beam_energy));
        }

        /*!
         * Return the \f$\gamma\f$ factor of the electron beam.
         * @return \f$\gamma\f$ factor
         */
        double gamma() const {
            return (1 + _beam_energy / ELECTRON_REST_MASS);
        }

        /*!
         * Return the number of threads per MPI process.
         * @return number of threads.
         */
        unsigned int numberOfThreads() const {
            return _num_threads;
        }

        /*!
         * Returns true if any ADF is to be saved at all.
         * @return true if any ADF is saved
         */
        bool adf() const {
            return _adf_enabled;
        }

        /*!
         * Get the scan boundaries in relative units (i.e., between 0 and 1)
         * of the STEM probe in x direction.
         * @return STEM scan x interval
         */
        const std::tuple<double, double> &adfScanX() const {
            return _adf_scan_x;
        }

        /*!
         * Get the scan boundaries in relative units (i.e., between 0 and 1)
         * of the STEM probe in y direction.
         * @return STEM scan y interval
         */
        const std::tuple<double, double> &adfScanY() const {
            return _adf_scan_y;
        }

        /*!
         * Get the detector angles interval and number, as a tuple containing
         * min detector angle, max detector angle, number of points.
         * @return tuple with detector angle information.
         */
        const std::tuple<double, double, unsigned int> &adfDetectorAngles() const {
            return _adf_detector_angles;
        }

        /*!
         * Get the number of detector angles to calculate intensities for.
         * @return number of detector angles
         */
        unsigned int adfNumberOfDetectorAngles() const {
            return std::get<2>(_adf_detector_angles);
        }

        /*!
         * Return the periodicity of slices after which intensities are written to the output file.
         * 0 - Only after the bottom of the sample
         * 1 - after each slice
         * N - after every Nth slice
         * @return periodicity of intensitiy output with respect to slices.
         */
        unsigned int adfSaveEveryNSlices() const {
            return _adf_save_every_n_slices;
        }

        /*!
         * Get the exponent of the detector interval sizes.
         * 1 - linear interval (default)
         * > 1 - adaptive interval (smaller angles -> finer sampling)
         * @return detector interval exponent
         */
        float adfDetectorIntervalExponent() const {
            return _adf_detector_interval_exponent;
        }

        /*!
         * Return whether FP configurations should be averaged in the output file.
         * @return true if FP configurations are averaged.
         */
        bool adfAverageConfigurations() const {
            return _adf_average_configurations;
        }

        /*!
         * Return whether different defoci simulations are averaged in the output file.
         * @return true if defoci are averaged.
         */
        bool adfAverageDefoci() const {
            return _adf_average_defoci;
        }

        /*!
         * Returns true if any CBED is to be saved at all.
         * @return true if any CBED is saved
         */
        bool cbed() const {
            return _cbed_enabled;
        }

        /*!
         * Get the scan boundaries in relative units (i.e., between 0 and 1)
         * of the CBEDs in x direction.
         * @return CBED scan x interval
         */
        const std::tuple<double, double> &cbedScanX() const {
            return _cbed_scan_x;
        }

        /*!
         * Get the scan boundaries in relative units (i.e., between 0 and 1)
         * of the CBEDs in y direction.
         * @return CBED scan y interval
         */
        const std::tuple<double, double> &cbedScanY() const {
            return _cbed_scan_y;
        }

        /*!
         * Get the desired size of the CBED images to store. Will return a tuple (0,0) if
         * not set as a parameter.
         * @return CBED desired size.
         */
        const std::tuple<unsigned int, unsigned int> &cbedSize() const {
            return _cbed_size;
        }

        /*!
         * Get the desired size of the CBED images to store in X direction. Will return 0 if
         * not set as a parameter.
         * @return CBED desired size in X direction
         */
        unsigned int cbedSizeX() const {
            return std::get<0>(_cbed_size);
        }

        /*!
         * Get the desired size of the CBED images to store in Y direction. Will return 0 if
         * not set as a parameter.
         * @return CBED desired size in Y direction
         */
        unsigned int cbedSizeY() const {
            return std::get<1>(_cbed_size);
        }

        /*!
         * Returns true if the desired CBED size is not 0,0, in which case the CBED
         * is to be rescaled before storage.
         * @return true if CBED is rescaled.
         */
        bool cbedRescale() const {
            return cbedSizeX() > 0 && cbedSizeY() > 0;
        }

        /*!
         * Return whether CBED FP configurations should be averaged in the output file.
         * @return true if CBED FP configurations are averaged.
         */
        bool cbedAverageConfigurations() const {
            return _cbed_average_configurations;
        }

        /*!
         * Return whether different CBED defoci simulations are averaged in the output file.
         * @return true if CBED defoci are averaged.
         */
        bool cbedAverageDefoci() const {
            return _cbed_average_defoci;
        }

        /*!
         * Return the periodicity of slices after which CBEDs are written to the output file.
         * 0 - Only after the bottom of the sample
         * 1 - after each slice
         * N - after every Nth slice
         * @return periodicity of CBED output with respect to slices.
         */
        unsigned int cbedSaveEveryNSlices() const {
            return _cbed_save_every_n_slices;
        }

        /// electron rest mass in keV. \f$m_0  c^2\f$ in keV
        constexpr static double ELECTRON_REST_MASS = 510.99906;

        /// product of planck's constant and speed of light in keV * Angstroms.
        constexpr static double PLANCK_CONSTANT_SPEED_LIGHT = 12.39841875;

        /// pi. Approximately.
        constexpr static double pi = 3.14159265358979323846;

        /// pi. Approximately.
        constexpr static double e = 1.602176462e-19;

    private:
        Params() = default;

        std::string _command_line_arguments{""};

        std::string _uuid{""};

        std::string _diagnostics_dir{""};
        bool _skip_simulation{false};
        double _sample_density{36.0};
        bool _output_compress{false};

        double _probe_max_aperture{24.0};
        double _probe_min_aperture{0.0};
        double _probe_c5_coeff{5e7};
        double _probe_spherical_aberration_coeff{2e4};
        double _probe_defocus{-20};
        unsigned int _probe_num_defoci{1};
        double _probe_fwhm_defoci{60};
        double _probe_astigmatism_angle{0};
        double _probe_astigmatism_ca{0};
        double _probe_scan_density{4}; // in A^-1

        double _beam_energy{200.0};

        double _max_potential_radius{3.0};

        double _slice_thickness{2.715};

        unsigned int _number_configurations{1};
        bool _fixed_slicing{true};

        std::string _crystal_file{""};
        std::string _param_file{""};
        std::string _output_file{""};
        std::string _tmp_dir{""};
        std::string _field_file{""};
        std::string _title{""};

        bool _bandwidth_limiting{true};
        bool _normalize_always{false};
        bool _double_precision{false};

        unsigned int _random_seed{0};
        std::mt19937 _rgen{0};
        unsigned _work_package_size{10};
        unsigned _num_threads{1};

        // Settings related to ADF mode
        bool _adf_enabled{true};
        std::tuple<double, double> _adf_scan_x{std::make_tuple(0.0, 1.0)};
        std::tuple<double, double> _adf_scan_y{std::make_tuple(0.0, 1.0)};
        unsigned int _adf_save_every_n_slices{1};
        std::tuple<double, double, unsigned int> _adf_detector_angles{std::make_tuple(1.0, 300.0, 300)};
        float _adf_detector_interval_exponent{1.0};
        bool _adf_average_configurations{true};
        bool _adf_average_defoci{true};

        // settings related to CBED mode
        bool _cbed_enabled{false};
        std::tuple<double, double> _cbed_scan_x{std::make_tuple(0.0, 1.0)};
        std::tuple<double, double> _cbed_scan_y{std::make_tuple(0.0, 1.0)};
        std::tuple<unsigned int, unsigned int> _cbed_size{std::make_tuple(0, 0)};
        unsigned int _cbed_save_every_n_slices{0};
        bool _cbed_average_configurations{true};
        bool _cbed_average_defoci{true};

        bool _http_reporting{false};
        std::string _http_url{""};
        std::string _http_auth_user{""};
        std::string _http_auth_pass{""};
        std::vector<std::tuple<std::string, std::string>> _http_parameters;

        libconfig::Config _cfg;

        std::vector<std::string> _cli_options;

        bool cliParamGiven(const std::string &p) {
            return std::find(_cli_options.begin(), _cli_options.end(), p) != _cli_options.end();
        }

    public:
        Params(Params const &) = delete;

        void operator=(Params const &)  = delete;
    };

}

#endif //STEMSALABIM_PARAMS_HPP
