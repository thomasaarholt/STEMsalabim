/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#include "IO.hpp"

#include <malloc.h>

#include "config.h"

#include "Simulation.hpp"

class NcException : public std::exception {
public:
    NcException(const char *message, const char *location, int line) {
        std::string loc = std::string(location);
        std::string m = std::string(message);
        msg = std::string(loc + ":" + std::to_string(line) + " " + m);
    }

    virtual const char *what() const throw() {
        return msg.c_str();
    }

private:
    std::string msg{""};
};

/// Check nc call and throw exception on error
inline void chk_nc(const char *location, int line, int retCode) {
    if(retCode == NC_NOERR)
        return;

    const char *msg = nullptr;
    if(NC_ISSYSERR(retCode)) {
        msg = strerror(retCode);
        msg = msg ? msg : "NetCDF: Unknown system error";
    } else {
        msg = nc_strerror(retCode);
    }

    throw NcException(msg, location, line);
}

#define CHK(code) chk_nc(__FILE__, __LINE__, code)

using namespace std;
using namespace stemsalabim;

/// write a const char array to an attribute
inline void att(int group, int var, const string &name, const char *val) {
    CHK(nc_put_att_text(group, var, name.c_str(), strlen(val), val));
}

/// write a const char array to a non-variable attribute
inline void att(int group, const string &name, const char *val) {
    att(group, NC_GLOBAL, name, val);
}

/// write a string to an attribute
inline void att(int group, int var, const string &name, const string &val) {
    CHK(nc_put_att_text(group, var, name.c_str(), val.length(), val.c_str()));
}

/// write a string to a non-variable attribute
inline void att(int group, const string &name, const string &val) {
    att(group, NC_GLOBAL, name, val);
}

/// write a bool to an attribute
inline void att(int group, int var, const string &name, bool val) {
    unsigned short v = val ? (unsigned short) 1 : (unsigned short) 0;
    CHK(nc_put_att_ushort(group, var, name.c_str(), NC_USHORT, 1, &v));
}

/// write a bool to a non-variable attribute
inline void att(int group, const string &name, bool val) {
    att(group, NC_GLOBAL, name, val);
}

/// write an int to an attribute
inline void att(int group, int var, const string &name, unsigned int val) {
    CHK(nc_put_att_uint(group, var, name.c_str(), NC_UINT, 1, &val));
}

/// write an int to a non-variable attribute
inline void att(int group, const string &name, unsigned int val) {
    att(group, NC_GLOBAL, name, val);
}

/// write a double to an attribute
inline void att(int group, int var, const string &name, double val) {
    CHK(nc_put_att_double(group, var, name.c_str(), NC_DOUBLE, 1, &val));
}

/// write a double to a non-variable attribute
inline void att(int group, const string &name, double val) {
    att(group, NC_GLOBAL, name, val);
}

/// write a double to an attribute
inline void att(int group, int var, const string &name, vector<double> val) {
    CHK(nc_put_att_double(group, var, name.c_str(), NC_DOUBLE, val.size(), val.data()));
}

/// write a double to a non-variable attribute
inline void att(int group, const string &name, vector<double> val) {
    att(group, NC_GLOBAL, name, val);
}

/// write a double to an attribute
inline void att(int group, int var, const string &name, vector<unsigned int> val) {
    CHK(nc_put_att_uint(group, var, name.c_str(), NC_UINT, val.size(), val.data()));
}

/// write a double to a non-variable attribute
inline void att(int group, const string &name, vector<unsigned int> val) {
    att(group, NC_GLOBAL, name, val);
}


/// write multiple strings to a non-variable attribute
inline void strAtt(int group, const string &name, const vector<string> &val) {
    vector<const char *> vc;
    transform(val.begin(), val.end(), back_inserter(vc), [](string x) -> char * {
        char *pc = new char[x.size() + 1];
        strcpy(pc, x.c_str());
        return pc;
    });
    CHK(nc_put_att_string(group, NC_GLOBAL, name.c_str(), 1, vc.data()));
}

/// get/put methods for numeric data.

inline void
getVara(int ncid, int var_id, const vector<size_t> &index, const vector<size_t> &count, vector<double> &vals) {
    CHK(nc_get_vara_double(ncid, var_id, index.data(), count.data(), vals.data()));
}

inline void
getVara(int ncid, int var_id, const vector<size_t> &index, const vector<size_t> &count, vector<float> &vals) {
    CHK(nc_get_vara_float(ncid, var_id, index.data(), count.data(), vals.data()));
}

inline void
putVara(int ncid, int var_id, const vector<size_t> &index, const vector<size_t> &count, const vector<float> &vals) {
    CHK(nc_put_vara_float(ncid, var_id, index.data(), count.data(), vals.data()));
}

inline void
putVara(int ncid, int var_id, const vector<size_t> &index, const vector<size_t> &count, const vector<double> &vals) {
    CHK(nc_put_vara_double(ncid, var_id, index.data(), count.data(), vals.data()));
}

inline void putVara(int ncid, int var_id, const vector<size_t> &index, const vector<size_t> &count, const float *vals) {
    CHK(nc_put_vara_float(ncid, var_id, index.data(), count.data(), vals));
}

inline void
putVara(int ncid, int var_id, const vector<size_t> &index, const vector<size_t> &count, const double *vals) {
    CHK(nc_put_vara_double(ncid, var_id, index.data(), count.data(), vals));
}


void IO::initNcFile(const shared_ptr<GridManager> &gridman, const shared_ptr<Crystal> &crystal) {
    Params &p = Params::getInstance();
    auto &mpi_env = mpi::Environment::getInstance();

    if(mpi_env.isSlave())
        output::error("This function should be called from the MPI master exclusively!\n");

    try {

        int id;

        if(_created)
            CHK(nc_open(p.outputFilename().c_str(), NC_WRITE, &id));
        else
            CHK(nc_create(p.outputFilename().c_str(), NC_NETCDF4 | NC_CLOBBER, &id));

        // quickly check, if the file contains valid data. If so, set _created true and exit this
        if(!_created) {

            // write the config file and command line parameters to the output
            typedef initializer_list<int> dl;

            // add the groups in the out NC file
            int runtime_id, amber_id, params_id;
            CHK(nc_def_grp(id, "runtime", &runtime_id));
            CHK(nc_def_grp(id, "AMBER", &amber_id));
            CHK(nc_def_grp(id, "params", &params_id));

            // add the dimensions of all groups
            int atom_dim, element_dim, spatial_dim, cell_spatial_dim, cell_angular_dim, label_dim, phonon_dim, defocus_dim;

            CHK(nc_def_dim(amber_id, "atom", crystal->numberOfAtoms(), &atom_dim));
            CHK(nc_def_dim(amber_id, "elements", crystal->numberOfElements(), &element_dim));
            CHK(nc_def_dim(amber_id, "spatial", 3, &spatial_dim));
            CHK(nc_def_dim(amber_id, "cell_spatial", 3, &cell_spatial_dim));
            CHK(nc_def_dim(amber_id, "cell_angular", 3, &cell_angular_dim));
            CHK(nc_def_dim(amber_id, "label", 6, &label_dim));
            CHK(nc_def_dim(amber_id, "frame", gridman->defoci().size() * p.numberOfConfigurations(), &phonon_dim));
            CHK(nc_def_dim(params_id, "defocus", p.numberOfDefoci(), &defocus_dim));

            if(p.adf()) {
                int adf_id;
                int position_x_dim, position_y_dim, detector_angle_dim, adf_defocus_dim, adf_phonon_dim, slice_dim;
                int intensities_var, probe_x_grid_var, probe_y_grid_var, detector_grid_var, slice_grid_var;

                auto adf_points_x = (unsigned int) gridman->adfXGrid().size();
                auto adf_points_y = (unsigned int) gridman->adfYGrid().size();

                // define ADF grouo
                CHK(nc_def_grp(id, "adf", &adf_id));

                // define ADF dimensions
                CHK(nc_def_dim(adf_id, "adf_position_x", adf_points_x, &position_x_dim));
                CHK(nc_def_dim(adf_id, "adf_position_y", adf_points_y, &position_y_dim));
                CHK(nc_def_dim(adf_id, "adf_detector_angle", p.adfNumberOfDetectorAngles(), &detector_angle_dim));
                CHK(nc_def_dim(adf_id, "adf_defocus", p.adfAverageDefoci() ? 1 : p.numberOfDefoci(), &adf_defocus_dim));
                CHK(nc_def_dim(adf_id,
                               "adf_phonon",
                               p.adfAverageConfigurations() ? 1 : p.numberOfConfigurations(),
                               &adf_phonon_dim));
                CHK(nc_def_dim(adf_id, "adf_slice", gridman->adfSliceCoords().size(), &slice_dim));

                // define ADF variables
                CHK(nc_def_var(adf_id,
                               "adf_intensities",
                               p.doublePrecision() ? NC_DOUBLE : NC_FLOAT,
                               6,
                               dl({adf_defocus_dim, position_x_dim, position_y_dim, adf_phonon_dim, slice_dim,
                                   detector_angle_dim}).begin(),
                               &intensities_var));
                CHK(nc_def_var(adf_id,
                               "adf_probe_x_grid",
                               NC_DOUBLE,
                               1,
                               dl({position_x_dim}).begin(),
                               &probe_x_grid_var));
                CHK(nc_def_var(adf_id,
                               "adf_probe_y_grid",
                               NC_DOUBLE,
                               1,
                               dl({position_y_dim}).begin(),
                               &probe_y_grid_var));
                CHK(nc_def_var(adf_id,
                               "adf_detector_grid",
                               NC_DOUBLE,
                               1,
                               dl({detector_angle_dim}).begin(),
                               &detector_grid_var));
                CHK(nc_def_var(adf_id, "adf_slice_coords", NC_DOUBLE, 1, dl({slice_dim}).begin(), &slice_grid_var));

                // ADF variable settings
                vs chunk_intensities({1, adf_points_x, adf_points_y, 1, 1, p.adfNumberOfDetectorAngles()});
                CHK(nc_def_var_chunking(adf_id, intensities_var, NC_CHUNKED, chunk_intensities.data()));

                if(p.outputCompress())
                    CHK(nc_def_var_deflate(adf_id, intensities_var, NC_NOSHUFFLE, 1, 1));
            }

            if(p.cbed()) {
                int cbed_id;
                int position_x_dim, position_y_dim, k_x_dim, k_y_dim, cbed_defocus_dim, cbed_phonon_dim, slice_dim;
                int cbed_intensity_var, cbed_x_grid_var, cbed_y_grid_var, x_grid_var, y_grid_var, slice_grid_cbed_var;

                auto cbed_points_x = (unsigned int) gridman->cbedXGrid().size();
                auto cbed_points_y = (unsigned int) gridman->cbedYGrid().size();

                unsigned int k_sampling_x = gridman->storedCbedSizeX();
                unsigned int k_sampling_y = gridman->storedCbedSizeY();

                // define CBED group
                CHK(nc_def_grp(id, "cbed", &cbed_id));

                // define CBED dimensions
                CHK(nc_def_dim(cbed_id, "cbed_position_x", cbed_points_x, &position_x_dim));
                CHK(nc_def_dim(cbed_id, "cbed_position_y", cbed_points_y, &position_y_dim));
                CHK(nc_def_dim(cbed_id, "cbed_k_x", k_sampling_x, &k_x_dim));
                CHK(nc_def_dim(cbed_id, "cbed_k_y", k_sampling_y, &k_y_dim));
                CHK(nc_def_dim(cbed_id,
                               "cbed_defocus",
                               p.cbedAverageDefoci() ? 1 : p.numberOfDefoci(),
                               &cbed_defocus_dim));
                CHK(nc_def_dim(cbed_id,
                               "cbed_phonon",
                               p.cbedAverageConfigurations() ? 1 : p.numberOfConfigurations(),
                               &cbed_phonon_dim));
                CHK(nc_def_dim(cbed_id, "cbed_slice", gridman->cbedSliceCoords().size(), &slice_dim));

                // define CBED variables
                CHK(nc_def_var(cbed_id,
                               "cbed_intensities",
                               p.doublePrecision() ? NC_DOUBLE : NC_FLOAT,
                               7,
                               dl({cbed_defocus_dim, position_x_dim, position_y_dim, cbed_phonon_dim, slice_dim,
                                   k_x_dim, k_y_dim}).begin(),
                               &cbed_intensity_var));

                CHK(nc_def_var(cbed_id,
                               "cbed_probe_x_grid",
                               NC_DOUBLE,
                               1,
                               dl({position_x_dim}).begin(),
                               &cbed_x_grid_var));
                CHK(nc_def_var(cbed_id,
                               "cbed_probe_y_grid",
                               NC_DOUBLE,
                               1,
                               dl({position_y_dim}).begin(),
                               &cbed_y_grid_var));
                CHK(nc_def_var(cbed_id, "cbed_x_grid", NC_DOUBLE, 1, dl({k_x_dim}).begin(), &x_grid_var));
                CHK(nc_def_var(cbed_id, "cbed_y_grid", NC_DOUBLE, 1, dl({k_y_dim}).begin(), &y_grid_var));

                CHK(nc_def_var(cbed_id,
                               "cbed_slice_coords",
                               NC_DOUBLE,
                               1,
                               dl({slice_dim}).begin(),
                               &slice_grid_cbed_var));

                // CBED variable settings
                vs chunk_cbeds({1, 1, 1, 1, 1, k_sampling_x, k_sampling_y});
                CHK(nc_def_var_chunking(cbed_id, cbed_intensity_var, NC_CHUNKED, chunk_cbeds.data()));

                if(p.outputCompress())
                    CHK(nc_def_var_deflate(cbed_id, cbed_intensity_var, NC_NOSHUFFLE, 1, 1));
            }

            // set up all variables
            int spatial_var, cell_spatial_var, cell_angular_var, coords_var, coords_lattice_var, cells_var, angles_var;
            int radii_var, msd_var, slices_var, elements_var, system_lengths_var, system_angles_var, atom_types_var;


            CHK(nc_def_var(amber_id, "spatial", NC_CHAR, 1, dl({spatial_dim}).begin(), &spatial_var));
            CHK(nc_def_var(amber_id, "cell_spatial", NC_CHAR, 1, dl({cell_spatial_dim}).begin(), &cell_spatial_var));
            CHK(nc_def_var(amber_id,
                           "cell_angular",
                           NC_CHAR,
                           2,
                           dl({cell_angular_dim, label_dim}).begin(),
                           &cell_angular_var));
            CHK(nc_def_var(amber_id,
                           "coordinates",
                           NC_FLOAT,
                           3,
                           dl({phonon_dim, atom_dim, spatial_dim}).begin(),
                           &coords_var));
            CHK(nc_def_var(amber_id,
                           "lattice_coordinates",
                           NC_FLOAT,
                           3,
                           dl({phonon_dim, atom_dim, spatial_dim}).begin(),
                           &coords_lattice_var));
            CHK(nc_def_var(amber_id,
                           "cell_lengths",
                           NC_FLOAT,
                           2,
                           dl({phonon_dim, cell_spatial_dim}).begin(),
                           &cells_var));
            CHK(nc_def_var(amber_id,
                           "cell_angles",
                           NC_FLOAT,
                           2,
                           dl({phonon_dim, cell_angular_dim}).begin(),
                           &angles_var));
            CHK(nc_def_var(amber_id, "radius", NC_FLOAT, 2, dl({phonon_dim, atom_dim}).begin(), &radii_var));
            CHK(nc_def_var(amber_id, "msd", NC_FLOAT, 2, dl({phonon_dim, atom_dim}).begin(), &msd_var));
            CHK(nc_def_var(amber_id, "slice", NC_INT, 2, dl({phonon_dim, atom_dim}).begin(), &slices_var));
            CHK(nc_def_var(amber_id, "element", NC_SHORT, 2, dl({phonon_dim, atom_dim}).begin(), &elements_var));
            CHK(nc_def_var(amber_id,
                           "system_lengths",
                           NC_FLOAT,
                           1,
                           dl({cell_spatial_dim}).begin(),
                           &system_lengths_var));
            CHK(nc_def_var(amber_id, "system_angles", NC_FLOAT, 1, dl({cell_spatial_dim}).begin(), &system_angles_var));
            CHK(nc_def_var(amber_id, "atom_types", NC_CHAR, 2, dl({element_dim, label_dim}).begin(), &atom_types_var));

            int defocus_grid_var, defocus_weights_var;
            CHK(nc_def_var(params_id, "defocus", NC_DOUBLE, 1, dl({defocus_dim}).begin(), &defocus_grid_var));
            CHK(nc_def_var(params_id,
                           "defocus_weights",
                           NC_DOUBLE,
                           1,
                           dl({defocus_dim}).begin(),
                           &defocus_weights_var));

            // write attributes
            att(runtime_id, "programVersionMajor", PKG_VERSION_MAJOR);
            att(runtime_id, "programVersionMinor", PKG_VERSION_MINOR);
            att(runtime_id, "programVersionPatch", PKG_VERSION_PATCH);
            att(runtime_id, "gitCommit", GIT_COMMIT_HASH);
            att(runtime_id, "title", p.title());
            att(runtime_id, "UUID", p.uuid());

            // write some meta information to the netCDF file.
            att(amber_id, "Conventions", "AMBER");
            att(amber_id, "ConventionVersion", "1.0");
            att(amber_id, "program", PKG_NAME);
            att(amber_id,
                "programVersion",
                output::fmt("%s.%s.%s", PKG_VERSION_MAJOR, PKG_VERSION_MINOR, PKG_VERSION_PATCH));
            att(amber_id, "title", p.title());

            // create the dimensions complying to the AMBER specs

            // set up chunking
            size_t chunk_size = crystal->numberOfAtoms();
            vs chunk_coords({1, chunk_size, 3});
            vs chunk_cells({1, 3});
            vs chunk_msd({1, chunk_size});
            vs chunk_elements({1, chunk_size, 1});

            CHK(nc_def_var_chunking(amber_id, coords_var, NC_CHUNKED, chunk_coords.data()));
            CHK(nc_def_var_chunking(amber_id, coords_lattice_var, NC_CHUNKED, chunk_coords.data()));
            CHK(nc_def_var_chunking(amber_id, cells_var, NC_CHUNKED, chunk_cells.data()));
            CHK(nc_def_var_chunking(amber_id, angles_var, NC_CHUNKED, chunk_cells.data()));
            CHK(nc_def_var_chunking(amber_id, elements_var, NC_CHUNKED, chunk_elements.data()));
            CHK(nc_def_var_chunking(amber_id, radii_var, NC_CHUNKED, chunk_elements.data()));
            CHK(nc_def_var_chunking(amber_id, msd_var, NC_CHUNKED, chunk_msd.data()));
            CHK(nc_def_var_chunking(amber_id, slices_var, NC_CHUNKED, chunk_elements.data()));

            if(p.outputCompress()) {
                CHK(nc_def_var_deflate(amber_id, coords_var, NC_NOSHUFFLE, 1, 1));
                CHK(nc_def_var_deflate(amber_id, coords_lattice_var, NC_NOSHUFFLE, 1, 1));
                CHK(nc_def_var_deflate(amber_id, cells_var, NC_NOSHUFFLE, 1, 1));
                CHK(nc_def_var_deflate(amber_id, angles_var, NC_NOSHUFFLE, 1, 1));
                CHK(nc_def_var_deflate(amber_id, elements_var, NC_NOSHUFFLE, 1, 1));
                CHK(nc_def_var_deflate(amber_id, radii_var, NC_NOSHUFFLE, 1, 1));
                CHK(nc_def_var_deflate(amber_id, msd_var, NC_NOSHUFFLE, 1, 1));
                CHK(nc_def_var_deflate(amber_id, slices_var, NC_NOSHUFFLE, 1, 1));
            }

            // fill variables with AMBER spec data
            const char angles_labels[3][6] = {"alpha", "beta", "gamma"};
            CHK(nc_put_var_text(amber_id, spatial_var, "xyz"));
            CHK(nc_put_var_text(amber_id, cell_spatial_var, "abc"));
            CHK(nc_put_var(amber_id, cell_angular_var, angles_labels));

            float system_lengths_data[3];
            system_lengths_data[0] = (float) crystal->sizeX() / 10.0f;
            system_lengths_data[1] = (float) crystal->sizeY() / 10.0f;
            system_lengths_data[2] = (float) crystal->sizeZ() / 10.0f;
            CHK(nc_put_var_float(amber_id, system_lengths_var, system_lengths_data));

            float system_angles_data[3];
            system_angles_data[0] = 0.0;
            system_angles_data[1] = 0.0;
            system_angles_data[2] = 0.0;
            CHK(nc_put_var_float(amber_id, system_angles_var, system_angles_data));

            // write the time and date of the simulation start
            att(runtime_id, "time_start", output::currentTimeString());

            // insert atom types
            unsigned long i = 0;
            for(shared_ptr<atomic::Element> el : crystal->elements()) {

                CHK(nc_put_vara_text(amber_id,
                                     atom_types_var,
                                     &vs({i++, 0})[0],
                                     &vs({1, el->symbol().size() + 1})[0],
                                     el->symbol().c_str()));
            }

            // variable attributes according to AMBER specs
            att(amber_id, coords_var, "unit", "nanometer");
            att(amber_id, radii_var, "unit", "nanometer");
            att(amber_id, cells_var, "unit", "nanometer");
            att(amber_id, angles_var, "unit", "degree");

            _created = true;

        }

        nc_close(id);

    } catch(NcException &e) {
        output::error("%s\n", e.what());
    }

}

void IO::initBuffers(const shared_ptr<GridManager> &gridman) {
    Params &p = Params::getInstance();

    if(p.adf()) {
        _adf_intensities_buffer.resize(gridman->adfDetectorGrid().size() * gridman->adfSliceCoords().size());
    }

    if(p.cbed()) {
        unsigned int k_sampling_x = gridman->storedCbedSizeX();
        unsigned int k_sampling_y = gridman->storedCbedSizeY();
        // resize the write buffer
        _cbed_intensities_buffer.resize(k_sampling_x * k_sampling_y * gridman->cbedSliceCoords().size());
    }
}


void IO::writeParams(const shared_ptr<GridManager> &gridman) {
    int id;
    Params &p = Params::getInstance();
    auto &mpi_env = mpi::Environment::getInstance();

    if(mpi_env.isSlave())
        output::error("This function should be called from the MPI master exclusively!\n");

    CHK(nc_open(p.outputFilename().c_str(), NC_WRITE, &id));

    try {
        string prms_string;
        ifstream t(p.paramsFileName());
        prms_string = string((istreambuf_iterator<char>(t)), istreambuf_iterator<char>());

        int g, g_app, g_sim, g_probe, g_specimen, g_grating, g_adf, g_cbed;
        int g_fp, g_http;

        CHK(nc_inq_grp_ncid(id, "params", &g));

        CHK(nc_def_grp(g, "application", &g_app));
        CHK(nc_def_grp(g, "simulation", &g_sim));
        CHK(nc_def_grp(g, "probe", &g_probe));
        CHK(nc_def_grp(g, "specimen", &g_specimen));
        CHK(nc_def_grp(g, "grating", &g_grating));
        CHK(nc_def_grp(g, "adf", &g_adf));
        CHK(nc_def_grp(g, "cbed", &g_cbed));
        CHK(nc_def_grp(g, "frozen_phonon", &g_fp));
        CHK(nc_def_grp(g, "http_reporting", &g_http));

        {
            att(g, "program_arguments", p.cliArguments());
            att(g, "config_file_contents", prms_string);
            att(g, "work_package_size", p.workPackageSize());
            att(g, "num_threads", p.numberOfThreads());
        }

        {
            att(g_app, "random_seed", p.randomSeed());
            att(g_app, "precision", p.doublePrecision() ? "double" : "single");
        }

        {
            att(g_sim, "title", p.title());
            att(g_sim, "normalize_always", p.normalizeAlways());
            att(g_sim, "bandwidth_limiting", p.bandwidthLimiting());
            att(g_sim, "skip_simulation", p.isFixedSlicing());
            att(g_sim, "output_file", p.outputFilename());
        }

        {
            att(g_probe, "c5", p.probeC5() / 10);
            att(g_probe, "cs", p.probePhericalAberrationCoeff() / 10);
            att(g_probe, "astigmatism_ca", p.probeAstigmatismCa() / 10);
            att(g_probe, "defocus", p.meanDefocus() / 10);
            att(g_probe, "fwhm_defoci", p.fwhmDefocus() / 10);
            att(g_probe, "num_defoci", p.numberOfDefoci());
            att(g_probe, "astigmatism_ca", p.probeAstigmatismCa() / 10);
            att(g_probe, "astigmatism_angle", p.probeAstigmatismAngle());
            att(g_probe, "min_apert", p.probeMinAperture());
            att(g_probe, "max_apert", p.probeMaxAperture());
            att(g_probe, "beam_energy", p.beamEnergy());
            att(g_probe, "scan_density", p.probeDensity() * 10);
        }

        {
            att(g_specimen, "max_potential_radius", p.maxPotentialRadius() / 10);
            att(g_specimen, "crystal_file", p.crystalFilename());
        }

        {
            att(g_grating, "density", p.samplingDensity() * 10);
            att(g_grating, "nx", gridman->samplingX());
            att(g_grating, "ny", gridman->samplingY());
            att(g_grating, "slice_thickness", p.sliceThickness() / 10);
        }

        {
            vector<double> adf_scan_x({get<0>(p.adfScanX()), get<1>(p.adfScanX())});
            vector<double> adf_scan_y({get<0>(p.adfScanY()), get<1>(p.adfScanY())});

            att(g_adf, "enabled", p.adf());
            att(g_adf, "x", adf_scan_x);
            att(g_adf, "y", adf_scan_y);
            att(g_adf, "detector_min_angle", get<0>(p.adfDetectorAngles()));
            att(g_adf, "detector_max_angle", get<1>(p.adfDetectorAngles()));
            att(g_adf, "detector_num_angles", get<2>(p.adfDetectorAngles()));
            att(g_adf, "detector_interval_exponent", p.adfDetectorIntervalExponent());
            att(g_adf, "average_configurations", p.adfAverageConfigurations());
            att(g_adf, "average_defoci", p.adfAverageDefoci());
            att(g_adf, "save_slices_every", p.adfSaveEveryNSlices());
        }

        {
            vector<double> cbed_scan_x({get<0>(p.cbedScanX()), get<1>(p.cbedScanX())});
            vector<double> cbed_scan_y({get<0>(p.cbedScanY()), get<1>(p.cbedScanY())});
            vector<unsigned int> cbed_size({p.cbedSizeX(), p.cbedSizeY()});

            att(g_cbed, "enabled", p.cbed());
            att(g_cbed, "x", cbed_scan_x);
            att(g_cbed, "y", cbed_scan_y);
            att(g_cbed, "size", cbed_size);
            att(g_cbed, "average_configurations", p.cbedAverageConfigurations());
            att(g_cbed, "average_defoci", p.cbedAverageDefoci());
            att(g_cbed, "save_slices_every", p.cbedSaveEveryNSlices());

        }

        {
            att(g_fp, "number_configurations", p.numberOfConfigurations());
            att(g_fp, "fixed_slicing", p.isFixedSlicing());
        }

        {
            att(g_http, "reporting", p.httpReporting());
            att(g_http, "url", p.httpUrl());
            // the other parameters are not written to output file for security.
        }

    } catch(NcException &e) {
        output::error("%s\n", e.what());
    }

    nc_close(id);
}


void IO::writeCrystal(const shared_ptr<FPConfManager> &fpman, const shared_ptr<Crystal> &crystal) {

    int id;
    auto &mpi_env = mpi::Environment::getInstance();

    try {

        if(mpi_env.isSlave())
            output::error("This function should be called from the MPI master exclusively!\n");

        CHK(nc_open(Params::getInstance().outputFilename().c_str(), NC_WRITE, &id));

        int amber_id, coords_var, lattice_coords_var, cells_var, angles_var, elements_var, radii_var;
        int msd_var, slices_var;

        CHK(nc_inq_grp_ncid(id, "AMBER", &amber_id));
        CHK(nc_inq_varid(amber_id, "coordinates", &coords_var));
        CHK(nc_inq_varid(amber_id, "lattice_coordinates", &lattice_coords_var));
        CHK(nc_inq_varid(amber_id, "cell_lengths", &cells_var));
        CHK(nc_inq_varid(amber_id, "cell_angles", &angles_var));
        CHK(nc_inq_varid(amber_id, "element", &elements_var));
        CHK(nc_inq_varid(amber_id, "radius", &radii_var));
        CHK(nc_inq_varid(amber_id, "msd", &msd_var));
        CHK(nc_inq_varid(amber_id, "slice", &slices_var));

        size_t n = crystal->numberOfAtoms();

        float cell_lengths[3];
        cell_lengths[0] = (float) crystal->sizeX() / 10.0f;
        cell_lengths[1] = (float) crystal->sizeY() / 10.0f;
        cell_lengths[2] = (float) crystal->sizeZ() / 10.0f;
        float cell_angles[3] = {90.f, 90.f, 90.f};

        short *elements_data = (short *) malloc(sizeof(short) * n);
        float *radii_data = (float *) malloc(sizeof(float) * n);
        float *msd_data = (float *) malloc(sizeof(float) * n);
        int *slice_data = (int *) malloc(sizeof(int) * n);
        float *coords_data = (float *) malloc(sizeof(float) * n * 3);
        float *lattice_coords_data = (float *) malloc(sizeof(float) * n * 3);

        unsigned int iconf = fpman->currentConfiguration();

        size_t i = 0;
        for(auto &atom: crystal->getAtoms()) {
            //@TODO: Make this nicer
            elements_data[i] = (short) distance(crystal->elements().begin(),
                                                crystal->elements().find(atom->getElement()));

            radii_data[i] = atom->getElement()->atomicRadius() / 10;
            msd_data[i] = (float) (atom->getMSD() / 100.);
            double z = atom->getZ() + fpman->dz(i);
            slice_data[i] = crystal->sliceIndex(z);

            coords_data[3 * i + 0] = (float) (atom->getX() + fpman->dx(i)) / 10;
            coords_data[3 * i + 1] = (float) (atom->getY() + fpman->dy(i)) / 10;
            coords_data[3 * i + 2] = (float) (atom->getZ() + fpman->dz(i)) / 10;

            lattice_coords_data[3 * i + 0] = (float) atom->getX() / 10;
            lattice_coords_data[3 * i + 1] = (float) atom->getY() / 10;
            lattice_coords_data[3 * i + 2] = (float) atom->getZ() / 10;
            i++;
        }

        CHK(nc_put_vara_float(amber_id, msd_var, &vs({iconf, 0})[0], &vs({1, n})[0], &msd_data[0]));
        CHK(nc_put_vara_float(amber_id, cells_var, &vs({iconf, 0})[0], &vs({1, 3})[0], &cell_lengths[0]));
        CHK(nc_put_vara_float(amber_id, angles_var, &vs({iconf, 0})[0], &vs({1, 3})[0], &cell_angles[0]));
        CHK(nc_put_vara_float(amber_id, coords_var, &vs({iconf, 0, 0})[0], &vs({1, n, 3})[0], &coords_data[0]));
        CHK(nc_put_vara_float(amber_id,
                              lattice_coords_var,
                              &vs({iconf, 0, 0})[0],
                              &vs({1, n, 3})[0],
                              &lattice_coords_data[0]));
        CHK(nc_put_vara_short(amber_id, elements_var, &vs({iconf, 0})[0], &vs({1, n})[0], &elements_data[0]));
        CHK(nc_put_vara_float(amber_id, radii_var, &vs({iconf, 0})[0], &vs({1, n})[0], &radii_data[0]));
        CHK(nc_put_vara_int(amber_id, slices_var, &vs({iconf, 0})[0], &vs({1, n})[0], &slice_data[0]));

        free(coords_data);
        free(lattice_coords_data);
        free(elements_data);
        free(radii_data);
        free(msd_data);
        free(slice_data);
    } catch(NcException &e) {
        output::error("%s\n", e.what());
    }
    nc_close(id);
}

void IO::writeAdfIntensities(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
        const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf) {
    unique_lock<mutex> qlck(_io_lock);

    Params &p = Params::getInstance();

    if(!point.adf || !point.hasAdfIntensities())
        return;

    int g_id, v_id, f_id;

    try {
        CHK(nc_open(p.outputFilename().c_str(), NC_WRITE, &f_id));
        CHK(nc_inq_grp_ncid(f_id, "adf", &g_id));
        CHK(nc_inq_varid(g_id, "adf_intensities", &v_id));

        IO::writeAdfIntensities(idefocus, iconf, gridman, point, ibuf, g_id, v_id);

    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
    nc_close(f_id);
}


void IO::writeAdfIntensities(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
        const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, int g_id, int v_id) {

    Params &p = Params::getInstance();

    try {
        unsigned long nangles = gridman->adfDetectorGrid().size();
        unsigned int n_slices_total = (unsigned int) gridman->adfSliceCoords().size();

        // intensities are ordered by rank and therefore internally by x and y and rad.
        // we can write all data for one frozen phonon configuration at once.

        if(p.adfAverageDefoci() && (iconf > 0 || idefocus > 0)) {

            // see http://stackoverflow.com/questions/9915653/ for how to calculate a weighted, running average

            double w = gridman->defocusWeights()[idefocus];

            getVara(g_id,
                    v_id,
                    vs({0, point.adf_row, point.adf_col, 0, 0, 0}),
                    vs({1, 1, 1, 1, n_slices_total, nangles}),
                    _adf_intensities_buffer);

            for(size_t j = 0; j < _adf_intensities_buffer.size(); ++j)
                _adf_intensities_buffer[j] = (_adf_intensities_buffer[j] * _running_weight +
                                              w * ibuf->value(point.adf_intensities, j)) / (_running_weight + w);

            putVara(g_id,
                    v_id,
                    vs({0, point.adf_row, point.adf_col, 0, 0, 0}),
                    vs({1, 1, 1, 1, n_slices_total, nangles}),
                    _adf_intensities_buffer);

            _running_weight += w;

        } else if(!p.adfAverageDefoci() && p.adfAverageConfigurations() && iconf > 0) {

            // no defocus average but configuration average

            getVara(g_id,
                    v_id,
                    vs({idefocus, point.adf_row, point.adf_col, 0, 0, 0}),
                    vs({1, 1, 1, 1, n_slices_total, nangles}),
                    _adf_intensities_buffer);

            for(size_t j = 0; j < _adf_intensities_buffer.size(); ++j)
                _adf_intensities_buffer[j] = (_adf_intensities_buffer[j] * iconf +
                                              ibuf->value(point.adf_intensities, j)) / (iconf + 1);

            putVara(g_id,
                    v_id,
                    vs({idefocus, point.adf_row, point.adf_col, 0, 0, 0}),
                    vs({1, 1, 1, 1, n_slices_total, nangles}),
                    _adf_intensities_buffer);

        } else {
            // no average, either when this is the first configuration over all or no averaging is chosen.
            putVara(g_id,
                    v_id,
                    vs({idefocus, point.adf_row, point.adf_col, iconf, 0, 0}),
                    vs({1, 1, 1, 1, n_slices_total, nangles}),
                    ibuf->ptr(point.adf_intensities));
        }

    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
}


void IO::writeCBEDIntensities(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
        const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &cbuf) {
    unique_lock<mutex> qlck(_io_lock);

    Params &p = Params::getInstance();

    if(!point.cbed || !point.hasCbedIntensities())
        return;

    int g_id, v_id, f_id;

    try {
        CHK(nc_open(p.outputFilename().c_str(), NC_WRITE, &f_id));
        CHK(nc_inq_grp_ncid(f_id, "cbed", &g_id));
        CHK(nc_inq_varid(g_id, "cbed_intensities", &v_id));

        writeCBEDIntensities(idefocus, iconf, gridman, point, cbuf, g_id, v_id);


    } catch(NcException &e) {
        output::error("CBED %s\n", e.what());
    }

    nc_close(f_id);
}

void IO::writeCBEDIntensities(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
        const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &cbuf, int g_id, int v_id) {
    Params &p = Params::getInstance();

    unsigned long nx = gridman->storedCbedSizeX();
    unsigned long ny = gridman->storedCbedSizeY();
    auto n_slices_total = (unsigned int) gridman->cbedSliceCoords().size();

    try {
        if(p.cbedAverageDefoci() && (iconf > 0 || idefocus > 0)) {

            // see http://stackoverflow.com/questions/9915653/ for how to calculate a weighted, running average

            double w = gridman->defocusWeights()[idefocus];

            getVara(g_id,
                    v_id,
                    vs({0, point.cbed_row, point.cbed_col, 0, 0, 0, 0}),
                    vs({1, 1, 1, 1, n_slices_total, nx, ny}),
                    _cbed_intensities_buffer);

            for(size_t j = 0; j < _cbed_intensities_buffer.size(); ++j)
                _cbed_intensities_buffer[j] = (_cbed_intensities_buffer[j] * _running_weight +
                                               w * cbuf->value(point.cbed_intensities, j)) / (_running_weight + w);

            putVara(g_id,
                    v_id,
                    vs({0, point.cbed_row, point.cbed_col, 0, 0, 0, 0}),
                    vs({1, 1, 1, 1, n_slices_total, nx, ny}),
                    _cbed_intensities_buffer);

            _running_weight += w;

        } else if(!p.cbedAverageDefoci() && p.cbedAverageConfigurations() && iconf > 0) {

            // no defocus average but configuration average

            getVara(g_id,
                    v_id,
                    vs({idefocus, point.cbed_row, point.cbed_col, 0, 0, 0, 0}),
                    vs({1, 1, 1, 1, n_slices_total, nx, ny}),
                    _cbed_intensities_buffer);

            for(size_t j = 0; j < _cbed_intensities_buffer.size(); ++j)
                _cbed_intensities_buffer[j] = (_cbed_intensities_buffer[j] * iconf +
                                               cbuf->value(point.cbed_intensities, j)) / (iconf + 1);

            putVara(g_id,
                    v_id,
                    vs({idefocus, point.cbed_row, point.cbed_col, 0, 0, 0, 0}),
                    vs({1, 1, 1, 1, n_slices_total, nx, ny}),
                    _cbed_intensities_buffer);

        } else {
            // no average, either when this is the first configuration over all or no averaging is chosen.
            putVara(g_id,
                    v_id,
                    vs({idefocus, point.cbed_row, point.cbed_col, iconf, 0, 0, 0}),
                    vs({1, 1, 1, 1, n_slices_total, nx, ny}),
                    cbuf->ptr(point.cbed_intensities));
        }


    } catch(NcException &e) {
        output::error("CBED %s\n", e.what());
    }
}

string getTempFileName() {
    Params &p = Params::getInstance();
    auto &mpi_env = mpi::Environment::getInstance();
    int mpi_rank = mpi_env.rank();

    return output::fmt("%s/%s_%d.bin", p.tmpDir(), p.uuid(), mpi_rank);
}

void IO::writeTemporaryResult(unsigned int idefocus, unsigned int iconf, ScanPoint &point,
        shared_ptr<memory::buffer::number_buffer<float>> &ibuf,
        shared_ptr<memory::buffer::number_buffer<float>> &cbuf) {

    auto &mpi_env = mpi::Environment::getInstance();
    int mpi_rank = mpi_env.rank();
    auto tmpfile = std::ofstream(getTempFileName(), std::ios::binary | std::ios::app);

    // the format is as follows:
    // int rank
    // for each pixel:
    //     unsigned int idefocus
    //     unsigned int iconf
    //     unsigned int index
    //     float[] adf_intensities
    //     float[] cbed_intensities

    if(tmpfile.fail()) {
        output::error("Couldn't open temporary binary %s file for writing.\n", getTempFileName());
    }

    if(tmpfile.tellp() == 0) {
        // empty file, write rank
        tmpfile.write(reinterpret_cast<const char *>(&mpi_rank), sizeof(mpi_rank));
    }

    tmpfile.write(reinterpret_cast<const char *>(&idefocus), sizeof(idefocus));
    tmpfile.write(reinterpret_cast<const char *>(&iconf), sizeof(iconf));
    tmpfile.write(reinterpret_cast<const char *>(&point.index), sizeof(point.index));

    if(point.hasAdfIntensities()) {
        tmpfile.write(reinterpret_cast<const char *>(ibuf->ptr(point.adf_intensities)),
                      point.adf_intensities.byte_size());
        point.clearAdfIntensities(ibuf);
    }

    if(point.hasCbedIntensities()) {
        tmpfile.write(reinterpret_cast<const char *>(cbuf->ptr(point.cbed_intensities)),
                      point.cbed_intensities.byte_size());
        point.clearCBEDIntensities(cbuf);
    }

    tmpfile.close();

}

void IO::copyFromTemporaryFile(const shared_ptr<GridManager> &gridman,
        shared_ptr<memory::buffer::number_buffer<float>> &ibuf,
        shared_ptr<memory::buffer::number_buffer<float>> &cbuf) {

    Params &prms = Params::getInstance();
    auto &mpi_env = mpi::Environment::getInstance();
    auto tmpfile = std::ifstream(getTempFileName(), std::ifstream::binary);

    tmpfile.seekg(0, std::ios::end);
    const int length = tmpfile.tellg();
    tmpfile.seekg(0, std::ios::beg);

    // first, read rank (and check, if it is correct...)
    int mpi_rank_from_file;
    tmpfile.read(reinterpret_cast<char *>(&mpi_rank_from_file), sizeof(mpi_rank_from_file));

    if(mpi_rank_from_file != mpi_env.rank()) {
        output::error("Reading wrong temporary file!\n");
    }

    int adf_g_id, adf_v_id, cbed_g_id, cbed_v_id, f_id;

    try {
        CHK(nc_open(prms.outputFilename().c_str(), NC_WRITE, &f_id));
        if(prms.adf()) {
            CHK(nc_inq_grp_ncid(f_id, "adf", &adf_g_id));
            CHK(nc_inq_varid(adf_g_id, "adf_intensities", &adf_v_id));
        }
        if(prms.cbed()) {
            CHK(nc_inq_grp_ncid(f_id, "cbed", &cbed_g_id));
            CHK(nc_inq_varid(cbed_g_id, "cbed_intensities", &cbed_v_id));
        }
    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }

    unsigned int idefocus;
    unsigned int iconf;
    unsigned int pix_idx;

    while((int) tmpfile.tellg() < length) {
        tmpfile.read(reinterpret_cast<char *>(&idefocus), sizeof(idefocus));
        tmpfile.read(reinterpret_cast<char *>(&iconf), sizeof(iconf));
        tmpfile.read(reinterpret_cast<char *>(&pix_idx), sizeof(pix_idx));

        ScanPoint p = gridman->scanPoints()[pix_idx];

        if(prms.adf() && p.adf) {
            auto &entry = ibuf->add_empty();
            tmpfile.read(reinterpret_cast<char *>(ibuf->ptr(entry)), entry.byte_size());
            p.storeAdfIntensities(entry);
            writeAdfIntensities(idefocus, iconf, gridman, p, ibuf, adf_g_id, adf_v_id);
            p.clearAdfIntensities(ibuf);
        }

        if(prms.cbed() &&p.cbed) {
            auto &entry = cbuf->add_empty();
            tmpfile.read(reinterpret_cast<char *>(cbuf->ptr(entry)), entry.byte_size());
            p.storeCBEDIntensities(entry);
            writeCBEDIntensities(idefocus, iconf, gridman, p, cbuf, cbed_g_id, cbed_v_id);
            p.clearCBEDIntensities(cbuf);
        }

    }

    tmpfile.close();

    nc_close(f_id);

    // remove the file, as all the data is now transferred.
    remove(getTempFileName().c_str());

}


void IO::writeGrids(const shared_ptr<GridManager> &gridman) {
    Params &p = Params::getInstance();
    int id;

    auto &mpi_env = mpi::Environment::getInstance();

    try {

        if(mpi_env.isSlave())
            output::error("This function should be called from the MPI master exclusively!\n");

        CHK(nc_open(p.outputFilename().c_str(), NC_WRITE, &id));

        if(p.adf()) {
            int adf_id, probe_x_grid_var, probe_y_grid_var, detector_grid_var, slice_coords_var;

            CHK(nc_inq_grp_ncid(id, "adf", &adf_id));

            CHK(nc_inq_varid(adf_id, "adf_probe_x_grid", &probe_x_grid_var));
            CHK(nc_inq_varid(adf_id, "adf_probe_y_grid", &probe_y_grid_var));
            CHK(nc_inq_varid(adf_id, "adf_detector_grid", &detector_grid_var));
            CHK(nc_inq_varid(adf_id, "adf_slice_coords", &slice_coords_var));

            vector<double> probe_x_grid = gridman->adfXGrid();
            vector<double> probe_y_grid = gridman->adfYGrid();
            vector<double> slice_coords = gridman->adfSliceCoords();
            vector<double> detector_grid = gridman->adfDetectorGrid();

            for(size_t j = 0; j < probe_x_grid.size(); ++j)
                probe_x_grid[j] /= 10;

            for(size_t j = 0; j < probe_y_grid.size(); ++j)
                probe_y_grid[j] /= 10;

            for(size_t j = 0; j < slice_coords.size(); ++j)
                slice_coords[j] /= 10;

            putVara(adf_id, probe_x_grid_var, vs({0}), vs({probe_x_grid.size()}), probe_x_grid);
            putVara(adf_id, probe_y_grid_var, vs({0}), vs({probe_y_grid.size()}), probe_y_grid);
            putVara(adf_id, detector_grid_var, vs({0}), vs({detector_grid.size()}), detector_grid);
            putVara(adf_id, slice_coords_var, vs({0}), vs({slice_coords.size()}), slice_coords);
        }

        if(p.cbed()) {
            int cbed_id, cbed_x_grid_var, cbed_y_grid_var, x_grid_var, y_grid_var, slice_cbed_coords_var;

            CHK(nc_inq_grp_ncid(id, "cbed", &cbed_id));

            CHK(nc_inq_varid(cbed_id, "cbed_probe_x_grid", &cbed_x_grid_var));
            CHK(nc_inq_varid(cbed_id, "cbed_probe_y_grid", &cbed_y_grid_var));
            CHK(nc_inq_varid(cbed_id, "cbed_x_grid", &x_grid_var));
            CHK(nc_inq_varid(cbed_id, "cbed_y_grid", &y_grid_var));
            CHK(nc_inq_varid(cbed_id, "cbed_slice_coords", &slice_cbed_coords_var));


            vector<double> cbed_k_grid_x = gridman->outputKXGrid();
            vector<double> cbed_k_grid_y = gridman->outputKYGrid();

            vector<double> cbed_x_grid = gridman->cbedXGrid();
            vector<double> cbed_y_grid = gridman->cbedYGrid();

            vector<double> slice_cbed_coords = gridman->cbedSliceCoords();

            for(size_t j = 0; j < cbed_x_grid.size(); ++j)
                cbed_x_grid[j] /= 10;

            for(size_t j = 0; j < cbed_y_grid.size(); ++j)
                cbed_y_grid[j] /= 10;

            for(size_t j = 0; j < slice_cbed_coords.size(); ++j)
                slice_cbed_coords[j] /= 10;

            putVara(cbed_id, x_grid_var, vs({0}), vs({cbed_k_grid_x.size()}), cbed_k_grid_x);
            putVara(cbed_id, y_grid_var, vs({0}), vs({cbed_k_grid_y.size()}), cbed_k_grid_y);

            putVara(cbed_id, cbed_x_grid_var, vs({0}), vs({cbed_x_grid.size()}), cbed_x_grid);
            putVara(cbed_id, cbed_y_grid_var, vs({0}), vs({cbed_y_grid.size()}), cbed_y_grid);
            putVara(cbed_id, slice_cbed_coords_var, vs({0}), vs({slice_cbed_coords.size()}), slice_cbed_coords);

        }

        // write defocus array
        int prms_grp_id, defocus_var_id, defocus_weights_var_id;
        CHK(nc_inq_grp_ncid(id, "params", &prms_grp_id));
        CHK(nc_inq_varid(prms_grp_id, "defocus", &defocus_var_id));
        CHK(nc_inq_varid(prms_grp_id, "defocus_weights", &defocus_weights_var_id));

        vector<double> defocus_grid = gridman->defoci();
        vector<double> defocus_weights = gridman->defocusWeights();
        for(size_t j = 0; j < defocus_grid.size(); ++j)
            defocus_grid[j] /= 10;
        putVara(prms_grp_id, defocus_var_id, vs({0}), vs({defocus_grid.size()}), defocus_grid);
        putVara(prms_grp_id, defocus_weights_var_id, vs({0}), vs({defocus_weights.size()}), defocus_weights);

    } catch(NcException &e) {
        output::error("%s\n", e.what());
    }

    nc_close(id);
}


IO::~IO() {
    int id, rt_id;

    try {

        auto &mpi_env = mpi::Environment::getInstance();

        if(!mpi_env.isMpi() || mpi_env.isMaster()) {
            CHK(nc_open(Params::getInstance().outputFilename().c_str(), NC_WRITE, &id));
            CHK(nc_inq_grp_ncid(id, "runtime", &rt_id));
            att(rt_id, "time_stop", output::currentTimeString());
        }

    } catch(NcException &e) {
        output::error("%s\n", e.what());
    }

    nc_close(id);
}


void IO::dumpWave(string filename, const Wave &wave) {
    FILE *f = fopen(filename.c_str(), "w");
    for(unsigned int ix = 0; ix < wave.lx(); ix++)
        for(unsigned int iy = 0; iy < wave.ly(); iy++)
            fprintf(f,
                    "%d %d %f %f %f\n",
                    ix,
                    iy,
                    wave(ix, iy).real(),
                    wave(ix, iy).imag(),
                    sqrt(pow(wave(ix, iy).real(), 2) + pow(wave(ix, iy).imag(), 2)));
    fclose(f);
}
