/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#ifndef STEMSALABIM_CRYSTAL_H
#define STEMSALABIM_CRYSTAL_H

#include <tuple>
#include <vector>
#include <set>
#include <complex>
#include <memory>
#include <cmath>
#include <stdlib.h>

#include "../libatomic/Atom.hpp"
#include "../libatomic/Element.hpp"
#include "../utilities/algorithms.hpp"
#include "Params.hpp"

namespace stemsalabim {


    class Slice;

    /*!
     * The Crystal class manages the crystal, consisting of Slice objects of the
     * multi-slice algorithm, references to all Atom objects and supercell size boundaries.
     * In addition, it contains the real and k-space grids on which the simulation's wave functions
     * are discretized.
     */

    class Crystal {

    public:
        /*!
         * Initialize the crystal. In this method, the XYZ file with the atomic positions and
         * supercell information is read, the K space grid is generated and the Slice vector is
         * initialized.
         * @param field_file_content string content of the crystal XYZ file.
         */
        void init(const std::string &field_file_content);

        void readFieldFile(const std::string &field_file_content);

        /*!
         * Returns a reference to the vector of pointers to all Atom objects.
         * @return atoms
         */
        const std::vector<std::shared_ptr<atomic::Atom>> &getAtoms() const {
            return _atoms;
        };

        /*!
         * return the x length of the crystal in angstroms
         * @return size x in angstroms
         */
        double sizeX() const {
            return _size_x;
        }

        /*!
         * return the y length of the crystal in angstroms
         * @return size y in angstroms
         */
        double sizeY() const {
            return _size_y;
        }

        /*!
         * return the z length of the crystal in angstroms
         * @return size z in angstroms
         */
        double sizeZ() const {
            return _size_z;
        }

        /*!
         * Get the total number of atoms in the crystal
         * @return number of atoms
         */
        unsigned long numberOfAtoms() const {
            return _atoms.size();
        }

        /*!
         * Get the number of different elements that appear in the crystal
         * @return number of different elements
         */
        unsigned long numberOfElements() const {
            return _elements.size();
        }

        /*!
         * Get a set of all elements contained in the crystal.
         * @return set of elements in the crystal
         */
        const std::set<std::shared_ptr<atomic::Element>> &elements() const {
            return _elements;
        }

        /*!
         * Get the number of slices of the multi-slice simulation
         * @return number of slices
         */
        unsigned int numberOfSlices() const {
            return (unsigned int) _slices.size();
        }

        /*!
         * Get a vector of pointers to the Slice objects of the multi-slice algorithm
         * @return Slice objects
         */
        const std::vector<std::shared_ptr<Slice>> &slices() const {
            return _slices;
        }

        /*!
         * Get a vector of pointers to the Slice objects of the multi-slice algorithm. const version
         * @return Slice objects
         */
        std::vector<std::shared_ptr<Slice>> &slices() {
            return _slices;
        }

        /*!
         * Assign the field points given in the field file to their corresponding slice.
         */
        void assignFieldsToSlices();

        /*!
         * For a given z coordinate, get the index of the slice that contains this z coordinate.
         * Negative z coordinates always result in slice 0, z coordinates that are larger than the
         * cell size in z direction result in the last slice's index.
         * @param z the coordinate
         * @return slice index
         */
        unsigned int sliceIndex(double z) const {
            Params &p = Params::getInstance();

            double dz = p.sliceThickness();
            double slice_top_offset = -dz / 8.0;
            auto slice_index = (int) floor((z - slice_top_offset) / dz);

            if(slice_index < 0)
                slice_index = 0;

            if(slice_index >= (int) numberOfSlices())
                slice_index = (int) numberOfSlices() - 1;

            return (unsigned int) slice_index;
        }


    private:
        std::vector<std::shared_ptr<atomic::Atom>> _atoms;
        std::vector<std::shared_ptr<Slice>> _slices;
        std::set<std::shared_ptr<atomic::Element>> _elements;
        std::vector<std::tuple<double, double, double, double>> _fields;
        double _size_x, _size_y, _size_z;

        /*!
         * Generates the slices
         */
        void generateSlices();

    };
}
#endif //STEMSALABIM_CRYSTAL_H
