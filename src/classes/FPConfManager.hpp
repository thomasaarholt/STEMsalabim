/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#ifndef STEMSALABIM_FROZENPHONONCONFIGURATIONMANAGER_HPP
#define STEMSALABIM_FROZENPHONONCONFIGURATIONMANAGER_HPP

#include "Crystal.hpp"

namespace stemsalabim {

    /*!
     * Management class of Frozen Phonon configurations.
     * It is responsible for generating and returning atomic displacements based on
     * the current configuration index.
     * @tparam float precision type, float or double
     */

    class FPConfManager {

    public:
        /*!
         * Constructor, sets member variables.
         * @param num_confs Number of FP configurations
         * @param crystal reference to the Crystal class
         */
        FPConfManager(std::shared_ptr<Crystal> &crystal)
                : _crystal(crystal)
                , _total_configurations(Params::getInstance().numberOfConfigurations() *
                                        Params::getInstance().numberOfDefoci())
                , _current_configuration(0) {}

        /*!
         * In this function the atomic displacements are generated based on their mean
         * square displacement parameters.
         * @param random_numbers A vector of sufficiently many random numbers to generate displacements.
         */
        void init(const std::vector<double> &random_numbers);

        /*!
         * Set the current FP configuration index.
         * @param iconf configuration index
         */
        void setConfiguration(unsigned int iconf) {
            _current_configuration = iconf;
        }

        /*!
         * Assign atoms to slices. This function is moved to FPConfManager as the slice
         * depends on the z coordinate of the atoms, which may be subject to displacements.
         */
        void assignAtomsToSlices();

        /*!
         * Get atomic displacement in x direction for the atom with id atom_id.
         * @param atom_id atom id to return
         * @return displacement in x direction
         */
        double dx(unsigned long atom_id) const {
            return std::get<0>(_displacements[_current_configuration][atom_id]);
        }

        /*!
         * Get atomic displacement in y direction for the atom with id atom_id.
         * @param atom_id atom id to return
         * @return displacement in y direction
         */
        double dy(unsigned long atom_id) const {
            return std::get<1>(_displacements[_current_configuration][atom_id]);
        }

        /*!
         * Get atomic displacement in z direction for the atom with id atom_id.
         * @param atom_id atom id to return
         * @return displacement in z direction
         */
        double dz(unsigned long atom_id) const {
            return std::get<2>(_displacements[_current_configuration][atom_id]);
        }

        /*!
         * Get atomic displacement in x direction for the atom with id atom_id and configuration index iconf.
         * @param iconf FP configuration index
         * @param atom_id atom id to return
         * @return displacement in x direction
         */
        double dx(unsigned int iconf, unsigned long atom_id) const {
            return std::get<0>(_displacements[iconf][atom_id]);
        }

        /*!
         * Get atomic displacement in y direction for the atom with id atom_id and configuration index iconf.
         * @param iconf FP configuration index
         * @param atom_id atom id to return
         * @return displacement in y direction
         */
        double dy(unsigned int iconf, unsigned long atom_id) const {
            return std::get<1>(_displacements[iconf][atom_id]);
        }

        /*!
         * Get atomic displacement in z direction for the atom with id atom_id and configuration index iconf.
         * @param iconf FP configuration index
         * @param atom_id atom id to return
         * @return displacement in z direction
         */
        double dz(unsigned int iconf, unsigned long atom_id) const {
            return std::get<2>(_displacements[iconf][atom_id]);
        }

        /*!
         * Return the number of FP configurations.
         * @return number of configurations
         */
        unsigned int numberOfConfigurations() const {
            return _total_configurations;
        }

        /*!
         * Get the current FP configuration.
         * @return current configuration index
         */
        unsigned int currentConfiguration() const {
            return _current_configuration;
        }

    private:
        std::shared_ptr<Crystal> _crystal;
        std::vector<std::vector<std::tuple<double, double, double>>> _displacements;

        unsigned int _total_configurations;
        unsigned int _current_configuration;
    };

}

#endif //STEMSALABIM_FROZENPHONONCONFIGURATIONMANAGER_HPP
