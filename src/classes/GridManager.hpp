/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#ifndef STEMSALABIM_GRIDMANAGER_HPP
#define STEMSALABIM_GRIDMANAGER_HPP

#include "Params.hpp"
#include "Crystal.hpp"
#include "../utilities/Wave.hpp"
#include "../utilities/memory.hpp"

namespace stemsalabim {

    /*!
     * Wrapper class that contains information about a single STEM scan point,
     * along with information where the related intensities are stored after
     * successful calculation.
     * @tparam float precision type, float or double
     */

    class ScanPoint {

    public:
        typedef typename std::shared_ptr<memory::buffer::number_buffer<float>> ptr_buf_t;

        /*!
         * Empty constructor needed for serialization
         */
        ScanPoint() = default;

        /*!
         * Constructpr
         * @param x real space x coordinate of the pixel
         * @param y real space y coordinate of the pixel
         * @param adf_row row index of the pixel
         * @param adf_col col index of the pixel
         * @param cbed_row cbed row index of the pixel
         * @param cbed_col cbed col index of the pixel
         * @param index index of the pixel
         * @param adf boolean, whether ADF intensities are calculated for this pixel
         * @param cbed boolean, whether CBED intensities are calculated for this pixel
         */
        ScanPoint(double x, double y, unsigned int adf_row, unsigned int adf_col, unsigned int cbed_row,
                unsigned int cbed_col, unsigned int index, bool adf, bool cbed)
                : adf_row(adf_row)
                , adf_col(adf_col)
                , cbed_row(cbed_row)
                , cbed_col(cbed_col)
                , x(x)
                , y(y)
                , index(index)
                , adf(adf)
                , cbed(cbed) {}

        /*!
         * Stores intensities in the member vector.
         * @param in intensities as a vector of float
         * @param buf memory buffer where the data is stored
         */
        void storeAdfIntensities(const std::vector<float> &in, ptr_buf_t &buf) {
            adf_intensities = buf->add(in);
            _adf_intensities_stored = true;
        }

        /*!
         * Stores CBED intensities in the member vector.
         * @param in CBED intensities as a vector of float
         * @param buf memory buffer where the data is stored
         */
        void storeCBEDIntensities(const std::vector<float> &in, ptr_buf_t &buf) {
            cbed_intensities = buf->add(in);
            _cbed_intensities_stored = true;
        }

        /*!
         * Stores intensities in the member vector given a pre-created buffer entry.
         * @param in intensities as a vector of float
         * @param buf memory buffer where the data is stored
         */
        void storeAdfIntensities(memory::buffer::entry<float> & buf_entry) {
            adf_intensities = buf_entry;
            _adf_intensities_stored = true;
        }

        /*!
         * Stores CBED intensities in the member vector  given a pre-created buffer entry.
         * @param in CBED intensities as a vector of float
         * @param buf memory buffer where the data is stored
         */
        void storeCBEDIntensities(memory::buffer::entry<float> & buf_entry) {
            cbed_intensities = buf_entry;
            _cbed_intensities_stored = true;
        }

        /*!
         * Clear intensities in the member vector.
         * @param buf memory buffer where the data is stored
         */
        void clearAdfIntensities(ptr_buf_t &buf) {
            buf->remove(adf_intensities);
            _adf_intensities_stored = false;
        }

        /*!
         * Clear CBED intensities in the member vector.
         * @param buf memory buffer where the data is stored
         */
        void clearCBEDIntensities(ptr_buf_t &buf) {
            buf->remove(cbed_intensities);
            _cbed_intensities_stored = false;
        }

        /*!
         * Check if CBED intensities are stored in this struct.
         * @return true if CBED intensities are stored.
         */
        bool hasCbedIntensities() const {
            return _cbed_intensities_stored;
        }

        /*!
         * Check if intensities are stored in this struct.
         * @return true if intensities are stored.
         */
        bool hasAdfIntensities() const {
            return _adf_intensities_stored;
        }

        // members are public, so we don't need getters/setters.

        unsigned int adf_row;
        unsigned int adf_col;
        unsigned int cbed_row;
        unsigned int cbed_col;
        double x;
        double y;
        unsigned int index;
        bool adf{false};
        bool cbed{false};

        bool _adf_intensities_stored{false};
        bool _cbed_intensities_stored{false};

        memory::buffer::entry<float> adf_intensities;
        memory::buffer::entry<float> cbed_intensities;

    };

    /*!
     * Management class for the various parameter sweeps/grids required in the simulation.
     * @tparam float precision type, float or double
     */

    class GridManager {

    public:
        explicit GridManager(const std::shared_ptr<Crystal> &crystal)
                : _crystal(crystal) {}

        /*!
         * Generate the STEM scan points, work packages, slice coordinates, detector grid, and defocus values.
         * Must be called before usage of the class.
         * @param crystal Crystal object reference.
         */
        void generateGrids();

        /*!
         * Get the discretized detector k points.
         * @return vector of k points representing the detector angles
         */
        const std::vector<double> adfDetectorGrid() const {
            return _detector_grid;
        }

        /*!
         * Get the z coordinates of the Slice objects of the multi-slice simulation.
         * @return vector of z coordinates of the slices
         */
        const std::vector<double> adfSliceCoords() const {
            return _adf_slice_coords;
        }

        /*!
         * Get the z coordinates of the Slice objects of the CBED output multi-slice simulation.
         * @return vector of z coordinates of the slices for CBED
         */
        const std::vector<double> cbedSliceCoords() const {
            return _cbed_slice_coords;
        }

        /*!
         * Return all ScanPoint objects for all the pixels that are calculated by the simulation.
         * @return vector of ScanPoints
         */
        const std::vector<ScanPoint> scanPoints() const {
            return _scan_points;
        }

        /*!
         * Get the real space coordinates of the discrete scan points in x direction.
         * @return scan x grid
         */
        const std::vector<double> &adfXGrid() const {
            return _adf_x_grid;
        }

        /*!
         * Get the real space coordinates of the discrete scan points in y direction.
         * @return scan y grid
         */
        const std::vector<double> &adfYGrid() const {
            return _adf_y_grid;
        }

        /*!
         * Get the real space coordinates of the discrete CBED points in x direction.
         * @return CBED x grid
         */
        const std::vector<double> &cbedXGrid() const {
            return _cbed_x_grid;
        }

        /*!
         * Get the real space coordinates of the discrete CBED points in y direction.
         * @return CBED y grid
         */
        const std::vector<double> &cbedYGrid() const {
            return _cbed_y_grid;
        }

        /*!
         * Whether the slice with specified id is stored in the adf output or not.
         * @param id the slice id
         * @return true, when slice is stored, otherwise false
         */
        bool adfStoreSlice(unsigned int id) const {
            return _adf_store_slice[id];
        }

        /*!
         * Whether the slice with specified id is stored in the cbed output or not.
         * @param id the slice id
         * @return true, when slice is stored, otherwise false
         */
        bool cbedStoreSlice(unsigned int id) const {
            return _cbed_store_slice[id];
        }

        /*!
         * Index of the ADF bin (detector angle array) of k space indices kx and ky.
         * @param ix k space index in x direction
         * @param iy k space index in y direction
         * @return index of the bin to store intensity in
         */
        int adfBinIndex(unsigned int ix, unsigned int iy) const {
            return _adf_bin_index[ix][iy];
        }

        /*!
         * Get the defocus values that are simulated.
         * @return vector of defoci (in Angstrom)
         */
        const std::vector<double> &defoci() const {
            return _defoci;
        }

        /*!
         * Generates the frequency space
         */
        void generateKSpace();

        /*!
         * Get the frequency / k value in x direction at grid point position i
         * @param i grid point position i
         * @return k-space value
         */
        double kx(unsigned int i) const {
            return _kx_space[i];
        }

        /*!
         * Get the frequency / k value in y direction at grid point position i
         * @param i grid point position i
         * @return k-space value
         */
        double ky(unsigned int i) const {
            return _ky_space[i];
        }

        /*!
         * Get the frequencies / k values in x direction
         * @return k-space values
         */
        const std::vector<double> &kx() const {
            return _kx_space;
        }

        /*!
         * Get the frequencies / k values in y direction
         * @return k-space values
         */
        const std::vector<double> &ky() const {
            return _ky_space;
        }

        /*!
         * Get the real space value in x direction at grid point position i
         * @param i grid point position i
         * @return real space value
         */
        double x(unsigned int i) const {
            return _x_space[i];
        }

        /*!
         * Get the real space value in y direction at grid point position i
         * @param i grid point position i
         * @return real space value
         */
        double y(unsigned int i) const {
            return _y_space[i];
        }


        // We need to calculate the scales in x and y direction explicitely, because the
        // grating should be an even number.

        /*!
         * Get the x scale to translate integer grid points into a real space distance
         * @return x scale in 1/A
         */
        double scaleX() const {
            return samplingX() / _crystal->sizeX();
        }

        /*!
         * Get the y scale to translate integer grid points into a real space distance
         * @return y scale in 1/A
         */
        double scaleY() const {
            return samplingY() / _crystal->sizeY();
        }

        /*!
         * Get the grid sampling in x direction. It is rounded to an even integer because
         * of better behaving fourier transforms.
         * @return x grid sampling
         */
        unsigned int samplingX() const {
            return (unsigned int) algorithms::round_even(Params::getInstance().samplingDensity() * _crystal->sizeX());
        }

        /*!
         * Get the grid sampling in y direction. It is rounded to an even integer because
         * of better behaving fourier transforms.
         * @return y grid sampling
         */
        unsigned int samplingY() const {
            return (unsigned int) algorithms::round_even(Params::getInstance().samplingDensity() * _crystal->sizeY());
        }

        /*!
         * Return a wave object to multiply other waves with, that
         * will bandwidth limit the wave if requested.
         * @return bandwidth limit mask
         */
        const Wave & getBandwidthMask() const {
            return _bandwidth_limit_mask;
        }


        /*!
         * Calculates and returns the k grid values that are actually stored in the output file in kx direction.
         * Units: mrad
         * @return the output kx grid in mrad
         */
        std::vector<double> outputKXGrid() const {
            Params &p = Params::getInstance();

            int bandwidth_lx = storedCbedSizeX(true);
            int final_lx = storedCbedSizeX();

            std::vector<double> tmp((unsigned long)bandwidth_lx);
            std::vector<double> outp((unsigned long)final_lx);

            for(int i = 0; i < bandwidth_lx; i++) {
                int ii = i;

                if(i > bandwidth_lx / 2)
                    ii = samplingX() - bandwidth_lx + i;

                // add to k array and rescale to mrad
                tmp[i] = _kx_space[ii] * p.wavelength() * 1e3;
            }

            if(final_lx == bandwidth_lx)
                return tmp;

            double min_kx = *std::min_element(std::begin(tmp), std::end(tmp));
            double max_kx = *std::max_element(std::begin(tmp), std::end(tmp));

            // if CBED is to be rescaled in X direction, calculate the scaled k values
            // here.
            for(int i = 0; i < final_lx; ++i) {
                if(i > final_lx / 2)
                    outp[i] = (i - final_lx) * (max_kx - min_kx) / storedCbedSizeY();
                else
                    outp[i] = i * (max_kx - min_kx) / storedCbedSizeY();
            }

            return outp;
        }

        /*!
         * Calculates and returns the k grid values that are actually stored in the output file in ky direction.
         * Units: mrad
         * @return the output ky grid in mrad
         */
        std::vector<double> outputKYGrid() const {
            Params &p = Params::getInstance();

            int bandwidth_ly = storedCbedSizeY(true);
            int final_ly = storedCbedSizeY();

            std::vector<double> tmp((unsigned long)bandwidth_ly);
            std::vector<double> outp((unsigned long)storedCbedSizeX());

            for(int i = 0; i < bandwidth_ly; i++) {
                int ii = i;

                if(i > bandwidth_ly / 2)
                    ii = samplingY() - bandwidth_ly + i;

                // add to k array and rescale to mrad
                tmp[i] = _ky_space[ii] * p.wavelength() * 1e3;
            }

            if(final_ly == bandwidth_ly)
                return tmp;

            double min_ky = *std::min_element(std::begin(tmp), std::end(tmp));
            double max_ky = *std::max_element(std::begin(tmp), std::end(tmp));

            // if CBED is to be rescaled in X direction, calculate the scaled k values
            // here.
            for(int i = 0; i < final_ly; ++i) {
                if(i > final_ly / 2)
                    outp[i] = (i - final_ly) * (max_ky - min_ky) / storedCbedSizeY();
                else
                    outp[i] = i * (max_ky - min_ky) / storedCbedSizeY();
            }

            return outp;
        }

        /*!
         * Get the relative weights of the defoci when averaging. The weights are
         * determined simulating a Gaussian profile around the center defocus. This
         * is a simulated chromatic aberration.
         * @return vector of defocus weights.
         */
        const std::vector<double> &defocusWeights() const {
            return _defocus_weights;
        }

        /*!
         * Get the size of the stored CBED, taking into account bandwidth limiting
         * and rescaling in X direction.
         * @return size of CBED in x direction
         */
        unsigned int storedCbedSizeX(bool ignore_resize = false) const {
            Params &p = Params::getInstance();
            if(p.cbedRescale() && !ignore_resize)
                return p.cbedSizeX();

            if(!p.bandwidthLimiting())
                return samplingX();

            return (unsigned int) ceil(samplingX() * 2. / 3.);
        }

        /*!
         * Get the size of the stored CBED, taking into account bandwidth limiting
         * and rescaling in Y direction.
         * @return size of CBED in y direction
         */
        unsigned int storedCbedSizeY(bool ignore_resize = false) const {
            Params &p = Params::getInstance();
            if(p.cbedRescale() && !ignore_resize)
                return p.cbedSizeY();

            if(!p.bandwidthLimiting())
                return samplingY();

            return (unsigned int) ceil(samplingY() * 2. / 3.);
        }


    private:
        const std::shared_ptr<Crystal> &_crystal;

        std::vector<double> _detector_grid;
        std::vector<double> _adf_slice_coords;
        std::vector<double> _cbed_slice_coords;
        std::vector<double> _defoci;
        std::vector<double> _defocus_weights;
        std::vector<ScanPoint> _scan_points;

        std::vector<double> _kx_space;
        std::vector<double> _ky_space;
        std::vector<double> _x_space;
        std::vector<double> _y_space;

        std::vector<bool> _adf_store_slice;
        std::vector<bool> _cbed_store_slice;

        std::vector<std::vector<int>> _adf_bin_index;

        std::vector<double> _adf_x_grid;
        std::vector<double> _adf_y_grid;
        std::vector<double> _cbed_x_grid;
        std::vector<double> _cbed_y_grid;

        Wave _bandwidth_limit_mask;
    };

}

#endif //STEMSALABIM_GRIDMANAGER_HPP
