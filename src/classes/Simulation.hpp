/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#ifndef STEMSALABIM_SIMULATION_HPP
#define STEMSALABIM_SIMULATION_HPP

#include <memory>


#include "FPConfManager.hpp"
#include "GridManager.hpp"
#include "Slice.hpp"
#include "Params.hpp"
#include "IO.hpp"
#include "../utilities/mpi.hpp"
#include "../utilities/TaskQueue.hpp"
#include "../utilities/output.hpp"
#include "SimulationState.hpp"

namespace stemsalabim {

    /*!
     * The main simulation class. Carries out a *STEMsalabim* simulation.
     * @tparam float precision type, float or double
     */

    class Simulation {
    public:

        /*!
         * Initializes the simulation, reads parameter and sets up all the member pointers etc.
         */
        void init();

        /*!
         * This is the main simulation loop. It contains the main loops (frozen phonon, defocus, pixels)
         * and handles the distribution of tasks to MPI workers and threads.
         */
        void run();


    private:
        enum STATUS_SIGNAL { START_SIMULATION, START_DEFOCUS, START_CONFIGURATION, PROGRESS, FINISH_SIMULATION };

        /// The FPConfManager instance of the frozen phonon simulation.  Takes care of displacing atoms etc.
        std::shared_ptr<FPConfManager> _fpman;

        /// The GridManager that manages all required sweeps and grids.
        std::shared_ptr<GridManager> _gridman;

        /// The Crystal object, that holds all the slices of the multi-slice method
        std::shared_ptr<Crystal> _crystal;

        /// all IO functions, i.e., NC results file management.
        std::shared_ptr<IO> _io;

        /// A timer to prevent sending HTTP requests too often.
        std::chrono::high_resolution_clock::time_point _http_request_timer{std::chrono::high_resolution_clock::now()};

        /// The propagator can be represented as two 1D functions, one for each
        /// spatial direction.
        std::vector<std::complex<float>> _propagator_x;
        std::vector<std::complex<float>> _propagator_y;
        Wave _propagator;

        /// these buffers temporarily hold intensity data as continuous storage. This is to prevent
        /// memory fragmentation errors.
        std::shared_ptr<memory::buffer::number_buffer<float>> _adf_intensity_buffer;
        std::shared_ptr<memory::buffer::number_buffer<float>> _cbed_intensity_buffer;
        std::vector<char> _serialization_buffer;

        /*!
         * Carries out a multi-slice simulation for a single probe position
         * and a specific defocus value. Returns a vector with the intensities, that is aligned by slices * angles, i.e.,
         * having M angles and N slices, [0, M[ are the angular intensity of slice 0, [M, 2M[ the second slice etc.
         * @param point the Pixel to calculate
         * @param defocus the defocus in nm.
         * @return vector of the intensities for each slice and detector angle.
         */
        void calculatePixel(ScanPoint &point, const double defocus);

        /*!
         * The part of the multi-slice simulation that is carried out by the MPI master, i.e., rank 0.
         * Most importantly, the master orchestrates the distribution of tasks to the MPI slaves. Work
         * packages are generated, sent to slaves, received, deserialized and stored in the corresponding
         * result structures. However, additionally the master also carries out some multi-slice simulations
         * when more than one thread is available.
         * @param st The SimulationState object of the current simulation state.
         */
        void multisliceMaster(const SimulationState &st);

        /*!
         * This is the part of the multi-slice simulations that is carried out by the MPI slaves,
         * who wait for pixel indices to process, calculate the results, serialize them and submit
         * them to the MPI master. Each slave has its own little taskq queue that is worked.
         * @param st The SimulationState object of the current simulation state.
         */
        void multisliceWorker(const SimulationState &st);

        /*!
         * Iterates the work_packages array and writes finished pixels to the output file. Should only be called from
         * MPI rank 0 and is not thread safe, so only use it from the main thread!
         * @param st The SimulationState object of the current simulation state.
         * @param work_packages The work packages, containing all STEM scan points. As the queues are
         *                      index-based, a reference to this structure is required for information.
         * @param indices vector of the indices to store.
         */
        void storeFinishedPixels(const SimulationState &st, std::vector<ScanPoint> &work_packages,
                const std::vector<unsigned int> &indices);

        /*!
         * Generate the propagator function for propagation of the wave between the slices.
         */
        void generatePropagator();

        /*!
         * Print a line to stdout, with some additional information such as MPI rank, defocus and configuration etc.
         * @param st The SimulationState object of the current simulation state.
         * @param line the string to print.
         */
        void printLine(const SimulationState &st, const std::string &line);

        /*!
         * Print a line to stdout, with some additional information such as MPI rank, defocus and configuration etc.
         * Line is printed only when this is the master MPI process.
         * @param st The SimulationState object of the current simulation state.
         * @param line the string to print.
         */
        void printMaster(const SimulationState &st, const std::string &line);

        /*!
         * Print a line to stdout, with some additional information such as MPI rank, defocus.
         * Line is printed only when this is the master MPI process.
         * @param line the string to print.
         */
        void printMaster(const std::string &line);

        /*!
         * propagate and bandwidth limit a wave one slice.
         * @param wave the wave to propagate.
         * @param do_backward_fft Whether or not a backward FFT should be carried out after propagation.
         */
        void propagate(Wave &wave, bool do_backward_fft);

        /*!
         * Given an (empty) wave, shape the wave to represent a focused STEM electron beam with
         * given defocus and x and y coordinate.
         * @param wave the Complex2D wave object to shape
         * @param defocus defocus value
         * @param x x coordinate of the focus point
         * @param y y coordinate of the focus point
         */
        void makeProbe(Wave &wave, const double defocus, double x, double y) const;

        /*!
         * Method to send HTTP status report when HTTP reporting is configured.
         * @param signal The signal to send
         * @param st The SimulationState object of the current simulation state.
         * @param progress The progress where 1=100%.
         */
        void postStatus(STATUS_SIGNAL signal, const SimulationState &st, float progress);

        /*!
         * Method to send HTTP status report when HTTP reporting is configured.
         * @param signal The signal to send
         * @param st The SimulationState object of the current simulation state.
         */
        void postStatus(STATUS_SIGNAL signal, const SimulationState &st) {
            postStatus(signal, st, 0);
        }

        /*!
         * Initializes result store buffers and the serialization buffer for the message passing.
         */
        void initBuffers();

        /*!
         * Store all finished results up to now.
         * @param st The SimulationState object of the current simulation state.
         * @param work_packages work_packages vector with the ScanPoints
         * @param scan_work The TaskQueue of the scan work
         * @return the microseconds the operation required.
         */
        std::chrono::microseconds
        storeDirtyResults(const SimulationState &st, std::vector<ScanPoint> &work_packages,
                TaskQueue &scan_work);
    };
}

#endif //STEMSALABIM_SIMULATION_HPP
