/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#ifndef STEMSALABIM_SIMULATIONSTATE_HPP
#define STEMSALABIM_SIMULATIONSTATE_HPP

#include <vector>
#include <chrono>

#include "GridManager.hpp"
#include "FPConfManager.hpp"

namespace stemsalabim {

    /*!
     * Utility class to manage the current simulation state, i.e., current configuration, defocus etc.
     * It is supposed to be managed by SimulationStateManager
     */
    class SimulationState {
    public:
        /*!
         * Constructor to set the state
         * @param idefocus index of defocus
         * @param iconf index of configuration
         * @param ndefoci number of defoci
         * @param nconfs number of configuarions
         * @param defocus defocus value in nm
         * @param iteration iteration value (mixed defocus, configuration)
         */
        SimulationState(unsigned int idefocus, unsigned int iconf, unsigned int ndefoci, unsigned int nconfs,
                float defocus, unsigned int iteration)
                : _idefocus(idefocus)
                , _iconf(iconf)
                , _ndefoci(ndefoci)
                , _nconfs(nconfs)
                , _defocus(defocus)
                , _iteration(iteration) {}

        /*!
         * We provide a default constructor so that the class can be used for functions that
         * need a SimulationState but don't use it.
         */
        SimulationState() = default;

        /*!
         * Accessor method
         * @return configuration index
         */
        inline unsigned int iconf() const {
            return _iconf;
        }

        /*!
         * Accessor method
         * @return defocus index
         */
        inline unsigned int idefocus() const {
            return _idefocus;
        }

        /*!
         * Accessor method
         * @return defocus value in nm
         */
        inline float defocus() const {
            return _defocus;
        }

        /*!
         * Accessor method
         * @return iteration index
         */
        inline unsigned int iteration() const {
            return _iteration;
        }

        /*!
         * Start the state. This only sets an internal timer for duration estimation.
         */
        inline void start() {
            _start_time = std::chrono::high_resolution_clock::now();
        };

        /*!
         * Get the estimated microseconds for completion of a configuration.
         * @param progress the current progress of in [0,1] of the configuration
         * @return estimated microseconds
         */
        inline std::chrono::microseconds etaConf(float progress) const {
            return std::chrono::microseconds((long) round(algorithms::getTimeSince(_start_time).count() / progress));
        }

        /*!
         * Get the estimated microseconds for completion of a defocus.
         * @param progress the current progress of in [0,1] of the configuration
         * @return estimated microseconds
         */
        inline std::chrono::microseconds etaDefocus(float progress) const {
            return etaConf(progress) * _nconfs;
        }

        /*!
         * Get the estimated microseconds for completion of the hole simulation.
         * @param progress the current progress of in [0,1] of the configuration
         * @return estimated microseconds
         */
        inline std::chrono::microseconds etaSimulation(float progress) const {
            return etaConf(progress) * _nconfs * _ndefoci;
        }


    private:
        unsigned int _idefocus{0};
        unsigned int _iconf{0};
        unsigned int _ndefoci{0};
        unsigned int _nconfs{0};
        float _defocus{0};
        unsigned int _iteration{0};
        std::chrono::high_resolution_clock::time_point _start_time{};

    };

/*!
 * Class to manage the simulation states. Essentially wraps a vector of SimulationState that may be iterated.
 * @tparam float precision type, float or double
 */

    class SimulationStateManager {
    public:
        /*!
         * Constructor. Prepares a vector of all the SimulationState objects that the simulation will go through.
         * @param gridman the GridManager object.
         */
        explicit SimulationStateManager(const std::shared_ptr<GridManager> &gridman)
                : _gridman(gridman) {
            Params &prms = Params::getInstance();

            for(unsigned int idefocus = 0; idefocus < prms.numberOfDefoci(); idefocus++) {
                for(unsigned int iconf = 0; iconf < prms.numberOfConfigurations(); iconf++) {
                    unsigned int iteration = idefocus * prms.numberOfConfigurations() + iconf;
                    _states.push_back(SimulationState(idefocus,
                                                      iconf,
                                                      prms.numberOfDefoci(),
                                                      prms.numberOfConfigurations(),
                                                      gridman->defoci()[idefocus],
                                                      iteration));
                }
            }
        }

        typedef typename std::vector<SimulationState>::iterator iterator;
        typedef typename std::vector<SimulationState>::const_iterator const_iterator;

        iterator begin() { return _states.begin(); }

        const_iterator begin() const { return _states.begin(); }

        iterator end() { return _states.end(); }

        const_iterator end() const { return _states.end(); }

    private:

        /// The GridManager that manages all required sweeps and grids.
        std::shared_ptr<GridManager> _gridman;

        /// container of the SimulationState s
        std::vector<SimulationState> _states;
    };
}

#endif //STEMSALABIM_SIMULATIONSTATE_HPP
