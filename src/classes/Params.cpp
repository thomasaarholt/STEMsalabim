/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#include "Params.hpp"

#include <thread>
#include <fstream>

#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif

#include "../3rdparty/args/args.hxx"

#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif

#include "../3rdparty/sole/sole.hpp"
#include "../utilities/output.hpp"
#include "../utilities/algorithms.hpp"

using namespace libconfig;
using namespace args;
using namespace std;
using namespace stemsalabim;

void Params::initFromCLI(int argc, const char **argv) {
    ArgumentParser parser("STEMsalabim usage instructions.", "Please refer to the User documentation.");
    HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});
    Flag skip_simulation(parser, "skip-simulation", "Skip simulation, only write crystal", {"skip-simulation"});
    ValueFlag<string> params(parser, "params", "The parameter file path.", {'p', "params"});
    ValueFlag<unsigned int> num_threads(parser, "num-threads", "The number of threads per MPI proc.", {"num-threads"});
    ValueFlag<unsigned int> package_size(parser,
                                         "package-size",
                                         "Number of tasks in one MPI work package.",
                                         {"package-size"});
    ValueFlag<float> defocus(parser, "defocus", "The probe defocus.", {'d', "defocus"});

    ValueFlag<unsigned int> num_configurations(parser,
                                               "num-configurations",
                                               "The number of Frozen Phonon configurations.",
                                               {"num-configurations"});
    ValueFlag<string> output_file(parser, "output-file", "The output file path.", {'o', "output-file"});
    ValueFlag<string> tmp_dir(parser, "tmp-dir", "The path to the temporary directory.", {"tmp-dir"});
    ValueFlag<string> crystal_file(parser, "crystal-file", "The crystal file path.", {'c', "crystal-file"});
    ValueFlag<string> title(parser, "title", "The simulation title.", {'t', "title"});
    ValueFlag<string> diag_dir(parser,
                               "diagnostics-dir",
                               "The directory to place diagnostics output files.",
                               {"diagnostics-dir"});

    try {
        parser.ParseCLI(argc, argv);
    } catch(Help &) {
        cout << parser;
        exit(0);
    } catch(ParseError &e) {
        cerr << e.what() << endl;
        cerr << parser;
        exit(1);
    }

    if(argc > 1) {
        vector<string> all_args;
        all_args.assign(argv + 1, argv + argc);
        _command_line_arguments = algorithms::join(all_args, " ");
    }

    if(skip_simulation) {
        _cli_options.push_back(params.Name());
        _skip_simulation = true;
    }

    if(params) {
        _cli_options.push_back(params.Name());
        _param_file = get(params);
    } else {
        output::error("a parameter file MUST be specified!\n");
    }

    if(num_threads) {
        _cli_options.push_back(num_threads.Name());
        _num_threads = get(num_threads);
    }

    if(package_size) {
        _cli_options.push_back(package_size.Name());
        _work_package_size = get(package_size);
    }

    if(num_configurations) {
        _cli_options.push_back(num_configurations.Name());
        _number_configurations = get(num_configurations);
    }

    if(output_file) {
        _cli_options.push_back(output_file.Name());
        _output_file = get(output_file);
    }

    if(tmp_dir) {
        _cli_options.push_back(tmp_dir.Name());
        _tmp_dir = get(tmp_dir);
    }

    if(diag_dir) {
        _cli_options.push_back(diag_dir.Name());
        _diagnostics_dir = get(diag_dir);
    }

    if(crystal_file) {
        _cli_options.push_back(crystal_file.Name());
        _crystal_file = get(crystal_file);
    }

    if(title) {
        _cli_options.push_back(title.Name());
        _title = get(title);
    }

    if(defocus) {
        _cli_options.push_back(defocus.Name());
        _probe_defocus = get(defocus);
    }

    _uuid = sole::uuid4().str();
}

void Params::readParamsFromString(const string &prms) {

    try {
        _cfg.readString(prms);
#if LIBCONFIGXX_VER_MAJOR == 1 && LIBCONFIGXX_VER_MINOR < 7
        _cfg.setOptions(Setting::OptionAutoConvert & ~Setting::OptionOpenBraceOnSeparateLine);
#else
        _cfg.setOptions(Config::OptionAutoConvert & ~Config::OptionOpenBraceOnSeparateLine);
#endif
    } catch(const ParseException &pex) {
        output::error("Parse error at %s:%d - %s", pex.getFile(), pex.getLine(), pex.getError());
    }

    Setting &root = _cfg.getRoot();

    //Setting &root = _cfg.getRoot();
    _cfg.lookupValue("application.random_seed", _random_seed);
    _cfg.lookupValue("application.skip_simulation", _skip_simulation);

    string prec_str = "single";
    _cfg.lookupValue("application.precision", prec_str);
    if(prec_str.compare("d") == 0 || prec_str.compare("double") == 0 || prec_str.compare("high") == 0)
        _double_precision = true;

    if(!cliParamGiven("title"))
        _cfg.lookupValue("simulation.title", _title);

    if(!cliParamGiven("output-file"))
        _cfg.lookupValue("simulation.output_file", _output_file);
    if(!cliParamGiven("tmp-dir"))
        _cfg.lookupValue("simulation.tmp_dir", _tmp_dir);
    _cfg.lookupValue("simulation.output_compress", _output_compress);

    _cfg.lookupValue("simulation.normalize_always", _normalize_always);
    _cfg.lookupValue("simulation.bandwidth_limiting", _bandwidth_limiting);

    _cfg.lookupValue("probe.c5", _probe_c5_coeff);
    _cfg.lookupValue("probe.cs", _probe_spherical_aberration_coeff);
    _cfg.lookupValue("probe.astigmatism_ca", _probe_astigmatism_ca);
    _cfg.lookupValue("probe.astigmatism_angle", _probe_astigmatism_angle);
    _cfg.lookupValue("probe.min_apert", _probe_min_aperture);
    _cfg.lookupValue("probe.max_apert", _probe_max_aperture);
    _cfg.lookupValue("probe.beam_energy", _beam_energy);
    _cfg.lookupValue("probe.scan_density", _probe_scan_density);

    if(!cliParamGiven("defocus"))
        _cfg.lookupValue("probe.defocus", _probe_defocus);

    _cfg.lookupValue("probe.num_defoci", _probe_num_defoci);
    _cfg.lookupValue("probe.fwhm_defoci", _probe_fwhm_defoci);

    _cfg.lookupValue("specimen.max_potential_radius", _max_potential_radius);
    _cfg.lookupValue("specimen.field_file", _field_file);

    if(!cliParamGiven("crystal-file"))
        _cfg.lookupValue("specimen.crystal_file", _crystal_file);

    _cfg.lookupValue("grating.density", _sample_density);
    _cfg.lookupValue("grating.slice_thickness", _slice_thickness);

    if(!cliParamGiven("num-configurations"))
        _cfg.lookupValue("frozen_phonon.number_configurations", _number_configurations);
    _cfg.lookupValue("frozen_phonon.fixed_slicing", _fixed_slicing);

    // CBED stuff
    if(_cfg.exists("cbed")) {
        _cfg.lookupValue("cbed.enabled", _cbed_enabled);
        _cfg.lookupValue("cbed.x.[0]", get<0>(_cbed_scan_x));
        _cfg.lookupValue("cbed.x.[1]", get<1>(_cbed_scan_x));
        _cfg.lookupValue("cbed.y.[0]", get<0>(_cbed_scan_y));
        _cfg.lookupValue("cbed.y.[1]", get<1>(_cbed_scan_y));
        _cfg.lookupValue("cbed.size.[0]", get<0>(_cbed_size));
        _cfg.lookupValue("cbed.size.[1]", get<1>(_cbed_size));
        _cfg.lookupValue("cbed.save_slices_every", _cbed_save_every_n_slices);
        _cfg.lookupValue("cbed.average_configurations", _cbed_average_configurations);
        _cfg.lookupValue("cbed.average_defoci", _cbed_average_defoci);

        // when we calculate a single defocus only, set average_defoci to false.
        if(_cbed_average_defoci && _probe_num_defoci == 1)
            _cbed_average_defoci = false;

        // when defoci are averaged, configurations need to be averaged as well.
        if(_cbed_average_defoci)
            _cbed_average_configurations = true;
    }

    // ADF stuff
    if(_cfg.exists("adf")) {
        _cfg.lookupValue("adf.enabled", _adf_enabled);
        _cfg.lookupValue("adf.x.[0]", get<0>(_adf_scan_x));
        _cfg.lookupValue("adf.x.[1]", get<1>(_adf_scan_x));
        _cfg.lookupValue("adf.y.[0]", get<0>(_adf_scan_y));
        _cfg.lookupValue("adf.y.[1]", get<1>(_adf_scan_y));

        _cfg.lookupValue("adf.detector_min_angle", get<0>(_adf_detector_angles));
        _cfg.lookupValue("adf.detector_max_angle", get<1>(_adf_detector_angles));
        _cfg.lookupValue("adf.detector_num_angles", get<2>(_adf_detector_angles));
        _cfg.lookupValue("adf.detector_interval_exponent", _adf_detector_interval_exponent);

        _cfg.lookupValue("adf.average_configurations", _adf_average_configurations);
        _cfg.lookupValue("adf.average_defoci", _adf_average_defoci);
        _cfg.lookupValue("adf.save_slices_every", _adf_save_every_n_slices);

        // when we calculate a single defocus only, set average_defoci to false.
        if(_adf_average_defoci && _probe_num_defoci == 1)
            _adf_average_defoci = false;

        // when defoci are averaged, configurations need to be averaged as well.
        if(_adf_average_defoci)
            _adf_average_configurations = true;
    }

    // convert lengths to angstrom and densities to /angstrom
    if(_cfg.exists("grating.density"))
        _sample_density /= 10;

    if(_cfg.exists("probe.scan_density"))
        _probe_scan_density /= 10;

    if(_cfg.exists("grating.slice_thickness"))
        _slice_thickness *= 10;

    if(_cfg.exists("specimen.max_potential_radius"))
        _max_potential_radius *= 10;

    if(_cfg.exists("probe.c5"))
        _probe_c5_coeff *= 10;

    if(_cfg.exists("probe.cs"))
        _probe_spherical_aberration_coeff *= 10;

    if(cliParamGiven("defocus") || _cfg.exists("probe.defocus"))
        _probe_defocus *= 10;

    if(_cfg.exists("probe.fwhm_defoci"))
        _probe_fwhm_defoci *= 10;

    if(_cfg.exists("probe.astigmatism_ca"))
        _probe_astigmatism_ca *= 10;

    _cfg.lookupValue("http_reporting.reporting", _http_reporting);
    _cfg.lookupValue("http_reporting.url", _http_url);
    _cfg.lookupValue("http_reporting.auth_user", _http_auth_user);
    _cfg.lookupValue("http_reporting.auth_pass", _http_auth_pass);
    if(_cfg.exists("http_reporting.parameters")) {
        for(auto &prm: root["http_reporting"]["parameters"]) {
            _http_parameters.push_back(make_tuple(prm.getName(), (const char *) (prm)));
        }
    }

    if(_tmp_dir.empty()) {
        // use the path of the output file directory for tmp
        int found = _output_file.find_last_of("/\\");
        if(found < 0)
            _tmp_dir = ".";
        else
            _tmp_dir = _output_file.substr(0,found);
    }

    if(_random_seed == 0)
        _random_seed = (unsigned int) chrono::system_clock::now().time_since_epoch().count();
    _rgen.seed(_random_seed);

}
