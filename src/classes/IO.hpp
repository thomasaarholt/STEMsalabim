/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#ifndef STEMSALABIM_IO_H
#define STEMSALABIM_IO_H


#include <memory>
#include <vector>
#include <tuple>

#include <netcdf.h>
#include "Params.hpp"
#include "../utilities/memory.hpp"
#include "../utilities/Wave.hpp"

namespace stemsalabim {


    class Crystal;


    class GridManager;


    class ScanPoint;


    class FPConfManager;

    /*!
     * Class for writing the output NetCDF file of STEMsalabim.
     * @tparam float precision type, double or float
     */

    class IO {
    public:
        /*!
         * Destructor, takes care of closing the NetCDF file and writes the stopping time to it.
         * @TODO: Maybe this should be moved to its own function.
         */
        ~IO();

        /*!
         * Initialize NetCDF output file. Here, most of the hierarchical structure is written, including
         * variables and dimension. Also, compression and chunking is set up and some information about the simulation
         * is written to the file.
         * @param gridman Reference to the GridManager required for writing some information.
         */
        void initNcFile(const std::shared_ptr<GridManager> &gridman, const std::shared_ptr<Crystal> &crystal);

        /*!
         * Initialize the memory buffers that are used when averaging results.
         * @param gridman Reference to the GridManager required for writing some information.
         */
        void initBuffers(const std::shared_ptr<GridManager> &gridman);

        /*!
         * Appends the atomic positions of the current FP configuration to the AMBER group.
         * @param fpman FP configuration manager
         */
        void writeCrystal(const std::shared_ptr<FPConfManager> &fpman, const std::shared_ptr<Crystal> &crystal);

        /*!
         * Write all simulation parameters to the output file.
         */
        void writeParams(const std::shared_ptr<GridManager> &gridman);

        /*!
         * Append to the ADF intensities variable.
         * @param iconf current FP configuration index
         * @param idefocus current defocus index
         * @param gridman reference to a GridManager
         * @param point The pixel for which intensity values are to be written.
         * @param ibuf The number buffer with the actual intensity values.
         */
        void writeAdfIntensities(unsigned int idefocus, unsigned int iconf,
                const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf);
        void writeAdfIntensities(unsigned int idefocus, unsigned int iconf,
                const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, int g_id, int v_id);

        /*!
         * Append to the CBED intensities variable.
         * @param iconf current FP configuration index
         * @param idefocus current defocus index
         * @param gridman reference to a GridManager
         * @param point The pixel for which intensity values are to be written.
         * @param cbuf The number buffer with the actual intensity values.
         */
        void writeCBEDIntensities(unsigned int idefocus, unsigned int iconf,
                const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &cbuf);
        void writeCBEDIntensities(unsigned int idefocus, unsigned int iconf,
                const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &cbuf, int g_id, int v_id);

        /*!
         * Write temporary results to a binary file (custom format). This is done in MPI mode.
         * @param idefocus current defocus index
         * @param iconf current FP configuration index
         * @param point The pixel for which intensity values are to be written.
         * @param ibuf The number buffer with the actual adf intensity values.
         * @param cbuf The number buffer with the actual cbed intensity values.
         */
        static void writeTemporaryResult(unsigned int idefocus, unsigned int iconf,
                ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf,
                std::shared_ptr<memory::buffer::number_buffer<float>> &cbuf);

        /*!
         * Copy written temporary results from the binary files to the NC output file. (MPI mode)
         * @param gridman reference to a GridManager
         * @param ibuf The number buffer with the actual adf intensity values.
         * @param cbuf The number buffer with the actual cbed intensity values.
         */
        void copyFromTemporaryFile(const std::shared_ptr<GridManager> &gridman,
                std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf,
                std::shared_ptr<memory::buffer::number_buffer<float>> &cbuf);

        /*!
         * Write the various grids to the output file.
         * @param gridman reference to the GridManager
         */
        void writeGrids(const std::shared_ptr<GridManager> &gridman);

        /*!
         * Write a Complex2D wave function to a text file. This is a utility function for debugging purposes.
         * @param filename the file to write to
         * @param wave the wave to write.
         */
        static void dumpWave(std::string filename, const Wave &wave);

    private:
        typedef std::vector<size_t> vs;

        std::mutex _io_lock;

        bool _created{false};

        double _running_weight{0};

        std::vector<float> _adf_intensities_buffer;
        std::vector<float> _cbed_intensities_buffer;


    };
}

#endif //STEMSALABIM_IO_H
