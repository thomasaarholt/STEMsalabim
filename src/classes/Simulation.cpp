/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#include "Simulation.hpp"
#include <memory>
#include <thread>
#include "../libatomic/Scattering.hpp"
#include "../utilities/http.hpp"
#include "../utilities/timings.hpp"

using namespace std;
using namespace chrono;
using namespace stemsalabim;


void Simulation::init() {
    high_resolution_clock::time_point init_start = high_resolution_clock::now();

    Params &p = Params::getInstance();

    auto &mpi_env = mpi::Environment::getInstance();

    _crystal = std::make_shared<Crystal>();
    _gridman = std::make_shared<GridManager>(_crystal);
    _fpman = std::make_shared<FPConfManager>(_crystal);

    if(mpi_env.isMpi())
        output::print("[%s, %d] Hello from %s, MPI rank %d/%d, about to spawn %d threads.\n",
                      mpi_env.name(),
                      mpi_env.rank(),
                      mpi_env.name(),
                      mpi_env.rank(),
                      mpi_env.size(),
                      p.numberOfThreads());


    {
        ifstream t("/proc/" + to_string(getpid()) + "/status");
        string procstatus((istreambuf_iterator<char>(t)), istreambuf_iterator<char>());
        istringstream f(procstatus);
        string line;
        while(getline(f, line)) {
            if(line.find("Cpus_allowed_list") != string::npos) {
                PRINT_DIAGNOSTICS(output::fmt("[%s, %d] %s.\n", mpi_env.name(), mpi_env.rank(), line));
            }
        }
    }

    // read and communicate the content of the crystal file and initialize crystal
    // object.
    {
        string crystal_file_content;
        if(mpi_env.isMaster()) {
            ifstream t(p.crystalFilename());

            stringstream buffer;
            buffer << t.rdbuf();

            crystal_file_content = buffer.str();
        }
        mpi_env.broadcast(crystal_file_content, mpi::Environment::MASTER);
        _crystal->init(crystal_file_content);
    }

    // read and communicate the field file content, when it is given as a parameter
    if(!p.fieldFilename().empty()) {
        string field_file_content;
        if(mpi_env.isMaster()) {
            ifstream t(p.fieldFilename());

            stringstream buffer;
            buffer << t.rdbuf();

            field_file_content = buffer.str();
        }
        mpi_env.broadcast(field_file_content, mpi::Environment::MASTER);
        _crystal->readFieldFile(field_file_content);
        _crystal->assignFieldsToSlices();
    }


    // generate the required random numbers for the frozen phonon stuff.
    {
        const size_t number_fp_random_numbers = p.numberOfConfigurations() *
                                                p.numberOfDefoci() *
                                                _crystal->numberOfAtoms() *
                                                3;
        vector<double> numbers;
        if(mpi_env.isMaster()) {
            numbers.resize(number_fp_random_numbers);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
            normal_distribution<double> distribution(0.0, 1.0);
            auto rgen = p.getRandomGenerator();
            for(unsigned long i = 0; i < number_fp_random_numbers; ++i)
                numbers[i] = distribution(rgen);
#pragma GCC diagnostic pop
        }
        mpi_env.broadcast(numbers, mpi::Environment::MASTER);

        _fpman->init(numbers);
    }

    _gridman->generateGrids();

    // post start signal after generation of the grids.
    postStatus(STATUS_SIGNAL::START_SIMULATION, SimulationState() /* actually not required for this signal */);

    // generateSlices must be done before writing anything to the output file, i.e.,
    // before calling io->init()
    generatePropagator();

    // initialize output file if this is master process and write some simulation information
    // to it.
    _io = std::make_shared<IO>();
    if(mpi_env.isMaster()) {
        // As IO should be handled by the master only, we need to initialize
        // the instance only when this is the master process.
        // The crystal instance will be initialized here and then broadcast to
        // tge slaves.
        _io->initNcFile(_gridman, _crystal);
        _io->writeParams(_gridman);
        _io->writeGrids(_gridman);
    }
    _io->initBuffers(_gridman);

    printMaster(output::fmt("Calculating on a real and k space lattice of %dx%d points.\n",
                            _gridman->samplingX(),
                            _gridman->samplingY()));
    printMaster(output::fmt("Calculating %d defoci, %d FP confs, %dx%d scan points.\n",
                            p.numberOfDefoci(),
                            p.numberOfConfigurations(),
                            _gridman->adfXGrid().size(),
                            _gridman->adfYGrid().size()));

    if(mpi_env.isMpi())
        PRINT_DIAGNOSTICS("starting initialization of FFTW wisdom\n");

    // every MPI processor figures out the FFT plans itself. This should somehow be
    // improved at some point
    //Wave::initPlans(_gridman->samplingX(), _gridman->samplingY());

    // make the atomic potentials
    //Scattering::init(_crystal);

    if(mpi_env.isMpi())
        PRINT_DIAGNOSTICS("finished initialization\n");

    printMaster(output::fmt("Initialization took %s.\n", output::humantime(algorithms::getTimeSince(init_start))));

    initBuffers();
}


void Simulation::run() {
    Params &prms = Params::getInstance();

    // initialize an MPI environment
    auto &mpi_env = mpi::Environment::getInstance();

    // set up some clocks to measure times.
    high_resolution_clock::time_point start_sim, t1;
    start_sim = high_resolution_clock::now();

    // in case the slicing is fixed, this can be done outside of the main loops
    if(prms.isFixedSlicing()) {
        _fpman->assignAtomsToSlices();

        for(shared_ptr<Slice> &slice: _crystal->slices())
            printMaster(output::fmt("Slice %d [%.3fnm - %.3fnm] contains %d atoms.\n",
                                    slice->id(),
                                    slice->z() / 10,
                                    (slice->z() + slice->thickness()) / 10,
                                    slice->numberOfAtoms()));
    }

    SimulationStateManager state_manager(_gridman);

    // loop over simulations
    for(SimulationState &st: state_manager) {

        // this starts a timer
        st.start();

        _fpman->setConfiguration(st.iteration());

        // if a new defocus was started, do some output stuff.
        if(!st.iconf()) {
            postStatus(STATUS_SIGNAL::START_DEFOCUS, st);
            printMaster(st, output::fmt("Starting Defocus %.2fnm.\n", st.defocus() / 10));
        }

        postStatus(STATUS_SIGNAL::START_CONFIGURATION, st);
        printMaster(st, "Starting configuration. \n");

        t1 = high_resolution_clock::now();

        // in each configuration, assign atoms to the corresponding slices.
        if(!prms.isFixedSlicing()) {
            _fpman->assignAtomsToSlices();

            printMaster(st, output::fmt("Finished slicing in %s.\n", output::humantime(algorithms::getTimeSince(t1))));

            for(shared_ptr<Slice> &slice: _crystal->slices())
                printMaster(st,
                            output::fmt("Slice %d [%.3fnm - %.3fnm] contains %d atoms.\n",
                                        slice->id(),
                                        slice->z() / 10,
                                        (slice->z() + slice->thickness()) / 10,
                                        slice->numberOfAtoms()));
        }

        // in each frozen phonon iteration, first write the crystal to the output
        // file.
        if(mpi_env.isMaster())
            _io->writeCrystal(_fpman, _crystal);

        // skip multi-slice simulation when skip_simulation = true in the configuration file.
        if(prms.skipSimulation()) {
            continue;
        }

        // generate transmission functions in parallel using both threads and the main thread.
        // This is executed on each MPI processor.
        t1 = high_resolution_clock::now();

        if(mpi_env.isMpi())
            PRINT_DIAGNOSTICS(output::fmt("starting slice work\n"));

        TaskQueue slice_work;
        slice_work.append(_crystal->slices());

        vector<thread> threads;
        for(unsigned int nt = 1; nt < prms.numberOfThreads(); nt++) {
            threads.emplace_back([&] {
                unsigned int slic_index;
                while(slice_work.pop(slic_index)) {
                    _crystal->slices()[slic_index]->calculateTransmissionFunction(_fpman, _gridman);
                    slice_work.finish(slic_index);
                }
            });
        }

        // also do work on the main thread.
        unsigned int slic_index;
        while(slice_work.pop(slic_index)) {
            _crystal->slices()[slic_index]->calculateTransmissionFunction(_fpman, _gridman);
            slice_work.finish(slic_index);
        }

        if(mpi_env.isMpi())
            PRINT_DIAGNOSTICS("waiting for slice work to finish work\n");

        slice_work.waitUntilFinished();
        for(auto & t : threads)
            t.join();

        printLine(st,
                  output::fmt("Finished calculation of transmission functions in %s.\n",
                              output::humantime(algorithms::getTimeSince(t1))));

        if(mpi_env.isMaster()) {
            multisliceMaster(st);
        } else {
            multisliceWorker(st);
        }

        // each MPI process now (sequentially) writes his pixels to the NC file.
        if(mpi_env.isMpi()) {
            //This barrier is not really required.
            mpi_env.barrier();

            auto start_io = high_resolution_clock::now();

            for(int rank = 0; rank < mpi_env.size(); rank++) {
                if(rank == mpi_env.rank()) {
                    auto start = high_resolution_clock::now();
                    _io->copyFromTemporaryFile(_gridman, _adf_intensity_buffer, _cbed_intensity_buffer);

                    printLine(st,
                              output::fmt("Finished copying pixels for %s.\n",
                                          output::humantime(algorithms::getTimeSince(start))));
                }

                mpi_env.barrier();
            }

            printMaster(st,
                        output::fmt("Gathering result files took %s.\n",
                                    output::humantime(algorithms::getTimeSince(start_io))));
        }

    };

    postStatus(STATUS_SIGNAL::FINISH_SIMULATION, SimulationState());

    printMaster(output::fmt("Finished computation in %s. Find the results in %s.\n",
                            output::humantime(algorithms::getTimeSince(start_sim)),
                            prms.outputFilename()));

}


void Simulation::calculatePixel(ScanPoint &point, const double defocus) {
    Params &p = Params::getInstance();

    vector<float> adf_intensities;
    vector<float> cbed_intensities;
    vector<float> detector_intensities;

    if(point.adf) {
        adf_intensities.reserve(_gridman->adfSliceCoords().size() * _gridman->adfDetectorGrid().size());
        detector_intensities.resize(_gridman->adfDetectorGrid().size());
    }

    if(point.cbed)
        cbed_intensities.reserve(_gridman->storedCbedSizeX() *
                                 _gridman->storedCbedSizeY() *
                                 _gridman->cbedSliceCoords().size());

    Wave wave;
    wave.init(_gridman->samplingX(), _gridman->samplingY());

    // shape the probe wave function according to where the probe is
    makeProbe(wave, defocus, point.x, point.y);

    // propagate through each slice and the following vacuum
    for(const shared_ptr<Slice> &slice: _crystal->slices()) {

        slice->transmit(wave);

        // the propagate function with the false as second parameter
        // leaves the wave in fourier space, so we can apply detector without
        // FFTing the wave back and forth.
        // Also, propagation takes care of bandwidth limiting the wave function
        // to 2/3 of the maximum k.
        propagate(wave, false);

        //wave.forwardFFT();

        if(p.normalizeAlways())
            wave.normalize();

        if(point.adf && _gridman->adfStoreSlice(slice->id())) {

            std::fill(detector_intensities.begin(), detector_intensities.end(), 0);

            // here, we iterate the k space and calculate the wave angles.
            // Then, we determine the corresponding detector angle and sum the intensity up.
            for(unsigned int ix = 0; ix < _gridman->samplingX(); ix++) {
                for(unsigned int iy = 0; iy < _gridman->samplingY(); iy++) {

                    int ind = _gridman->adfBinIndex(ix, iy);

                    if(ind >= 0) {
                        // check for not a number
                        auto intens = (float) pow(abs(wave(ix, iy)), 2);
                        if(!::isnan(intens))
                            detector_intensities[ind] += intens;
                    }
                }
            }

            adf_intensities.insert(adf_intensities.end(), detector_intensities.begin(), detector_intensities.end());
        }

        if(point.cbed && _gridman->cbedStoreSlice(slice->id())) {

            // first, we cut off high frequencies from the wave if necessary. Note, that we follow
            // the FFTW storage scheme for frequencies, hence, half of the values are in the lower
            // range and the other half in the range of the wave function.
            size_t bandwidth_lx = _gridman->storedCbedSizeX(true);
            size_t bandwidth_ly = _gridman->storedCbedSizeY(true);

            vector<float> tmp_intensities;
            tmp_intensities.reserve(bandwidth_lx * bandwidth_ly);

            double total_intensity = 0, intensity;

            for(size_t i = 0; i < bandwidth_lx; i++) {
                size_t ii = i;

                if(i > bandwidth_lx / 2)
                    ii = wave.lx() - bandwidth_lx + i;

                for(size_t j = 0; j < bandwidth_ly; j++) {
                    size_t jj = j;

                    if(j > bandwidth_ly / 2)
                        jj = wave.ly() - bandwidth_ly + j;

                    intensity = pow(abs(wave(ii, jj)), 2);
                    total_intensity += intensity;
                    tmp_intensities.push_back(intensity);
                }
            }

            // take care of resizing the CBED if desired
            if(p.cbedRescale()) {
                vector<float> rescaled;
                rescaled = algorithms::bilinearRescale(tmp_intensities,
                                                       p.cbedSizeX(),
                                                       p.cbedSizeY(),
                                                       bandwidth_lx,
                                                       bandwidth_ly);

                double int_sum_rescaled = 0;
                for(float i : rescaled)
                    int_sum_rescaled += i;

                for(float &i : rescaled)
                    i *= total_intensity / int_sum_rescaled;

                cbed_intensities.insert(cbed_intensities.end(), rescaled.begin(), rescaled.end());
            } else {
                cbed_intensities.insert(cbed_intensities.end(), tmp_intensities.begin(), tmp_intensities.end());
            }
        }

        wave.backwardFFT();
    }

    if(point.adf)
        point.storeAdfIntensities(adf_intensities, _adf_intensity_buffer);

    if(point.cbed)
        point.storeCBEDIntensities(cbed_intensities, _cbed_intensity_buffer);

}

void Simulation::printLine(const SimulationState &st, const string &line) {
    Params &p = Params::getInstance();

    auto &mpi_env = mpi::Environment::getInstance();

    if(mpi_env.isSlave())
        output::print("[%d/%d, %d/%d] [%s, %d] %s",
                      st.idefocus() + 1,
                      p.numberOfDefoci(),
                      st.iconf() + 1,
                      p.numberOfConfigurations(),
                      mpi_env.name(),
                      mpi_env.rank(),
                      line);
    else
        output::print("[%d/%d, %d/%d] %s",
                      st.idefocus() + 1,
                      p.numberOfDefoci(),
                      st.iconf() + 1,
                      p.numberOfConfigurations(),
                      line);
}


void Simulation::printMaster(const SimulationState &st, const string &line) {
    auto &mpi_env = mpi::Environment::getInstance();

    if(mpi_env.isMaster())
        printLine(st, line);
}


void Simulation::printMaster(const string &line) {
    auto &mpi_env = mpi::Environment::getInstance();

    if(mpi_env.isMaster())
        output::print("%s", line);
}


void Simulation::multisliceMaster(const SimulationState &st) {

    Params &prms = Params::getInstance();

    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    // initialize an MPI environment
    auto &mpi_env = mpi::Environment::getInstance();

    if(!mpi_env.isMaster())
        output::error("This function cannot be called from an MPI slave!");

    // This vector<float> holds all pixels/scan points of the STEM image to be calculated.
    vector<ScanPoint> work_packages = _gridman->scanPoints();

    // This task queue holds all indices of the pixels to calculate. It'll be needed only
    // on the MPI master, as each MPI slave will manage its own queue of tasks.
    TaskQueue scan_work;
    scan_work.append(work_packages);

    // keep the master's worker threads busy as well. We will fetch from the task queue
    // one by one AFTER the previous task is finished, as the queue is worked on in parallel
    // via MPI. It is very important not to distribute all tasks here.
    std::vector<std::thread> threads;
    for(unsigned int nt = 1; nt < prms.numberOfThreads(); nt++) {
        threads.emplace_back([&] {
            unsigned int pix_index = 0;
            while(scan_work.pop(pix_index)) {
                calculatePixel(work_packages[pix_index], st.defocus());
                scan_work.finish(pix_index);
            }
        });
    }

    // now, wait for incoming results and, upon receiving one, send a new work package out if
    // there are any left
    if(mpi_env.isMpi()) {

        mpi::Status s;

        unsigned int last_prcnt = 0;

        // We loop here until a valid MPI request comes in. When it does, process the results and
        // send out a new work package.
        do {
            if(mpi_env.iprobe(s)) {
                if(s.tag() == mpi::Environment::MPI_TAG_TASKS_REQUEST) {
                    vector<unsigned int> package = scan_work.popMultiple(prms.workPackageSize());
                    vector<unsigned int> finished_pixels;

                    mpi_env.recv(finished_pixels, s.source(), s.tag());
                    mpi_env.send(package, s.source(), mpi::Environment::MPI_TAG_TASKS);

                    scan_work.finish(finished_pixels);
                }

            }

            for(unsigned int px_idx : scan_work.getDirtyAndClean()) {
                ScanPoint &sp = work_packages[px_idx];

                // we store only pixels with intensities, as all others are the ones
                // which are calculated by a different MPI worker.
                if(sp.hasAdfIntensities() || sp.hasCbedIntensities())
                    IO::writeTemporaryResult(st.idefocus(),
                                             st.iconf(),
                                             sp,
                                             _adf_intensity_buffer,
                                             _cbed_intensity_buffer);
            }

            // progress output
            float p = scan_work.progress();
            if(p * 100 > last_prcnt) {
                last_prcnt = (unsigned int) ceil(p * 100);

                postStatus(STATUS_SIGNAL::PROGRESS, st, p);

                printLine(st,
                          output::fmt("progr: %d%%, eta/conf: %s, eta/defocus: %s, eta/sim: %s\n",
                                      ceil(p * 100),
                                      output::humantime(st.etaConf(p)),
                                      output::humantime(st.etaDefocus(p)),
                                      output::humantime(st.etaSimulation(p))));
            }
        } while(!scan_work.finished());

        for(unsigned int px_idx : scan_work.getDirtyAndClean()) {
            ScanPoint &sp = work_packages[px_idx];
            IO::writeTemporaryResult(st.idefocus(), st.iconf(), sp, _adf_intensity_buffer, _cbed_intensity_buffer);
        }

        scan_work.waitUntilFinished();

        printLine(st,
                  output::fmt("Received all results, calculation took %s.\n",
                              output::humantime(algorithms::getTimeSince(t1))));

    } else {

        microseconds io_duration(0);

        // do some more work on the main thread.
        unsigned int pix_index = 0;
        unsigned int last_prcnt = 0;

        while(scan_work.pop(pix_index)) {
            calculatePixel(work_packages[pix_index], st.defocus());
            scan_work.finish(pix_index);

            // store finished intensities
            io_duration += storeDirtyResults(st, work_packages, scan_work);

            float p = scan_work.progress();

            if(p * 100 > last_prcnt) {
                last_prcnt = (unsigned int) ceil(p * 100);

                postStatus(STATUS_SIGNAL::PROGRESS, st, p);

                printLine(st,
                          output::fmt("progr: %d%%, eta/conf: %s, eta/defocus: %s, eta/sim: %s\n",
                                      ceil(p * 100),
                                      output::humantime(st.etaConf(p)),
                                      output::humantime(st.etaDefocus(p)),
                                      output::humantime(st.etaSimulation(p))));
            }
        }

        scan_work.waitUntilFinished();

        io_duration += storeDirtyResults(st, work_packages, scan_work);

        printLine(st,
                  output::fmt("Calculation took %s and I/O took %s.\n",
                              output::humantime(algorithms::getTimeSince(t1)),
                              output::humantime(io_duration)));
    }

    // join the threads
    for(auto & t: threads)
        t.join();


}


microseconds
Simulation::storeDirtyResults(const SimulationState &st, vector<ScanPoint> &work_packages, TaskQueue &scan_work) {
    vector<unsigned int> d = scan_work.getDirtyAndClean();
    if(!d.empty()) {
        auto start_io = high_resolution_clock::now();
        storeFinishedPixels(st, work_packages, d);
        auto finish_io = high_resolution_clock::now();
        return duration_cast<microseconds>(finish_io - start_io);
    }
    return std::chrono::milliseconds(0);
}


void Simulation::multisliceWorker(const SimulationState &st) {
    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    // initialize an MPI environment
    auto &mpi_env = mpi::Environment::getInstance();
    auto &prms = Params::getInstance();

    if(mpi_env.isMaster())
        output::error("This function cannot be called by the MPI master!");

    // This vector<float> holds all pixels/scan points of the STEM image to be calculated.
    vector<ScanPoint> work_packages = _gridman->scanPoints();

    // set up own work queue with pixel indices
    TaskQueue pixel_index_queue;
    unsigned long pixel_calculated = 0;
    vector<unsigned int> package;
    vector<thread> threads;

    while(true) {

        // request new tasks
        mpi_env.send(package, mpi::Environment::MASTER, mpi::Environment::MPI_TAG_TASKS_REQUEST);
        mpi_env.recv(package, 0, mpi::Environment::MPI_TAG_TASKS);

        if(package.empty())
            break;

        pixel_index_queue.append(package);

        pixel_calculated += package.size();

        for(unsigned int nt = 1; nt < prms.numberOfThreads(); nt++) {
            threads.emplace_back([&] {
                unsigned int pix_index;
                while(pixel_index_queue.pop(pix_index)) {
                    calculatePixel(work_packages[pix_index], st.defocus());
                    pixel_index_queue.finish(pix_index);
                }
            });
        }

        // also do work on the main thread.
        unsigned int pix_index;
        while(pixel_index_queue.pop(pix_index)) {
            calculatePixel(work_packages[pix_index], st.defocus());
            pixel_index_queue.finish(pix_index);
        }

        pixel_index_queue.waitUntilFinished();

        for(auto & t: threads)
            t.join();
        threads.clear();

        pixel_index_queue.reset();

        for(unsigned int px_idx : package) {
            ScanPoint &sp = work_packages[px_idx];
            IO::writeTemporaryResult(st.idefocus(), st.iconf(), sp, _adf_intensity_buffer, _cbed_intensity_buffer);
        }

    }

    PRINT_DIAGNOSTICS("Exiting loop");

    printLine(st,
              output::fmt("Finished working for %s and calculated %d pixels.\n",
                          output::humantime(algorithms::getTimeSince(t1)),
                          pixel_calculated));
}


void Simulation::initBuffers() {
    Params &prms = Params::getInstance();
    typedef typename memory::buffer::number_buffer<float> buf_type;

    auto size_type_size = sizeof(typename vector<float>::size_type);

    // The serialization buffer is for sending work packages around. It should be larger than the
    // theoretical maximal size of any work package filled with data.
    unsigned long number_intensities_per_pixel = _gridman->adfDetectorGrid().size() * _gridman->adfSliceCoords().size();
    unsigned long number_cbed_per_pixel = prms.cbed() ? (_gridman->storedCbedSizeX() *
                                                         _gridman->storedCbedSizeY() *
                                                         _gridman->cbedSliceCoords().size()) : 0;

    unsigned long max_size_work_package = sizeof(ScanPoint) +
                                          2 * size_type_size +
                                          number_intensities_per_pixel * sizeof(float) +
                                          number_cbed_per_pixel * sizeof(float);

    _serialization_buffer.resize(prms.workPackageSize() * max_size_work_package + /*Add 10MB*/10 * 1024 * 1024);
    _adf_intensity_buffer = std::make_shared<buf_type>(number_intensities_per_pixel, 2 * prms.workPackageSize());
    _cbed_intensity_buffer = std::make_shared<buf_type>(number_cbed_per_pixel, 2 * prms.workPackageSize());
}


void Simulation::storeFinishedPixels(const SimulationState &st, vector<ScanPoint> &work_packages,
        const vector<unsigned int> &indices) {
    Params &prms = Params::getInstance();

    for(unsigned int index: indices) {
        ScanPoint &point = work_packages[index];

        if(prms.adf() && point.adf && point.hasAdfIntensities()) {
            _io->writeAdfIntensities(st.idefocus(), st.iconf(), _gridman, point, _adf_intensity_buffer);
            point.clearAdfIntensities(_adf_intensity_buffer);
        }

        if(prms.cbed() && point.cbed && point.hasCbedIntensities()) {
            _io->writeCBEDIntensities(st.idefocus(), st.iconf(), _gridman, point, _cbed_intensity_buffer);
            point.clearCBEDIntensities(_cbed_intensity_buffer);
        }
    }
}


void Simulation::generatePropagator() {
    Params &p = Params::getInstance();

    double scale = -1. * Params::pi * p.sliceThickness() * p.wavelength();


    unsigned int lx = _gridman->samplingX();
    unsigned int ly = _gridman->samplingY();
    double sdense2 = pow(1. / 3. * p.samplingDensity(), 2);

    _propagator.init(lx, ly);

    for(unsigned int ix = 0; ix < lx; ix++) {
        for(unsigned int iy = 0; iy < ly; iy++) {
            if(!p.bandwidthLimiting() || pow(_gridman->kx(ix), 2) + pow(_gridman->ky(iy), 2) < sdense2) {
                _propagator(ix, iy) = polar(1.0, scale * (float) (pow(_gridman->kx(ix), 2) + pow(_gridman->ky(iy), 2)));
            } else {
                _propagator(ix, iy) = 0.0;
            }
        }
    }
}


void Simulation::propagate(Wave &wave, bool do_backward_fft) {
    // propagate and bandwidth limit
    // see page 145ff in Kirkland and 402 - 405 in autostem.cpp of his code.
    wave.forwardFFT();

    wave *= _propagator;

    if(do_backward_fft)
        wave.backwardFFT();
}


void Simulation::makeProbe(Wave &wave, const double defocus, double px, double py) const {
    Params &p = Params::getInstance();

    double wl = p.wavelength();
    double alpha_sq;
    double kx, ky;
    double chi_defocus, chi_astigmatism, chi_cs, chi_c5;
    double pref = Params::pi / wl;
    double w;

    wave.setIsKSpace(true);

    for(unsigned int x = 0; x < _gridman->samplingX(); ++x) {
        for(unsigned int y = 0; y < _gridman->samplingY(); ++y) {
            kx = _gridman->kx(x);
            ky = _gridman->ky(y);
            alpha_sq = wl * wl * (kx * kx + ky * ky);

            if(alpha_sq <= pow(p.probeMinAperture() / 1000., 2) || alpha_sq >= pow(p.probeMaxAperture() / 1000., 2)) {

                // set wave to zero here.
                wave(x, y) = std::polar(0.0, 1.0);
            } else {
                chi_defocus = -pref * defocus * alpha_sq;
                chi_astigmatism = pref *
                                  alpha_sq *
                                  p.probeAstigmatismCa() *
                                  (double) cos(2 * (atan2(ky, kx) - p.probeAstigmatismAngle()));
                chi_cs = pref / 2 * p.probePhericalAberrationCoeff() * (double) pow(alpha_sq, 2);
                chi_c5 = pref / 3 * p.probeC5() * (double) pow(alpha_sq, 3);

                w = 2.0 * Params::pi * (px * kx + py * ky);
                wave(x, y) = std::polar(1.0, w + chi_defocus - chi_astigmatism - chi_cs - chi_c5);
            }
        }
    }

    wave.normalize();
    wave.backwardFFT();
}


void Simulation::postStatus(STATUS_SIGNAL signal, const SimulationState &st, float progress) {
#if(HAVE_CURL)
    Params &p = Params::getInstance();

    if(!p.httpReporting())
        return;

    mpi::Environment &mpi_env = mpi::Environment::getInstance();

    if(mpi_env.isSlave())
        return;

    // cancel request when the last request was less than 20 seconds ago
    // TODO: Make this configurable
    if(signal == STATUS_SIGNAL::PROGRESS &&
       algorithms::getTimeSince(_http_request_timer) < duration_cast<microseconds>(seconds(20)))
        return;

    http::Environment &e = http::Environment::getInstance();
    Json::Value c;

    c["time"] = output::currentTimeString();
    c["id"] = p.uuid();

    c["num_threads"] = p.numberOfThreads();
    c["num_processors"] = mpi_env.size();
    c["num_defoci"] = _gridman->defoci().size();
    c["num_configurations"] = p.numberOfConfigurations();

    if(signal == STATUS_SIGNAL::START_SIMULATION) {
        c["event"] = "START_SIMULATION";
        c["version"] = output::fmt("%s.%s.%s", PKG_VERSION_MAJOR, PKG_VERSION_MINOR, PKG_VERSION_PATCH);
        c["git_commit"] = GIT_COMMIT_HASH;
        c["title"] = p.title();
    } else if(signal == STATUS_SIGNAL::START_DEFOCUS) {
        c["event"] = "START_DEFOCUS";
        c["defocus"] = st.defocus() / 10.;
        c["defocus_index"] = st.idefocus();
    } else if(signal == STATUS_SIGNAL::START_CONFIGURATION) {
        c["event"] = "START_FP_CONFIGURATION";
        c["defocus"] = st.defocus() / 10.;
        c["defocus_index"] = st.idefocus();
        c["configuration_index"] = st.iconf();
    } else if(signal == STATUS_SIGNAL::PROGRESS) {
        c["event"] = "PROGRESS";
        c["defocus"] = st.defocus() / 10.;
        c["defocus_index"] = st.idefocus();
        c["configuration_index"] = st.iconf();
        c["progress"] = progress;


    } else if(signal == STATUS_SIGNAL::FINISH_SIMULATION) {
        c["event"] = "FINISH";
    }

    _http_request_timer = high_resolution_clock::now();

    e.post(c);
#endif
}
