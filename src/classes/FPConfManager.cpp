/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#include "FPConfManager.hpp"


#include "Slice.hpp"

using namespace std;
using namespace stemsalabim;


void FPConfManager::init(const std::vector<double> &random_numbers) {
    // atomic displacements are cached for all configurations

    bool fixed_slicing = Params::getInstance().isFixedSlicing();

    size_t cnt = 0;
    _displacements.resize(_total_configurations);
    assert(random_numbers.size() >= _total_configurations * _crystal->numberOfAtoms() * 3);

    for(size_t j = 0; j < _total_configurations; j++) {

        _displacements[j].resize(_crystal->numberOfAtoms());

        for(size_t i = 0; i < _crystal->numberOfAtoms(); i++) {
            get<0>(_displacements[j][i]) = sqrt(_crystal->getAtoms()[i]->getMSD()) * random_numbers[cnt++];
            get<1>(_displacements[j][i]) = sqrt(_crystal->getAtoms()[i]->getMSD()) * random_numbers[cnt++];
            if(fixed_slicing) {
                get<2>(_displacements[j][i]) = 0;
                cnt++;
            } else {
                get<2>(_displacements[j][i]) = sqrt(_crystal->getAtoms()[i]->getMSD()) * random_numbers[cnt++];
            }
        }
    }
}


void FPConfManager::assignAtomsToSlices() {

    for(shared_ptr<Slice> &slic: _crystal->slices())
        slic->clearAtoms();

    size_t i = 0;
    for(shared_ptr<atomic::Atom> a: _crystal->getAtoms()) {
        unsigned int slice_index;
        if(a->hasSlice())
            slice_index = a->getSlice();
        else {
            double z = a->getZ() + dz(i);
            slice_index = _crystal->sliceIndex(z);
        }

        _crystal->slices()[slice_index]->addAtom(a);

        i++;
    }
}
