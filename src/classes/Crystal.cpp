/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#include "Crystal.hpp"

#include <memory>
#include <regex>

#include "../utilities/output.hpp"
#include "../libatomic/Scattering.hpp"
#include "Slice.hpp"

using namespace std;
using namespace stemsalabim;


void stemsalabim::Crystal::init(const std::string &crystal_file_content) {
    // this order is very important!

    atomic::ElementProvider p = atomic::ElementProvider::getInstance();

    // some useful variable declarations
    istringstream iss(crystal_file_content);
    string line = "";
    vector<string> tokens;
    double x, y, z, msd;
    unsigned int slice = 0;

    // the first line is only the number of atoms.
    getline(iss, line);

    // from the second line, extract the system size
    // and convert from nm to angstrom
    getline(iss, line);

    //determine, if we have extended xyz or normal xyz format. In normal xyz format, this
    // line contains only 3 numbers, corresponding to cell size in x y and z direction. In
    // extended xyz, it is like this: Lattice="lx 0.0 0.0 0.0 ly 0.0 0.0 0.0 lz"
    if(line.find("Lattice=") != string::npos) {
        regex e("Lattice=\"([0-9\\seE+-\\.]+)\"", regex_constants::icase);
        smatch m;
        if(regex_search(line, m, e)) {
            tokens = algorithms::split(m[1], ' ');

            algorithms::trim(tokens[0]);
            algorithms::trim(tokens[4]);
            algorithms::trim(tokens[8]);

            _size_x = std::stod(tokens[0]) * 10;
            _size_y = std::stod(tokens[4]) * 10;
            _size_z = std::stod(tokens[8]) * 10;
        } else {
            output::error("Wrong syntax of extended XYZ file line 2.");
        }
    } else {
        tokens = algorithms::split(line, ' ');

        algorithms::trim(tokens[0]);
        algorithms::trim(tokens[1]);
        algorithms::trim(tokens[2]);

        _size_x = std::stod(tokens[0]) * 10;
        _size_y = std::stod(tokens[1]) * 10;
        _size_z = std::stod(tokens[2]) * 10;

    }

    atomic::Scattering &sf = atomic::Scattering::getInstance();

    // the rest of the lines is atoms with positions
    size_t atom_id = 0;
    while(getline(iss, line)) {
        if(line.length() == 0)
            continue;

        tokens = algorithms::split(line, ' ');

        if(tokens.size() != 5 && tokens.size() != 6) {
            output::error("Init file needs to have 5 or 6 columns!\n");
        }

        bool with_slices = (tokens.size() == 6);

        algorithms::trim(tokens[0]);
        algorithms::trim(tokens[1]);
        algorithms::trim(tokens[2]);
        algorithms::trim(tokens[3]);
        algorithms::trim(tokens[4]);

        if(with_slices) {
            algorithms::trim(tokens[5]);
            slice = std::stoi(tokens[5]);
        }

        // x y z are columns 1,2,3 and the mean square displacement is column 4. Convert to angstrom
        x = std::stod(tokens[1]) * 10;
        y = std::stod(tokens[2]) * 10;
        z = std::stod(tokens[3]) * 10;
        msd = std::stod(tokens[4]) * 100;

        // set the site's occupant
        if(with_slices) {
            _atoms.push_back(shared_ptr<atomic::Atom>(new atomic::Atom(x,
                                                                       y,
                                                                       z,
                                                                       msd,
                                                                       p.elementBySymbol(tokens[0]),
                                                                       atom_id,
                                                                       slice)));
        } else {
            _atoms.push_back(shared_ptr<atomic::Atom>(new atomic::Atom(x,
                                                                       y,
                                                                       z,
                                                                       msd,
                                                                       p.elementBySymbol(tokens[0]),
                                                                       atom_id)));
        }

        _elements.insert(p.elementBySymbol(tokens[0]));

        sf.initPotential(p.elementBySymbol(tokens[0]));

        atom_id++;
    }

    generateSlices();
}


void stemsalabim::Crystal::readFieldFile(const std::string &field_file_content) {

    // some useful variable declarations
    istringstream iss(field_file_content);
    string line = "";
    vector<string> tokens;
    double x, y, z, field;

    // ignore first and second lines.
    getline(iss, line);
    getline(iss, line);

    // the rest of the lines is atoms with positions
    while(getline(iss, line)) {
        if(line.length() == 0)
            continue;

        tokens = algorithms::split(line, ' ');

        if(tokens.size() != 4) {
            output::error("Field file needs to have 4 columns!\n");
        }

        algorithms::trim(tokens[0]);
        algorithms::trim(tokens[1]);
        algorithms::trim(tokens[2]);
        algorithms::trim(tokens[3]);

        // x y z are columns 0,1,2 and the field is column 3. Convert to angstrom
        x = std::stod(tokens[0]) * 10;
        y = std::stod(tokens[1]) * 10;
        z = std::stod(tokens[2]) * 10;
        field = std::stod(tokens[3]);

        // set the site's occupant
        _fields.push_back(make_tuple(x, y, z, field));
    }
}


void stemsalabim::Crystal::generateSlices() {
    Params &p = Params::getInstance();

    double dz = p.sliceThickness();
    double slice_top_offset = -dz / 8.0;

    // first, generate the slices. Then iterate all existing atoms and add them to the slices they appear in.
    // then iterate the slices and generate the transmission functions.

    // we start slightly above the sample and need to get a little hacky in order to correctly
    // slice everything. Now, atomic coordinates with precision as bad as two decimals should be
    // taken care of.
    unsigned int c = 0;
    for(double z = slice_top_offset; z < _size_z; z += dz, c++)
        _slices.push_back(shared_ptr<Slice>(new Slice(z, dz, c)));
}


void Crystal::assignFieldsToSlices() {

    for(shared_ptr<Slice> &slic: _slices)
        slic->clearFields();

    size_t i = 0;
    double x, y, z, field;
    for(tuple<double, double, double, double> &f: _fields) {
        // should be improved
        std::tie(x, y, z, field) = f;
        unsigned int slice_index = sliceIndex(z);

        _slices[slice_index]->addField(x, y, field);

        i++;
    }
}
