/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#include <iostream>
#include "utilities/Wave.hpp"
#include "classes/Crystal.hpp"
#include "classes/Simulation.hpp"

using namespace std;
using namespace stemsalabim;

int main(int argc, const char **argv) {

    Params &p = Params::getInstance();
    p.initFromCLI(argc, argv);

    std::ifstream t(p.paramsFileName());
    string params_file_content = std::string((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());

    p.readParamsFromString(params_file_content);

    // create the simulation instance an pass the parameters instance
    // to it.
    Simulation s;
    s.init();
}