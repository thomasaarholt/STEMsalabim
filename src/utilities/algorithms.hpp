/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

// This namespace includes some algorithms functions for various purposes.

#ifndef ALGORITHMS_HPP_
#define ALGORITHMS_HPP_

/*! @file algorithms.hpp */

#include <vector>
#include <string>
#include <random>
#include <cstring>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <chrono>
#include <iterator>
#include <cmath>

/*!
 * Globale STEMsalabim namespace.
 */
namespace stemsalabim {
    /*!
     * Contains various algorithmic helper functions used in STEMsalabim.
     */
    namespace algorithms {

        template<typename T>
        void ignore(T &&) {}

        /*!
         * Round a number to the next higher even integer. For example, 3.3 would
         * be rounded to 4, as would 2.9.
         * @tparam numeric_t The type, should be numeric.
         * @param val The value to round
         * @return the next highest even integer.
         */
        template<typename numeric_t>
        int round_even(numeric_t val) {
            int res = (int) std::round(val);
            return res + res % 2;
        }

        /*!
         * Split a string by a (char) delimiter.
         * @param s The string to split
         * @param delim The delimiter character
         * @return vector of strings of the parts after splitting
         */
        inline std::vector<std::string> split(const std::string &s, char delim) {
            // clear the result vector
            std::vector<std::string> elems;

            // this will hold our token
            std::string item("");

            // use stdlib to tokenize the string
            std::stringstream ss(s);
            while(getline(ss, item, delim))
                if(!item.empty())
                    elems.push_back(item);

            return elems;
        }

        /*!
         * Concatenate a vector of strings using a custom separator.
         * @param elements The vector of elements to join
         * @param separator The separator character.
         * @return The concatenated string.
         */
        inline std::string join(const std::vector<std::string> &elements, const char *const separator) {
            switch(elements.size()) {
                case 0:
                    return "";
                case 1:
                    return elements[0];
                default:
                    std::ostringstream os;
                    std::copy(elements.begin(), elements.end() - 1, std::ostream_iterator<std::string>(os, separator));
                    os << *elements.rbegin();
                    return os.str();
            }
        }

        /*!
         * Trim a string (remove spaces) in-place from the left
         * @param s the string to trim
         */
        static inline void ltrim(std::string &s) {
            s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        }

        /*!
         * Trim a string (remove spaces) in-place from the right.
         * @param s the string to trim
         */
        static inline void rtrim(std::string &s) {
            s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(),
                    s.end());
        }

        /*!
         * Trim a string (remove spaces) in-place from both sides.
         * @param s the string to trim
         */
        static inline void trim(std::string &s) {
            ltrim(s);
            rtrim(s);
        }

        /*!
         * Trim a string (remove spaces) by copying from the left.
         * @param s the string to trim
         */
        static inline std::string ltrimmed(std::string s) {
            ltrim(s);
            return s;
        }

        /*!
         * Trim a string (remove spaces) by copying from the right.
         * @param s the string to trim
         */
        static inline std::string rtrimmed(std::string s) {
            rtrim(s);
            return s;
        }

        /*!
         * Trim a string (remove spaces) by copying from both sides.
         * @param s the string to trim
         */
        static inline std::string trimmed(std::string s) {
            trim(s);
            return s;
        }

        /*!
         * Get the closest entry in a vector to a given numeric value.
         * @param v the vector of doubles
         * @param val the value that distances are calculated to.
         * @return The entry of v that is algebraically closest to val.
         */
        inline double closestValue(std::vector<double> v, double val) {
            std::vector<double>::iterator low, up;

            up = upper_bound(v.begin(), v.end(), val);
            low = up - 1;

            if((val - *low) < (*up - val))
                return *low;
            else
                return *up;
        }

        /*!
         * Return distributed numbers between min and max. The interval may not be linear, when
         * exponent != 1. Returns num_steps numbers calculated as
         * \f$ret[i] = x_{min} + \left(\frac{i}{N}\right)^p * (x_{max} - x_{min})\f$
         * @param min The lowest boundary \f$x_{min}\f$.
         * @param max The highest boundary \f$x_{max}\f$.
         * @param num_steps Number of points \f$N\f$.
         * @param exponent The interval exponent \f$p\f$.
         * @param include_max If true, max is the last point in the interval.
         * @return The array of numbers
         */
        inline std::vector<double>
        adaptiveSpace(double min, double max, unsigned int num_steps, float exponent, bool include_max) {
            std::vector<double> ret(num_steps);

            unsigned int offset = 0;

            if(include_max)
                offset = 1;

            for(unsigned int c = 0; c < num_steps; c++)
                ret[c] = min + pow((double) c / (num_steps - offset), exponent) * (max - min);

            return ret;
        }

        /*!
         * Return distributed numbers between min and max with equal distances.
         * @param min The lowest boundary \f$x_{min}\f$.
         * @param max The highest boundary \f$x_{max}\f$.
         * @param num_steps Number of points \f$N\f$.
         * @param include_max If true, max is the last point in the interval.
         * @return The array of numbers
         */
        inline std::vector<double> linSpace(double min, double max, unsigned int num_steps, bool include_max) {
            return adaptiveSpace(min, max, num_steps, 1.0, include_max);
        }

        inline int getIndexOfAdaptiveSpace(double val, double min, double max, unsigned int num_steps, float exponent,
                bool include_max) {

            if(val - min < 0)
                return -1;

            unsigned int offset = 0;

            if(include_max)
                offset = 1;

            double last_value = (double) pow((num_steps - 1) * (max - min) / (num_steps - offset), exponent);

            return (int) floor(pow((val - min) / (max - min) * last_value, 1. / exponent) /
                               (max - min) *
                               (num_steps - offset));
        }


        /*!
         * Get elapsed seconds since time t.
         * @param t from where to calculate from
         * @return seconds from t
         */
        inline std::chrono::microseconds getTimeSince(std::chrono::high_resolution_clock::time_point t) {
            return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t);
        }

        /*!
         * Utility function that clamps a pixel value to [0, length[.
         * @param px The input px value
         * @param length the max length
         * @return the clamped pixel value
         */
        inline unsigned int clamp(int px, unsigned int length) {
            return px >= (int) length ? length - 1 : (px < 0 ? 0 : (unsigned int) px);
        }

        /*!
         * Carry out cubic interpolation.
         * @tparam float precision type, float or double
         * @param A parameter A
         * @param B parameter B
         * @param C parameter C
         * @param D parameter D
         * @param t weight
         * @return interpolated value.
         */

//            float cubicInterpolate(float A, float B, float C, float D, float t) {
//                float a = -A / 2.0 + (3.0 * B) / 2.0 - (3.0 * C) / 2.0 + D / 2.0;
//                float b = A - (5.0 * B) / 2.0 + 2.0 * C - D / 2.0;
//                float c = -A / 2.0 + C / 2.0;
//                float d = B;
//
//                return a * t * t * t + b * t * t + c * t + d;
//            }

        /*!
         * Rescale an image (represented as a flattened 1D vector of intensities) to a new size using
         * bicubic interpolation. This algorithm is unfavourable as it turned out, but I'll leave the code
         * here in case it is used one day.
         * @tparam float precision type, float or double
         * @param input input image as a flattened 1D array
         * @param dest_width destination width
         * @param dest_height destination height
         * @param src_width source width
         * @param src_height source height
         * @return The rescaled image as a similar, flattened vector of intensities.
         */

//            std::vector<float>
//            bicubicRescale(const std::vector<float> &input, unsigned int dest_width, unsigned int dest_height,
//                    unsigned int src_width, unsigned int src_height) {
//                std::vector<float> out(dest_width * dest_height, 0);
//
//                const float tx = float(src_width) / dest_width;
//                const float ty = float(src_height) / dest_height;
//
//                for(unsigned int i = 0; i < dest_width; ++i) {
//                    for(unsigned int j = 0; j < dest_height; ++j) {
//
//                        const int x = int(tx * i);
//                        const int y = int(ty * j);
//                        const float dx = tx * i - x;
//                        const float dy = ty * j - y;
//
//                        // 1st row
//                        float p00 = input[clamp(x - 1, src_width) * src_height + clamp(y - 1, src_height)];
//                        float p10 = input[clamp(x + 0, src_width) * src_height + clamp(y - 1, src_height)];
//                        float p20 = input[clamp(x + 1, src_width) * src_height + clamp(y - 1, src_height)];
//                        float p30 = input[clamp(x + 2, src_width) * src_height + clamp(y - 1, src_height)];
//
//                        // 2nd row
//                        float p01 = input[clamp(x - 1, src_width) * src_height + clamp(y + 0, src_height)];
//                        float p11 = input[clamp(x + 0, src_width) * src_height + clamp(y + 0, src_height)];
//                        float p21 = input[clamp(x + 1, src_width) * src_height + clamp(y + 0, src_height)];
//                        float p31 = input[clamp(x + 2, src_width) * src_height + clamp(y + 0, src_height)];
//
//                        // 3rd row
//                        float p02 = input[clamp(x - 1, src_width) * src_height + clamp(y + 1, src_height)];
//                        float p12 = input[clamp(x + 0, src_width) * src_height + clamp(y + 1, src_height)];
//                        float p22 = input[clamp(x + 1, src_width) * src_height + clamp(y + 1, src_height)];
//                        float p32 = input[clamp(x + 2, src_width) * src_height + clamp(y + 1, src_height)];
//
//                        // 4th row
//                        float p03 = input[clamp(x - 1, src_width) * src_height + clamp(y + 2, src_height)];
//                        float p13 = input[clamp(x + 0, src_width) * src_height + clamp(y + 2, src_height)];
//                        float p23 = input[clamp(x + 1, src_width) * src_height + clamp(y + 2, src_height)];
//                        float p33 = input[clamp(x + 2, src_width) * src_height + clamp(y + 2, src_height)];
//
//                        // interpolate bi-cubically!
//                        float col0 = cubicInterpolate(p00, p10, p20, p30, dx);
//                        float col1 = cubicInterpolate(p01, p11, p21, p31, dx);
//                        float col2 = cubicInterpolate(p02, p12, p22, p32, dx);
//                        float col3 = cubicInterpolate(p03, p13, p23, p33, dx);
//
//                        // as bicubic interpolation may result in values outside of the original boundaries,
//                        // we clip to 0 here. This results in artefacts and is the reason why this kind of
//                        // interpolation is not really good for our purposes.
//                        out[i * dest_height + j] = std::max(cubicInterpolate(col0, col1, col2, col3, dy), (float) 0.0);
//                    }
//                }
//
//                return out;
//            }

        /*!
         * Rescale an image (represented as a flattened 1D vector of intensities) to a new size using
         * bilinear interpolation.
         * @tparam float precision type, float or double
         * @param input input image as a flattened 1D array
         * @param dest_width destination width
         * @param dest_height destination height
         * @param src_width source width
         * @param src_height source height
         * @return The rescaled image as a similar, flattened vector of intensities.
         */

        inline
        std::vector<float>
        bilinearRescale(const std::vector<float> &input, unsigned int dest_width, unsigned int dest_height,
                unsigned int src_width, unsigned int src_height) {
            std::vector<float> out(dest_width * dest_height, 0);

            const float tx = float(src_width) / dest_width;
            const float ty = float(src_height) / dest_height;

            for(unsigned int i = 0; i < dest_width; ++i) {
                for(unsigned int j = 0; j < dest_height; ++j) {

                    const int x = int(tx * i);
                    const int y = int(ty * j);
                    const float dx = tx * i - x;
                    const float dy = ty * j - y;

                    // 1st row
                    float p00 = input[clamp(x + 0, src_width) * src_height + clamp(y + 0, src_height)];
                    float p01 = input[clamp(x + 0, src_width) * src_height + clamp(y + 1, src_height)];
                    float p10 = input[clamp(x + 1, src_width) * src_height + clamp(y + 0, src_height)];
                    float p11 = input[clamp(x + 1, src_width) * src_height + clamp(y + 1, src_height)];

                    // formula from here: https://en.wikipedia.org/wiki/Bilinear_interpolation
                    out[i * dest_height + j] = p00 * (1.0 - dx) * (1.0 - dy) +
                                               p10 * dx * (1.0 - dy) +
                                               p01 * (1.0 - dx) * dy +
                                               p11 * dx * dy;
                }
            }

            return out;
        }

        /*!
         * Sign function
         * @tparam T type
         * @param val val to test
         * @return 1 for positive number, -1 for negative number, 0 otherwise
         */
        template<typename T>
        int sgn(T val) {
            return (T(0) < val) - (val < T(0));
        }
    }

}

#endif // ALGORITHMS_HPP_