/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#ifndef STEMSALABIM_HTTP_HPP
#define STEMSALABIM_HTTP_HPP

/** @file */

#include "config.h"

// This file is only required when libCURL is found.
#if(HAVE_CURL)

#include <tuple>
#include <memory>
#include <string>
#include <curl/curl.h>
#include <vector>
#include "../classes/Params.hpp"
#include "algorithms.hpp"
#include "output.hpp"


namespace stemsalabim {
    /*!
     * Contains classes and functions related to HTTP requests.
     */
    namespace http {

        size_t my_dummy_write(char *ptr, size_t size, size_t nmemb, void *userdata) {
            (void) (userdata);
            (void) (ptr);
            return size * nmemb;
        }

        /*!
         * Singleton class to manage the global CURL initialization state.
         * An instance needs to be fetched using getInstance.
         */
        class Environment {
        public:
            /*!
             * Return the instance of the class, which is created if called for the first time.
             * @return http::Environment instance.
             */
            static Environment &getInstance() {
                static Environment instance;
                return instance;
            }

            /*!
             * Submit a JSON POST request with some custom content. The URL, Auth etc. is
             * read from the Params object.
             * @param content Json node with additional payload to append to the request.
             */
            void post(const Json::Value &content) {

                Params &prms = Params::getInstance();

                CURL *curl;

                curl = curl_easy_init();
                if(curl) {
                    Json::Value root;

                    // Add parameters specified in the configuration file
                    std::vector<std::string> query_parts;
                    for(auto &p : prms.httpParameters()) {
                        root[std::get<0>(p)] = std::get<1>(p);
                    }

                    // the custom content will be in the 'content' =  {...} node.
                    root["content"] = content;

                    // to string
                    Json::FastWriter fastWriter;
                    std::string payload = fastWriter.write(root);

                    // initialize curl request
                    struct curl_slist *headers = NULL;
                    headers = curl_slist_append(headers, "Accept: application/json");
                    headers = curl_slist_append(headers, "Content-Type: application/json");
                    headers = curl_slist_append(headers, "charsets: utf-8");

                    curl_easy_setopt(curl, CURLOPT_URL, prms.httpUrl().c_str());
                    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, payload.c_str());
                    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
                    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &my_dummy_write);

                    // TODO: disable SSL verification. This is unsecure, I know, and should be
                    // moved to an option in the parameter file.
                    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);

                    if(!prms.httpAuthUser().empty()) {
                        std::string login = prms.httpAuthUser() + ":" + prms.httpAuthPass();
                        curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                        curl_easy_setopt(curl, CURLOPT_USERPWD, login.c_str());
                    }

                    CURLcode res = curl_easy_perform(curl);

                    // for http basic auth, the first request is for auth and the second request
                    // actually sends the stuff.
                    if(!prms.httpAuthUser().empty()) {
                        res = curl_easy_perform(curl);
                    }

                    if(res != CURLE_OK)
                        output::stderr("HTTP status request failed: %s\n", curl_easy_strerror(res));

                    curl_easy_cleanup(curl);
                }
            }

        private:
            /*!
             * Constructor is private to avoid multiple construction
             */
            Environment()
                    : _initialized(false) {
                if(!_initialized) {
                    curl_global_init(CURL_GLOBAL_ALL);
                    _initialized = true;
                }
            }

            /*!
             * Clean up curl.
             */
            ~Environment() {
                if(!_initialized)
                    return;
                curl_global_cleanup();
            }

            bool _initialized;

        public:
            Environment(Environment const &) = delete;

            void operator=(Environment const &)  = delete;
        };
    }
}

#endif // HAVE_CURL

#endif //STEMSALABIM_HTTP_HPP
