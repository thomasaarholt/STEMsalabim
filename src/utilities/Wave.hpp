/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2018 Jan Oliver Oelerich <jan.oliver.oelerich@physik.uni-marburg.de>
 * Copyright (c) 2016-2018 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#ifndef STEMSALABIM_COMPLEX2D_HPP
#define STEMSALABIM_COMPLEX2D_HPP

#include <vector>
#include <complex>
#include <map>
#include <tuple>

#include <fftw3.h>
#include <iostream>
#include <mutex>

#include <cstdlib>
#include <atomic>

namespace stemsalabim {

    template<typename T>
    void ignore(T &&) {}

    /*!
     * We define our own allocator to use with the vector<> stdlib class so
     * we use fftwf_malloc and fftw_malloc, which apparently helps with memory
     * alignment.
     * @tparam float
     */
    template<typename T>
    class fftwAllocator {
    public:
        typedef T value_type;
        typedef value_type *pointer;
        typedef value_type const *const_pointer;
        typedef void *void_pointer;
        typedef void const *const_void_pointer;
        typedef std::size_t size_type;
        typedef std::ptrdiff_t difference_type;
        template<typename U>
        struct rebind {
            typedef fftwAllocator<U> other;
        };

        pointer address(T &object) const {
            return &object;
        }

        const_pointer address(T const &object) const {
            return &object;
        }

        size_type max_size() const {
            return std::numeric_limits<std::size_t>::max();
        }

        template<typename... Args>
        void construct(pointer p, Args &&... args) {
            new(static_cast<void *>(p)) T(std::forward<Args>(args)...);
        }

        void destroy(pointer p) {
            p->~T();
        }

        pointer allocate(size_type count, const void * = nullptr) {
            return reinterpret_cast<T *>(fftwf_malloc(count * sizeof(T)));
        }

        void deallocate(pointer p, size_type count) {
            ignore(count);
            fftwf_free(static_cast<void *>(p));
        }

        bool operator==(fftwAllocator const &rhs) const {
            ignore(rhs);
            return true;
        }

        bool operator!=(fftwAllocator const &rhs) const {
            return not(*this == rhs);
        }
    };

    class FFTWCache {
    public:
        static FFTWCache &getInstance() {
            static FFTWCache instance;
            return instance;
        }

        typedef std::vector<std::complex<float>, fftwAllocator<std::complex<float>>> data_type;

        fftwf_plan getFwdPlan(unsigned int lx, unsigned int ly, bool quick_planning = false) {
            std::unique_lock<std::mutex> lck(_cache_mtx);

            auto key = std::make_tuple(lx, ly);

            if(_fwd_cache.count(key) > 0)
                return _fwd_cache.at(key);

            _fwd_cache[key] = makePlan(lx, ly, false, quick_planning);

            return _fwd_cache[key];
        }

        fftwf_plan getBwdPlan(unsigned int lx, unsigned int ly, bool quick_planning = false) {
            std::unique_lock<std::mutex> lck(_cache_mtx);

            auto key = std::make_tuple(lx, ly);

            if(_bwd_cache.count(key) > 0)
                return _bwd_cache.at(key);

            _bwd_cache[key] = makePlan(lx, ly, true, quick_planning);

            return _bwd_cache[key];
        }

    private:
        FFTWCache() {
            if(!_initialized) {
                _initialized = true;
            }
        }

        fftwf_plan makePlan(unsigned int lx, unsigned int ly, bool backward = false, bool quick = false) {
            data_type d(lx * ly, std::complex<float>(1.0, 0.0));
            return fftwf_plan_dft_2d(lx,
                                     ly,
                                     reinterpret_cast<fftwf_complex *>(&d[0]),
                                     reinterpret_cast<fftwf_complex *>(&d[0]),
                                     backward ? FFTW_FORWARD : FFTW_BACKWARD,
                                     quick ? FFTW_ESTIMATE : FFTW_PATIENT);
        }

        ~FFTWCache() {
            if(!_initialized)
                return;

            for(auto const &pl: _fwd_cache) {
                fftwf_destroy_plan(pl.second);
            }

            for(auto const &pl: _bwd_cache) {
                fftwf_destroy_plan(pl.second);
            }

        }

        bool _initialized{false};
        std::mutex _cache_mtx;
        std::map<std::tuple<unsigned int, unsigned int>, fftwf_plan> _fwd_cache;
        std::map<std::tuple<unsigned int, unsigned int>, fftwf_plan> _bwd_cache;
    };

    /*!
     * A 2D data structure of complex type, where the real and imaginary part are
     * of precision float. It allows forward and backward FFTs using fftw3 and takes
     * care of storing its plans in singleton structures.
     * @tparam float
     */

    class Wave {

    public:
        /*!
         * Construct empty object
         */
        Wave() = default;

        /*!
         * Copy constructor.
         * @param from_copy where to construct from
         */
        Wave(const Wave &from_copy) {
            _lx = from_copy._lx;
            _ly = from_copy._ly;
            _is_kspace = from_copy._is_kspace;
            _data = from_copy._data;
        }

        /*!
         * Assignment operator.
         * @param m from where to assign
         * @return this
         */
        Wave &operator=(const Wave &m) {
            _is_kspace = m._is_kspace;
            _lx = m._lx;
            _ly = m._ly;
            _data = m._data;

            return *this;
        }

        /*!
         * Widths of the 2D structure.
         * @return width
         */
        unsigned int lx() const {
            return _lx;
        }

        /*!
         * Height of the 2D structure.
         * @return height
         */
        unsigned int ly() const {
            return _ly;
        }

        /*!
         * Access an element via its index. Note, that the data in Complex2D is mapped
         * onto a single dimensional vector.
         * @param idx the index of the element
         * @return the element.
         */
        std::complex<float> &operator[](unsigned long idx) {
            return _data[idx];
        };

        /*!
         * Access an element via its index. Note, that the data in Complex2D is mapped
         * onto a single dimensional vector.
         * @param idx the index of the element
         * @return the element.
         */
        const std::complex<float> &operator[](unsigned long idx) const {
            return _data[idx];
        };

        /*!
         * Access an element by specifying its width and height indices.
         * @param idx width index
         * @param idy height index
         * @return the corresponding element.
         */
        std::complex<float> &operator()(unsigned long idx, unsigned long idy) {
            return _data[idy + idx * _ly];
        }

        const std::complex<float> &operator()(unsigned long idx, unsigned long idy) const {
            return _data[idy + idx * _ly];
        }

        /*!
         * Add another function onto this one.
         * @param m the other function
         * @return this
         */
        Wave &operator+=(const Wave &m) {
            for(unsigned int i = 0; i < _lx * _ly; i++)
                _data[i] += m._data[i];
            return *this;
        }

        /*!
         * Multiplly another function onto this one.
         * @param m the other function
         * @return this
         */
        Wave &operator*=(const Wave &m) {
            // the builtin product of std::complex is not
            // vectorized by gcc, so we're doing it manually
            // here.
            float tmp = 0;
            for(unsigned int i = 0; i < _lx * _ly; i++) {
                tmp = _data[i].real();
                _data[i].real(_data[i].real() * m._data[i].real() - _data[i].imag() * m._data[i].imag());
                _data[i].imag(tmp * m._data[i].imag() + _data[i].imag() * m._data[i].real());
            }

            return *this;
        }

        /*!
         * Multiply with a real number, element wise.
         * @param m the number
         * @return this
         */
        Wave &operator*=(const float xf) {
            for(unsigned int i = 0; i < _lx * _ly; i++)
                _data[i] *= xf;
            return *this;
        }

        /*!
         * Assign a real number to all elements of this function. The imaginary
         * parts will be zero.
         * @param m the number
         * @return this
         */
        Wave &operator=(const float xf) {
            for(unsigned int i = 0; i < _lx * _ly; i++)
                _data[i] = xf;
            return *this;
        }

        /*!
         * Initialize the class, i.e., allocate memory.
         * @param lx width
         * @param ly height
         */
        void init(unsigned int lx, unsigned int ly) {
            init(lx, ly, std::complex<float>(0, 0));
        }

        /*!
         * Initialize the function with a given complex value for all elements. If the data array is already
         * of the correct length, just overwrite all the  values.
         * @param lx width
         * @param ly height
         * @param value the initial value
         */
        void init(unsigned int lx, unsigned int ly, std::complex<float> value) {
            _initialized = true;

            _lx = lx;
            _ly = ly;

            // data is automatically 0 + I*0
            if(_data.size() != _lx * _ly)
                _data.resize(_lx * _ly, value);
            else
                std::fill(_data.begin(), _data.end(), value);
        }

        /*!
         * Fourier transform the function in-place.
         */
        void forwardFFT() {
            _is_kspace = true;
            auto &fftw_cache = FFTWCache::getInstance();
            fftwf_plan p = fftw_cache.getFwdPlan(_lx, _ly, _fftw_quick_planning);
            fftwf_execute_dft(p,
                              reinterpret_cast<fftwf_complex *>(&_data[0]),
                              reinterpret_cast<fftwf_complex *>(&_data[0]));
        }

        /*!
         * Clear the function, i.e., erase all data.
         */
        void clear() {
            _data.clear();
        }

        void fftwQuickPlanning(bool quick) {
            _fftw_quick_planning = quick;
        }

        /*!
         * Returns if the WF is initialized.
         * @return true if initialized
         */
        bool initialized() const {
            return _initialized;
        }

        /*!
         * Backward Fourier transform the function in-place.
         */
        void backwardFFT() {
            _is_kspace = false;

            auto &fftw_cache = FFTWCache::getInstance();
            fftwf_plan p = fftw_cache.getBwdPlan(_lx, _ly, _fftw_quick_planning);
            fftwf_execute_dft(p,
                              reinterpret_cast<fftwf_complex *>(&_data[0]),
                              reinterpret_cast<fftwf_complex *>(&_data[0]));

            // normalize
            double norm = 1. / (_lx * _ly);
            for(std::complex<float> &d: _data) {
                d *= norm;
            }
        }

        /*!
         * Normalize the function in-place, so that the sum of absolute values of all elements is unity.
         */
        void normalize() {
            double isnorm = 1. / sqrt(integratedIntensity());
            for(unsigned int i = 0; i < _lx * _ly; i++) {
                _data[i] *= isnorm;
            }
        }

        /*!
         * In order to distinguish between forward and backward FFTs, one may use this
         * function to determine whether we are in real or frequency (k) space.
         * @param is_kspace true if frequency space.
         */
        void setIsKSpace(bool is_kspace) {
            _is_kspace = is_kspace;
        }

        /*!
         * Get the square sum of all data.
         * @return total square sum, i.e., total intensity.
         */
        double integratedIntensity() const {
            double sum = 0.0;
            for(unsigned int i = 0; i < _lx * _ly; i++)
                sum += pow(std::abs(_data[i]), 2);
            return sum;
        }

        /*!
         * Get handle to the raw data of the WF.
         * @return complex valued data vector
         */
        const FFTWCache::data_type &data() const {
            return _data;
        }

    private:
        unsigned int _lx{0};
        unsigned int _ly{0};

        bool _is_kspace{false};
        bool _fftw_quick_planning{false};
        bool _initialized{false};

        FFTWCache::data_type _data;
    };

}
#endif //STEMSALABIM_COMPLEX2D_HPP
