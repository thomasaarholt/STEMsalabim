`STEMsalabim <http://www.stemsalabim.de>`_
==========================================

Project homepage and documentation: `www.stemsalabim.de <http://www.stemsalabim.de>`_

The *STEMsalabim* software aims to provide accurate scanning transmission electron microscopy (STEM) image simulation of
a specimen whose atomic structure is known. It implements the frozen lattice multi-slice algorithm as described in great
detail in the book `Advanced computing in electron microscopy <http://dx.doi.org/10.1007/978-1-4419-6533-2>`_ by Earl J.
Kirkland.

While there are multiple existing implementations of the same technique, none of them is suitable for running on
high-performance computing (HPC) clusters, which is required in order to simulate large supercells or large sets of
simulations, e.g. for parameter sweeps.

The purpose of *STEMsalabim* is to fill this gap by providing a multi-slice implementation that is well parallelizable
both within and across computing nodes, using a mixture of threaded parallelization and message passing interface (MPI).

Credits
-------

We acknowledge the creators of the supplementary libraries that *STEMsalabim* depends on. 

We would also like to acknowledge the creators of `STEMsim <http://dx.doi.org/10.1007/978-1-4020-8615-1_36>`_, which we
used as a reference implementation to test *STEMsalabim*.

Once again, we would like to highlight the book `Advanced computing in electron microscopy
<http://dx.doi.org/10.1007/978-1-4419-6533-2>`_ by Earl J. Kirkland for its detailed description of the implementation
of multi-slice algorithms.

*STEMsalabim* was written in the `Structure & Technology Research Laboratory <https://www.uni-marburg.de/wzmw/strl>`_ of
*the `Philipps-Universität Marburg <https://www.uni-marburg.de/>`_.